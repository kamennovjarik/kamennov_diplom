/*!
\file main.cpp
\brief ����, ���������� ������� ������� ���������
*/

/*!
\mainpage 
\brief ��������� ��� ��������� ��������� ������ ���� �� ������ �� ���������� �����.
*\author �������� �.�.

��������� ��������� ��������� �������:
<ol>
<li>�������� ��� ����������, ���� ��� ������� � ������ �� ������ ����� ��� ��������������� ����� ��� ��� ��������� � ��� � ����� �� �������� ��������;</li>
<li>�������� ��� ����������, ���� �����������, ������� ���������� ���������� � ���� � ������ � ����� �� �������� ��������, ����� ��������� ���������� �Stanford CoreNLP� � ������ �����;</li>
<li>���� � ������ ���������� ������ ��� ���������, �� ��������� ���������� ����������, ���� � ����� ���� �� ��������� �� ����� � ����������� ��� ����������� ���������� ��� ����������� ������������ � �������� ���������;</li>
<li>���������� ��� ���������� ��� ������ ��������� ���������� �� ������������ �������������, ���� ����������� ������������ ��� � ������ � ����� �� �������� �������� � ���������� ��� ������� �� �������� ��� ��������� � ���;</li>
<li>�������� �� ������������� ������������� � ��������� ��������� ��� ������ ����������, ������� ����� ���������� �������;</li>
<li>���������� ������� ���� � ������ ��������� ������ ����, ���� ��� �������� ������� ��� ������ ���������� ��������, ��������� ����� ������ ������ ����, ���������� � ���� ������ ������ ����;</li>
<li>���� � ������ ��������� ������ ��� ��������� � ���������� ������� �� ���������� ����������, �� ���������, ��� �������� ���� ���������� (�������� ����� ��������� ��������� ������� �� ����� �����  ��� �������).</li>
</ol>
<p>��������� ��������� �� ���� ������ StanfordCoreNLP, DOT ���� � ��������� ���������� � ����� � ����������� ������ �����.</p>
<p>��������� ���������� ���� � ����������� ������ ����� � ������</p>
*/

#include <QtCore/QCoreApplication>
#include <testclass.h>
#include <texttree.h>
#include <extractFunctions.h>
QSet<person> person::allPersons;
#include <person.h>
QStringList sentence::keys;
#include <QDateTime>

/*!������� ������� �������
*\param [in] argc - ���������� ���������� ����������
*\param [in] argv - ���������� ���������(���� � ������)
*\return - 0 ��� ���������� ������ ���������

*\brief ������� ������� �������

������� ���������� ����/�����, ������������� ������ ����� ����������� � �������� ������
*/
int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);
	if(argc > 1 && !strcmp(argv[1], "/TEST"))
	{
		TestClass it;
		QTest::qExec(&it);
		getchar();
		return 0;
	}

	QString XMLfile, HTMLfile, DOTfile, res;// ���� � ������ (����� �������): � ������� �������, � ������ � ���������� ������ �����, � ����� ����������, � ����� ����������

	//���� ������� ������ ��������
	if(argc > 1)
	{
		//������� ��� ���� � ����� DOT
		DOTfile = QString(argv[1]);
	}

	//���� ������� ������ ��������
	if(argc > 2)
	{
		//������� ��� ���� � ����� XML
		XMLfile = QString(argv[2]);
	}

	//���� ������� ������ ��������
	if(argc > 3)
	{
		//������� ��� ���� � ����� HTML
		HTMLfile = QString(argv[3]);
	}
	
	//������ ���� ����������� ����������
	dilogMode(argc, XMLfile, HTMLfile, DOTfile);
	
	//�������� ����� Result � ����������, �� ������� ��������� �������
	QDir().mkdir(QDir::currentPath() + QString("/Result"));

	//������� �������� ��������� �����(� ������ ���� �� ����)
	res = QDir::currentPath() + QString("/Result/Result") + QDateTime::currentDateTime().toString("yyyy_MM_dd_HH_mm_ss_zzz") + QString(".html");

	textTree* treeText;//����� � ���� ������ �������
	QMap<QString ,StanfordWord*> mapText;//�����, �������� �� ������(��������� ����� ID ����������� � �����)
	QSet  <person> PossibleSpeakers;//��������� ����������, ������� ����� ���������� ���������� �����
	QList<StanfordWord *> markedText;//��������� �� ��� ����� � �������� � ������

	//���� �� ���������� �����, ��������� �������������
	if(!input(XMLfile, HTMLfile, DOTfile, &treeText, mapText, markedText))
	{
		//������� ������
		QListIterator<StanfordWord *> i(markedText);
		while(i.hasNext())
		{
			delete i.next();
		}

		//����� ������ ���������
		return 0;
	}
	
	//������������� ������ ����� �����������
	colorDistributionSearch();

	//�������� ������
	textMarkup(mapText, treeText, PossibleSpeakers);

	//������������ ���������� � ������������� ���������� ������, ���� � ���������� ����� ����� ��� ���������
	if(person::allPersons.size() == 2)
		inOrderFinder(treeText);

	//����� ������ � ����
	output(treeText, res);

	//������� ������
	QListIterator<StanfordWord *> i(markedText);
	while(i.hasNext())
	{
		delete i.next();
	}

	//��������� ���������
	return 0;
}

void substructSubjectBySubjQualifier (int wordID, int sentID, sentence & context, QSet  <person> & speakersList)
{
	int startPunct = 0;//������ ����� ����������, ������������ ������ ����� �� ��������� ����� � ����������� 
	int wordPos = 0;//������ ��������� ����� � �����������

	//����������� ������� ����� � �����������
	while(context.words[wordPos]->wordID != wordID)
	{
		if(context.words[wordPos]->isPunct())
		{
			startPunct = wordPos;//��������� ������ ����� ����������
		}
		wordPos++;
	}

	int endPunct = wordPos;//������ ����� ����������, ������������ ������ ������ �� ��������� ����� � �����������

	//����������� ������� ������� ����� ����������, ������������ ������ ������ �� ��������� ����� � �����������
	while(!context.words[endPunct]->isPunct())
	{
		endPunct++;
	}

	QSetIterator<person> j(person::allPersons);//�������� �� ��������� ����������
	QSet<person> canTold;//��������� ����������, ������� ������� �������������
	bool isName = false;//�������� �� ����� ������

	//��������� ���������� � �������� ���������������
	while(j.hasNext() && !isName)
	{
		person tmp = j.next();//������� ��������
		//�������� �� ��������� ������ ������� ���������
		isName = tmp.name == context.words[wordPos]->word;

		QStringListIterator k(tmp.canUseSubjSpecifiers);//�������� �� ������������ �������������� ������� ���������

		//����� �������������, ����������� ���������� �����
		while(k.hasNext() && !isName)
		{
			QString curSpecif = k.next();//������� ������������

			//���� ����� �������� � ������������
			if(curSpecif.contains(context.words[wordPos]->word))
				//� �� �������� ������ ���������
				if(tmp.name != context.words[wordPos]->word)
				{
					//�������� �������� ��������� � ������ ���������� � �������� ���������������
					canTold.insert(tmp);
				}
		}
	}

	//���� ��������� - ���
	if(isName)
	{
		//������� ���� ����������, ����� ����, ��� ��� �������
		QSetIterator<person> p(person::allPersons);
		while(p.hasNext())
		{
			person t1 = p.next();
			if(t1.name != context.words[wordPos]->word)
				speakersList << t1;
		}
		//��������� �������
		return;
	}

	//������ ��������� � ���������� ����������� ��������������, ���� ����� ������ � � ���� ���������� �� ����
	clearSetByUnikSpec(canTold, startPunct, endPunct, context);

	QSet<person> canBe;//������ ���������� � �������������, �� �������� ���������������
	QSetIterator<person> k(canTold);
	int maxLenOfSpec = 0;//������������ ����� ������������� � ���������� ����

	//����� ���������� � ����������� ���������������
	while (k.hasNext())
	{
		person tmp = k.next();//������� ��������
		QStringListIterator l(tmp.canUseSubjSpecifiers);

		//�������� ���� �������������� ���������
		while(l.hasNext())
		{
			QString curSpecif = l.next();//������� ������������
			cmparing cmpRes = canBeDone(curSpecif, context, startPunct, wordPos, endPunct);//�������� �� ��������� ����������

			//���� �������� ����� ���������� ������������
			if(cmpRes == EQUE)
			{
				//� ����� ������������� �� ������ ������������
				if(curSpecif.split(QString(" ")).size() >= maxLenOfSpec)
				{
					//���� ����� ������������� ������ ������������
					if(curSpecif.split(QString(" ")).size() > maxLenOfSpec)
					{
						//��������� ����� ������������ �����
						maxLenOfSpec = curSpecif.split(QString(" ")).size();
						//�������� ������ ���������� � ������ ���������
						speakersList.clear();
					}
					//�������� �������� ��������� � ��������� ������� ����������
					speakersList << tmp;
				}

			} else if(cmpRes == PARTIC)//���� ���������� ���������
			{
				//�������� ��������� �� ��������� ���������� ����������
				canBe << tmp;
			}
		}
	}

	//���� ��� �� ������ ��������� � ������ �����������
	if(speakersList.isEmpty())
	{
		//������� ��������� ���������� � ��������� �����������
		QSetIterator<person> m(canBe);
		while (m.hasNext())
		{
			speakersList << m.next();
		}
	} 
}

void substructSubjectByObjQualifier (int mode, int wordID, int sentID, sentence & context, QSet  <person> & speakersList)
{
	QList<StanfordWord*>::Iterator i = context.words.begin();//�������� �� ������ � �����������
	int pos = -1;//������ ����� � �����������
	bool isFindWord = false;//����� ������� � �����������
	
	//����� ����� � ��������� �����������
	for(i; i != context.words.end() && !isFindWord; i++)
	{
		if((*i)->wordID == wordID && (*i)->sentID == sentID && (*i)->type == WORD)
			isFindWord = true;
		pos++;
	}

	i--;

	//����� �� ���� ����������
	for(QSet<person>::Iterator j = person::allPersons.begin(); j != person::allPersons.end(); j++)
	{
		//�� ������� ������������ ������������� ������� ���������
		for(QStringList::const_iterator k = j->objSpecifiers.constBegin(); k != j->objSpecifiers.constEnd(); k++)
		{
			QStringList a = k->split(QString(" "));
			int wordCount = a.size();//���������� ���� � �������������

			//���� ��� ������� ���� ������ �������� ������� ������ ������������ � ������� �����������(������� � ��������� �����)
			if(mode == 0 && wordCount < context.size() - pos || mode == 1 && wordCount <= pos+1)
			{
				bool perHasSpecifier = true;//������ ������������ ��������� - �������
				if(mode == 0)
				{
					//�������� ������� ����� ������������� ��������� ������
					for(int l = 0; l < wordCount; l++)
					{
						//���� ������ ����� ������������� ��������� �� ������������� ����� ��� ����� �� ���������
						if(context.words[l+pos]->word != a[l] && context.words[l+pos]->lemma != a[l])
							perHasSpecifier = false;//������ ������������ �� �������� �������
					}
				} else
				{
					//�������� ������� ����� ������������� ��������� �����
					for(int l = 0; l < wordCount; l++)
					{
						//���� ������ ����� ������������� ��������� �� ������������� ����� ��� ����� �� ���������
						if(context.words[pos - l]->word != a[a.size() - 1 - l] && context.words[pos - l]->lemma != a[a.size() - 1 - l])
							perHasSpecifier = false;//������ ������������ �� �������� �������
					}
				}

				//�������� ��������� �� ��������� ��������� ����������
				if(perHasSpecifier)
				{
					speakersList << *j;
				}
			}
		}
	}

	//���� ������ ����� �������� ��������������, � �������� � ����������� ������ - �� ���� ����������
	if(speakersList.isEmpty() && (*i)->isAbjective() && !(*i + 1 - 2*mode)->isPunct())
	{
		//����������� ����� ������� �� ��� �� ����������� ��� ���������� �����
		substructSubjectByObjQualifier (mode, (*i + 1 - 2*mode)->wordID, (*i + 1 - 2*mode)->sentID, context, speakersList);
	}
}

void corefFinder(QMap<QString ,StanfordWord*> & Text, StanfordWord* mainWord, person & per)
{
	QVectorIterator<corefCoord> i(mainWord->corefs);//�������� �� ������� ������ � �����
	bool hasName = false;//����� �� ����� - ��� ���������

	//���� �� ������� ��� ��������� � ������ ��� �� �������� ��� �����
	while(i.hasNext() && !hasName)
	{
		corefCoord tmp = i.next();//������� �����
		QString word = Text.value(QString::number(tmp.sentenceID)+QString(" ")+QString::number(tmp.rootID))->word;//���� ���������� ����� � ����� ������

		//����� ��������� � �������� ������
		QSetIterator<person> j(person::allPersons);
		while(j.hasNext() && !hasName)
		{
			person currentPer = j.next();
			//���� ������ �������� � ������ ������
			if(currentPer.name == word)
			{
				//������� ������� ���������
				per = currentPer;
				return;
			}
		}
	}

	//������� ������� ���������
	per = person();
}

void appealSearch(sentence & context, QVector< corefCoord > & appealCoords)
{
	int wordCount = context.size();//���������� ���� � �����������
	
	QSet<QString> oficialAppeals;//����������� ���������
	oficialAppeals.insert(QString("Mr."));
	oficialAppeals.insert(QString("Sir"));
	oficialAppeals.insert(QString("Esq."));
	oficialAppeals.insert(QString("Mrs."));
	oficialAppeals.insert(QString("Miss"));
	oficialAppeals.insert(QString("Madam"));
	oficialAppeals.insert(QString("Ms."));
	oficialAppeals.insert(QString("you"));
	oficialAppeals.insert(QString("You"));

	int nonAbjCount = 0;//������� �� �������������� �� ��������� ����� ������� ����������
	corefCoord currentAppeal;//������� ���������
	currentAppeal.sentenceID = context.words[0]->sentID;
	currentAppeal.startID = 1;//������ ������� �����

	//��� ������� ����� � �����������
	for(int i = 0; i < wordCount; i++)
	{
		//���� ����� - ���� ����������...
		if(context.words[i]->isPunct())
		{
			//...� ����� ��� ���� ����� ���� �� ��������������
			if(nonAbjCount == 1)
			{
				//��������� ������ ����� ��� ���������
				currentAppeal.endID = context.words[i]->wordID;
				appealCoords.push_back(currentAppeal);
			}
			//�������� ������� ����� ����� ��������� �����
			currentAppeal.startID = context.words[i]->wordID + 1;
			//���������� �� �������������� ��������
			nonAbjCount = 0;
		}

		//���� ����� - ��������������� � �� ����������� ���������
		if((context.words[i]->isNoun()) && !oficialAppeals.contains(context.words[i]->word))
		{
			//��������� ������� �� ��������������
			nonAbjCount++;
			//�������� �������� ����� ����� �� ������ ���������������
			currentAppeal.rootID = context.words[i]->wordID;
		}

		//���� ����� - ������
		if(context.words[i]->isVerb())
		{
			//������ ����� �� ����� ���� ����������
			nonAbjCount+=2;
		}
	}
}

void substructSubject(sentence & Text, QSet <person> & possibleSpeakers)
{
	int pos = -1;//������ ����� � �����������

	//���� �������� ����� �� ���� ������ � �����������
	for(QList<StanfordWord*>::Iterator i = Text.words.begin(); i != Text.words.end(); i++)
	{
		pos++;
		//���� �������� ����� �������
		if(sentence::keys.contains((*i)->lemma))
		{
			QSet  <person> a;
			//���� ������� �� �������
			if(pos != 0)
			{
				//����� �� ����������� �������������� ����� �� �����
				i--;
				StanfordWord* ccc = (*i);
				substructSubjectByObjQualifier(1, ccc->wordID, ccc->sentID, Text, a);
				i++;
			}
			
			//���� ����� �� ������ ��������� � ����������� ����� �� ���������
			if(a.isEmpty() && pos != Text.size() - 1)
			{
				//����� �� ����������� �������������� ������ �� �����
				i++;
				StanfordWord* ccc = (*i);
				substructSubjectByObjQualifier(0, ccc->wordID, ccc->sentID, Text, a);
				i--;
			}

			//���������� ��������� ���������� � �������������� ���������
			QSetIterator<person> j(a);
			while(j.hasNext())
				possibleSpeakers << j.next();
		}
	}
}

void expansionCharacterZone (textTree *  treeText)
{
	int currentChild = 0;//������ ���������� ������� ������
	int chidrenInParagraph = 0;//���������� �������� � ������
	int sentCount = treeText->mainText.size();//���������� �����������
	QString lastStartingTag, lastEndingTag;//������ ��� ������ � ������� ��� �����

	//��������� ����� � ������ �����������
	for(int i = 0; i < sentCount; i++)
	{
		//��������� ������� ������� �����������
		currentChild = 0;
		int narrativeSize = treeText->mainText[i].size();//����� �����������

		//����� �������� � ������ ����� ����������� 
		for(int j = 0; j < narrativeSize; j++)
		{
			//���� ������� ������
			if(treeText->mainText[i].words[j]->type == CHILD)
			{
				QString tmpStartingTag, tmpEndingTag;//��������� ��� ������ � ����� ��� �������

				//�������� ���� �������
				getTag(treeText->mainText[i].childList[currentChild], tmpStartingTag, tmpEndingTag);

				//������� � ���������� �������
				currentChild++;

				//���� � ������ ������� �� �������� ��������
				if(tmpStartingTag == QString("<span style=\"color:\#0000ff\">"))
				{
					//� ��� ��� ����������� ���������� � ������
					if(lastStartingTag == QString())
					{
						//���������������� ���������� ������������� ����� � ������
						chidrenInParagraph++;
					} else
					{
						//���������� ���� ��� ������� �������
						treeText->mainText[i].childList[currentChild-1]->startingTag = lastStartingTag;
						treeText->mainText[i].childList[currentChild-1]->endingTag = lastEndingTag;
					}
				}else //������ ����������� ��������� ��������
				{
					//��������� ���� ������� �������
					lastStartingTag = tmpStartingTag;
					lastEndingTag = tmpEndingTag;

					// ���� �� ���� � ������ ���� ������������� ���������
					if(chidrenInParagraph!=0)
					{
						int currSent = i;//������� �����������
						chidrenInParagraph++;

						//��� ���� �������������� ������������� �������� � ������ ������������ ����
						for(int k = currentChild - 1; k <= 0 && chidrenInParagraph != 0; k--)
						{
							treeText->mainText[currSent].childList[k]->startingTag = lastStartingTag;
							treeText->mainText[currSent].childList[k]->endingTag = lastEndingTag;
							chidrenInParagraph--;

							//���� ���������� ������������� �������� � ������ �� ����� 0, � � ����������� ��� ������������� ��������
							if(chidrenInParagraph != 0 && k == 0)
							{
								//��������� � ���������� �����������, ���������� ��������
								currSent--;
								while(treeText->mainText[currSent].childList.size() == 0)
									currSent--;
								k = treeText->mainText[currSent].childList.size();
							}
						}
					}
				}
			}else if(treeText->mainText[i].words[j]->word == QString("</p>"))//���� ���������� ����� ������
			{
				//��������� ���� ������������ ��� ������ �������������
				chidrenInParagraph = 0;
				currentChild = 0;
				lastStartingTag = QString();
				lastEndingTag = QString();
			}
		}
	}
	
}

bool addressFinder(sentence & context, QSet  <person> & possibleSpeakers)
{
	QList<QSet<person>> allRes;//��� ���������� ������
	QVector< corefCoord > appealCoords;//���������� ���������

	//����� ��������� ���������
	appealSearch(context, appealCoords);

	//��������� ��������� �� ������� ���������
	for(QVector<corefCoord>::Iterator i = appealCoords.begin(); i != appealCoords.end(); i++)
	{
		QSet<person> a;
		substructSubjectBySubjQualifier ((*i).rootID, (*i).sentenceID, context, a);
		if(!a.isEmpty())
		{
			allRes << a;
		}
	}

	//���� ������� ��������� �������� �� ������������ ��������������
	if(allRes.size() > 1)
	{
		//������� ������������ ������ ���������
		QSet<person> a;
		QSetIterator<person> k(allRes[0]);
		while(k.hasNext())
		{
			a << k.next();
		}

		//�������� ����������� ���� ��������
		for(int r = 0; r < allRes.size(); r++)
		{
			QSet<person> res;
			TwoSetHavePersons(a, allRes[r], res);
			a.clear();
			QSetIterator<person> n(res);
			while(n.hasNext())
			{
				a << n.next();
			}
		}

		//��������� ���������� ��������� � �����
		QSetIterator<person> o(a);
		while(o.hasNext())
		{
			possibleSpeakers << o.next();
		}
	}else if(allRes.size() == 1)//���� ������ ������ ���� ���������
	{
		//��������� ��� � �����
		QSetIterator<person> o(allRes[0]);
		while(o.hasNext())
		{
			possibleSpeakers << o.next();
		}
	}

	//���� ���� �������� ���������, �� ����� ����(����������� �� ���������)
	if(allRes.size() > 1 && possibleSpeakers.size() == 0)
	{
		//��� ��������
		return false;
	}
	//��������� �� ����
	return true;
}

void inOrderFinder(textTree *  treeText)
{
	int udefinitSent = 0;//���������� ������������� �����������
	bool hasDefinitSent = false;//� ���� ���� ���� �� ���� ����������� �����������
	QString startingTag1, endingTag1;//��� ������ � ��� ����� 
	person per;//������� ��������

	//����� ������� ������������ ������� � ������
	QList<sentence>::iterator i = treeText->mainText.begin();
	for(; i != treeText->mainText.end() && !hasDefinitSent; i++)
	{
		bool hasChild = false;//���� ������� � ����

		//���� ������� ����
		if(!i->childList.isEmpty())
		{
			//�������� ��� � �������� �������
			getTag(i->childList[0], startingTag1, endingTag1);
			hasChild = true;
		}
		//���� ������� �� ��������
		if(hasChild && startingTag1.contains( QString("<span style=\"color:\#0000ff\">")))
		{
			//��������� ���������� ����������� ��� �����������
			udefinitSent++;
		}else if(hasChild)//���� ������� ��������
		{
			//������� ������� ������� ��������
			QString colour;
			colour = startingTag1.split(QString("<span style=\"color:\#"))[1];
			colour = colour.split(QString("\">"))[0];

			//�������� ��������� �� ��� �����
			per = person::personByColour(colour);
			hasDefinitSent = true;
		}	
	}

	i--;
	//���� �� ��� ���� ��� �� ������ ������������ ���������
	if(!hasDefinitSent)
	{
		//��������� ������ �������
		return;
	}
		
	int initCount = 1;//���������� ����������� ��� ������� ��������� �������� ����

	//����������� �� ������� ���� ���������� �����������
	for(QList<sentence>::Iterator j = i; j != treeText->mainText.begin()-1; --j)
	{
		//���� � ����������� ���� �������
		if(!j->childList.isEmpty())
		{
			//��� ������� ������� ��������� ����
			for(int k = 0; k < j->childList.size(); k++)
			{
				j->childList[k]->startingTag = per.generateStartingTag();
				j->childList[k]->endingTag = QString("</span>");
				initCount++;
			}
		}
		//���� ��������� ������ ������ ������ � ��������� ���� ���������� ���� ���� �������������
		if(j->words[0]->word == QString("<p>") && initCount != 0)
		{
			//����� ��������� �� ������� �� ������
			person per1;
			QList<person> p = person::allPersons.toList();
			per1 = p[0];
			if(per != per1)
			{
				per = per1;
			}
			else
			{
				per = p[1];
			}
			
			initCount = 0;
		}
	}

	//������������ ���������� ������ �� �������� ���������
	initCount = 1;

	//��� ���� ����������� ������ �� ������������
	for(QList<sentence>::Iterator j = i; j != treeText->mainText.end(); j++)
	{
		//���� �������� � ����������� �� ��������
		if(!j->childList.isEmpty() && j->childList[0]->startingTag == QString("<span style=\"color:\#0000ff\">"))
		{
			//��� ������� ������� ������������ ���������, ����������� �������
			for(int k = 0; k < j->childList.size(); k++)
			{
				j->childList[k]->startingTag = per.generateStartingTag();
				j->childList[k]->endingTag = QString("</span>");
				initCount++;
			}
		} else if(!j->childList.isEmpty())//���� �������� ��������
		{
			//�������� ��������� �� ����
			QString colour;
			colour = j->childList[0]->startingTag.split(QString("<span style=\"color:\#"))[1];
			colour = colour.split(QString("\">"))[0];
			person per1 = person::personByColour(colour);

			//���� ���������� �������� �� �������� �������
			if(per1 != per)
			{	
				//���������� ����������� ��������� �������
				per = per1;
			}
			initCount = 1;//��� ��� ��� ���� ���� ����������� �����������
		}

		//���� ��������� ��� ����� ������
		if(j->words[j->words.size()-1]->word == QString("</p>") && initCount != 0)
		{
			//������� ��������� �� ������� �� ������
			person per1;
			QList<person> p = person::allPersons.toList();
			per1 = p[0];
			if(per != per1)
			{
				per = per1;
			}
			else
			{
				per = p[1];
			}
			
			//��� ���� �� ���� ��������� �� ������ �����������
			initCount = 0;
		}
	}
}

void textMarkup(QMap<QString ,StanfordWord*> & Text, textTree *  treeText, QSet  <person> & PossibleSpeakers)
{
	//����������� ����� ������� ��� ������� ������ � ����� �������������� ������� ������ � ������ � ������ ������ ��������� ���������� �� ������������ �������������� ��� ������� ������
	bool lowerLewel = true;//� ������� ���� ��� ��������
	
	//��� ������� ����������� � ����
	for(QList<sentence>::Iterator i = treeText->mainText.begin(); i != treeText->mainText.end(); i++)
	{
		bool hasConflict = false;//��������� �� ���������
		QSet<person> b;//��������� �� ����������� ��������������
		
		//���� ����������� ������� �������� �������
		if(i->hasOnlyChilds())
		{
			//���� ��� �� ���������
			if(i+1!=treeText->mainText.end())
			{
				//�������� ���������, ��������� ��������� �����������
				substructSubject(*(i+1), b);
			}
		}else if(i->size() != 1)//���� � ����������� �� ������ �������
		{
			//�������� �������� �� ������� �����������
			substructSubject(*i, b);
		}
		
		QList<QSet<person>> allPossibleSpeakersLowLevel;//����� ��������� ����� ���� � ������ ���� �� ������ ������������ �������������� ������� ������
	
		//��� ������� ������� � ����������� ����� �� ������������ ��������������
		for(QList<textTree* >::Iterator j = i->childList.begin(); j != i->childList.end(); j++)
		{
			lowerLewel = false;
			QSet  <person> a;
			// ��������� ���������� � ������ ����������� ������
			textMarkup(Text, *j, a);

			//���� ��������� ���� ��������
			if(!a.isEmpty())
				allPossibleSpeakersLowLevel << a;//��������� � ����� �� ������������� �������������
		}
		
		QSet<person> a;//����������� ������� ���������� �� ������������ ��������������

		//���� ���� ������� ��������� �� ������������ ��������������
		if(!allPossibleSpeakersLowLevel.isEmpty())
		{
			//������� ������� ��������� ������������
			QSetIterator<person> k(allPossibleSpeakersLowLevel[0]);
			while(k.hasNext())
			{
				a << k.next();
			}

			//�������� ��� � ������ ����������� � ������
			for(int r = 0; r < allPossibleSpeakersLowLevel.size(); r++)
			{
				QSet<person> res;
				TwoSetHavePersons(a, allPossibleSpeakersLowLevel[r], res);
				a.clear();
				QSetIterator<person> n(res);
				while(n.hasNext())
				{
					a << n.next();
				}
				//���� ����������� ���� ������ ������
				if(a.size() == 0)
				{
					//������ ��������
					hasConflict = 1;
				}
			}
		}
		
		//���� ���� ���� ���� �������� ������ �� ����
		if(!(a.isEmpty() && b.isEmpty()))
		{
			//���� �� ����������� �������������� ������ ������ ���� ��������
			if(b.size() == 1)
			{
				//��������� �������� ��� ������, ��������� ������������
				for(QList<textTree*>::Iterator o = i->childList.begin(); o != i->childList.end(); o++)
				{
					QList<person> tmp = b.toList();
					(*o)->startingTag = tmp[0].generateStartingTag();
					(*o)->endingTag = QString("</span>");
				}
			} else if(a.size() == 1 && b.size() == 0)//���� �� ������� �� ������ ��������� �� ����������� ��������������, � �� ������������ ����������� ���� ������ ���������
			{
				//��������� ���� �������� �� ����� ���������, ����������� �� ������������ ��������������
				for(QList<textTree*>::Iterator o = i->childList.begin(); o != i->childList.end(); o++)
				{
					QList<person> tmp = a.toList();
					(*o)->startingTag = tmp[0].generateStartingTag();
					(*o)->endingTag = QString("</span>");
				}
			}else  //������, ��� ������� ����� ������������ ����� �� ������ ��� ������ ���������
			{
				QSet<person> fullPersonsList;//������ ����������, ������� �������� ���������� ��� ������� ����������� �� ���� ���������

				//���� ��� ��������� �� �����
				if(!a.isEmpty() && !b.isEmpty())
				{
					//�������������� ��������� = ����������� ���� ��������
					TwoSetHavePersons(a, b, fullPersonsList);
				}
				
				//���� ���� ������ ������ �� ������������ ��������������
				if(!a.isEmpty() && b.isEmpty())
				{
					//�������������� ��������� = ��������� �� ������������ ��������������
					QSetIterator<person> o(a);
					while(o.hasNext())
						fullPersonsList << o.next();
				}

				//���� ���� ������ ������ �� ����������� ��������������
				if(a.isEmpty() && !b.isEmpty())
				{
					//�������������� ��������� = ��������� �� ����������� ��������������
					QSetIterator<person> o(b);
					while(o.hasNext())
						fullPersonsList << o.next();
				}

				//���� �� ����������� �������� ��� �� ������ ���������
				if(fullPersonsList.size() == 0)
				{
					//������ �������� ����������� � ������������ ��������������
					for(QList<textTree*>::Iterator o = i->childList.begin(); o != i->childList.end(); o++)
					{
						(*o)->startingTag = QString("<u><span style=\"color:\#0000ff\">");
						(*o)->endingTag = QString("</span><u>(CONFLICT)");
					}
				} else if(fullPersonsList.size() == 1)//���� ����������� ���� ����� ������ ���������
				{
					//��������� �������� ������ ���������
					for(QList<textTree*>::Iterator o = i->childList.begin(); o != i->childList.end(); o++)
					{
						QList<person> tmp = fullPersonsList.toList();
						(*o)->startingTag = tmp[0].generateStartingTag();
						(*o)->endingTag = QString("</span>");
					}
				} else//���� �������� ��������� ����������
				{
					person per;

					//���� ������ ����������� ������� ������ �� ��������
					if(i->size() == 1 && i->words[0]->type == CHILD)
					{
						//���� ������ ����������� �� ���������
						if(i+1!=treeText->mainText.end())
						{
							//���� ��������� �� ������ � ��������� �����������
							corefFinderInSentence(Text, *(i+1), per);
						}
					}else if(i->size() != 1)//����� ���� � ������ �����������
					{
						corefFinderInSentence(Text, *i, per);
					}

					//���� ��������, ���������� �� ����� ���������� � ���������� ���������
					if(fullPersonsList.contains(per))
					{
						//��������� �������� ������ ���������
						for(QList<textTree*>::Iterator o = i->childList.begin(); o != i->childList.end(); o++)
						{
							(*o)->startingTag = per.generateStartingTag();
							(*o)->endingTag = QString("</span>");
						}
					} else
					{
						//���������� ���� ��� ���������� ����������
						QString startTag = QString("<u><span style=\"color:\#0000ff\">");
						QString endTag = QString("</span></u>(");
						QList<person> perList = fullPersonsList.toList();
						endTag+=perList[0].name;
						for(int p = 1; p < perList.size(); p++)
						{
							endTag+= QString(", ") + perList[p].name;
						}
						endTag+= QString(")");

						//��������� ������������� ������ ��������
						for(QList<textTree*>::Iterator o = i->childList.begin(); o != i->childList.end(); o++)
						{
							(*o)->startingTag = startTag;
							(*o)->endingTag = endTag;
						}
					}
					
				}

			}
		}else if(hasChildConflict(i->childList))//���� ��� ������ ����� � �������� �������� �� ��������� �� ������������� �������������
		{
			//��������� �������� ����������� ���������
			for(QList<textTree*>::Iterator o = i->childList.begin(); o != i->childList.end(); o++)
			{
				(*o)->startingTag = QString("<u><span style=\"color:\#0000ff\">");
				(*o)->endingTag = QString("</span><u>(CONFLICT)");
			}
		}
	}

	//��������� ���� ���������� � ������ ������
	expansionCharacterZone (treeText);

	//������� ������ ���������� �� ������������ �������������� ������� ������ ���� �� �� �������
	if(!treeText->highLevel)
	{
		QList<QSet<person>> allPersonsBySubjSpecifiers;//��� ��������� �� ������������ ��������������

		//� ������ ����������� �������� ������ �� ������������ ��������������
		for(QList<sentence>::Iterator i = treeText->mainText.begin(); i != treeText->mainText.end(); i++)
		{
			QSet<person> a;
			addressFinder(*i, a);
			if(!a.isEmpty())
				allPersonsBySubjSpecifiers << a;
		}

		//���� ��������� ���� �������
		if(allPersonsBySubjSpecifiers.size() > 0)
		{
			//�������� ��� ����� ����������� ���� ���������� ��������
			QSet<person> a;
			QSetIterator<person> k(allPersonsBySubjSpecifiers[0]);
			while(k.hasNext())
			{
				a << k.next();
			}

			for(int r = 0; r < allPersonsBySubjSpecifiers.size(); r++)
			{
				QSet<person> res;
				TwoSetHavePersons(a, allPersonsBySubjSpecifiers[r], res);
				a.clear();
				QSetIterator<person> n(res);
				while(n.hasNext())
				{
					a << n.next();
				}
			}

			QSetIterator<person> o(a);
			while(o.hasNext())
				PossibleSpeakers << o.next();
		}
		
		return;
	}


}

void getTag(textTree *  treeText, QString & startingTag, QString & endingTag)
{
	//�������� ��� ������
	startingTag = treeText ->startingTag;
	//�������� ��� �����
	endingTag = treeText ->endingTag;
}

cmparing canBeDone(QString specifier, sentence & context, int leftPunct, int wordPos, int rightPunct)
{
	QStringList specRef = specifier.split(QString(" "));//������������, �������� �� ������
	QStringListIterator i(specRef);
	bool hasWord = false;//������� ��������� �����
	
	//����� ��������� ����� � ������ �������������
	while(i.hasNext() && !hasWord)
	{
		if(i.next() == context.words[wordPos]->word)
		{
			hasWord = true;
		}
	}
	
	//���� ����� �� �������
	if(!hasWord)
	{
		//������� ������������
		return NOTEQUE;
	}

	i.previous();
	int t = wordPos;
	//�������� �� ����� �����
	while(i.hasPrevious() && t != leftPunct)
	{
		i.previous();
		t--;
	}

	//���� � ������������� ���� ����� �� ��������� ������ ��� ����� � ����������� �� ����������� �� ����������
	if(i.hasPrevious() && t == leftPunct)
	{
		//������� ��������� ����������
		return PARTIC;
	}

	int it = 0;
	//�������� �� ���������� ���� ����� �������
	while(it < specRef.size() && t < rightPunct)
	{
		QString tmp = specRef[it];
		//���� ������� ����� �� ��������� �� � ������ �� �� ������
		if(tmp != context.words[t]->word && tmp != context.words[t]->lemma)
		{
			//������� ��������� ����������
			return PARTIC;
		}
		it++;
		t++;
	}
	//���� ����� �� ������� �����, �� � ������������� ���� ��� �����
	if(t == rightPunct && it < specRef.size())
	{
		//������� ��������� ����������
		return PARTIC;
	}
	//������� ������ ����������
	return EQUE;
}

void TwoSetHavePersons(QSet<person> & a, QSet<person> & b, QSet<person> & res)
{
	res.clear();//�������� ���������� ����������

	//��������� ������� ������� �� ������ a � b
	for(QSet<person>::Iterator i = a.begin(); i != a.end(); i++)
	{
		//���� ������� ������������
		if(b.contains(*i))
		{
			//�������� ������ ���� � res
			res << (*i);
		}
	}
}

void corefFinderInSentence(QMap<QString ,StanfordWord*> & Text, sentence & context, person & per)
{
	int pos = -1;//������� ����� � �����������

	//����� ��������� �����
	for(QList<StanfordWord*>::Iterator i = context.words.begin(); i != context.words.end(); i++)
	{

		pos++;
		//���� ������� �������� �����
		if(sentence::keys.contains((*i)->lemma))
		{
			//���� �������� ����� �� � ������� �������
			if(pos != 0)
			{
				//����� ��������� �� ����� �� ����� ����� �� ���������
				corefFinder(Text, context.words[pos - 1] , per);

				//���� �������� ������
				if(!per.name.isEmpty())
				{
					//������� ���������
					return;
				}
			}
			//���� �������� ����� �� ���������
			if(pos != Text.size() - 1)
			{
				//����� ��������� �� ����� �� ����� ������ �� ���������
				corefFinder(Text, context.words[pos + 1] , per);
				//������� ���������
				return;
			}
		}
	}
}

bool input(QString XMLfile, QString HTMLfile, QString DOTfile, textTree** treeText, QMap<QString ,StanfordWord*> & mapText, QList<StanfordWord *> & markedText)
{
	//�������� ������� ����� � ������
	QTextStream cout(stdout);
	QTextStream cin(stdin);

	QStringList XMLtext, DOTtext;//����� ������� � ���������� � ������� �������������� ������

	//�������� ����� �� ����� �������
	bool hasFile = getTextFileByLines(XMLfile, XMLtext);

	//���� ���� �� ������
	if(!hasFile)
	{
		//������� ������ � ������� �������
		cout << "Input is incorrect: text parsing is not found"<<endl;
		return false;
	}

	//�������� �� ������ ����� ������� ����� ������ � ���� ���������� �� ���������� �����
	parseXML(XMLtext, mapText, markedText);

	QString XMLText;

	int quotationCountInXML = 0;
	//��������� ������ �������� ����� �� ������� � ��� ����
	for(QList<StanfordWord*>::Iterator i = markedText.begin(); i != markedText.end(); i++)
	{
		if((*i)->wordPOS != quotation)
		{
			XMLText += (*i)->word;//�������� ����� ������ �� ������� �������
		}
		else
		{
			quotationCountInXML++;//�� ��������� �������, �� ������� ���������� � ������
		}
		//���� ��� ������
		if((*i)->word.contains(QString("<span style=\"color:#0000ff\">")) || (*i)->word.contains(QString("</span>")) || (*i)->word.contains(QString("<p>")) || (*i)->word.contains(QString("</p>")))
		{
			//������� ������ � ������� �������
			cout << "Input is incorrect: invalid XML file. Please don't use tags in text parsed by StanfordCoreNLP."<<endl;
			return false;
		}
	}
	
	QString HTMLtext;//HTML ���� � ������� ������

	//�������� ����� �� ����� HTML
	hasFile = getTextFileByLine(HTMLfile, HTMLtext);

	//���� ���� �� ������
	if(!hasFile)
	{
		//������� ������ � ������� �������
		cout << "Input is incorrect: text not found"<<endl;
		return false;
	}

	//�������� �� ������������ XML � HTML
	
	QString HTMLTextTMP = HTMLtext;
	//�������� HTML �� ��������
	int j = 0;
	while ((j = HTMLTextTMP.indexOf(" ", 0)) != -1) 
	{
		HTMLTextTMP.remove(HTMLTextTMP.indexOf(" ", 0), 1);
	}

	//�������� HTML �� ���������
	while ((j = HTMLTextTMP.indexOf("	", 0)) != -1) 
	{
		HTMLTextTMP.remove(HTMLTextTMP.indexOf("	", 0), 1);
	}

	//�������� HTML �� ���������
	while ((j = HTMLTextTMP.indexOf("\n", 0)) != -1) 
	{
		HTMLTextTMP.remove(HTMLTextTMP.indexOf("\n", 0), 1);
	}

	//�������� HTML �� ��������
	while ((j = HTMLTextTMP.indexOf("<", 0)) != -1) 
	{
		int posTag = HTMLTextTMP.indexOf("<", 0);
		while(HTMLTextTMP[posTag] != QChar('>'))
		{
			HTMLTextTMP.remove(posTag, 1);
		}
		HTMLTextTMP.remove(posTag, 1);
	}

	//��������� �������(� ������� ������� � HTML) � �� ���������
	int quotationCountInHTML = 0;
	while ((j = HTMLTextTMP.indexOf("\'", 0)) != -1) 
	{
		HTMLTextTMP.remove(HTMLTextTMP.indexOf("\'", 0), 1);
		quotationCountInHTML++;
	}

	while ((j = HTMLTextTMP.indexOf("\"", 0)) != -1) 
	{
		HTMLTextTMP.remove(HTMLTextTMP.indexOf("\"", 0), 1);
		quotationCountInHTML++;
	}

	//�������� XML �� ��������
	while ((j = XMLText.indexOf(" ", 0)) != -1) 
	{
		XMLText.remove(XMLText.indexOf(" ", 0), 1);
	}

	//��������� ������� �� ������� ������� � XML � �� ���������
	while ((j = XMLText.indexOf("\'", 0)) != -1) 
	{
		XMLText.remove(XMLText.indexOf("\'", 0), 1);
		quotationCountInXML++;
	}

	while ((j = XMLText.indexOf("\"", 0)) != -1) 
	{
		XMLText.remove(XMLText.indexOf("\"", 0), 1);
		quotationCountInXML++;
	}
	
	if(quotationCountInXML != quotationCountInHTML || XMLText != HTMLTextTMP)
	{
		//������� ������ � ������� �������
		cout << "Input is incorrect: mismatch of HTML and XML files"<<endl;
		return false;
	}

	//�������� ������ �� ��������������
	int symbolC = 0;//���������� ��������
	int numberC = 0;//���������� �����

	//����������� ������������� ���������� �������(��������� ����)
	for(int i = 0; i < HTMLtext.size(); i++)
	{
		//���� ������� �����
		if(HTMLtext[i].isDigit())
		{
			//���� � ������ ������ ����������� �����
			while(HTMLtext[i].isDigit() && i < HTMLtext.size())
			{
				//��������� �������� �������� � �����
				symbolC++;
				numberC++;
				//������� � ���������� �������
				i++;
			}
		}

		//���� ��������� �������� ����
		if(HTMLtext[i] == QChar('<'))
		{
			//���������� ���
			while(HTMLtext[i]!=QChar('>') && i < HTMLtext.size())
			{
				i++;
			}
		}
		//��������� ������� ��������
		symbolC++;
	}

	//���� ����� ����� 50%
	if((double)numberC/(double)symbolC > 0.5)
	{
		//������� ������ � ������� �������
		cout << "Input is incorrect: the text is not artistic"<<endl;
		return false;
	}
	
	//�������� ������ ������� ������ � ������ �����
	TextFromParsingAndHTML(markedText, HTMLtext, treeText);

	//�������� ������ DOT �����
	hasFile = getTextFileByLines(DOTfile, DOTtext);
	if(!hasFile)
	{
		//������� ������ � ������� �������
		cout << "Input is incorrect: the list of names of characters is not found"<<endl;
		return false;
	}

	int str;//������ � �������������� ������� � ������ DOT

	//��������� ������ DOT ����� �� ������
	DOTERRORS error = checkDOTerrors(DOTtext, &str);

	//���� ������� ������
	if(error != NO_ERRORS)
	{
		//������� ������ � ������� �������
		showError(error, str);
		return false;
	}

	//�������� ��������� ���������� �� ������ DOT �����
	parseDOT(DOTtext);

	//�������� ���� �� ����� �������� ����
	QStringList keysText;
	QString keys = QCoreApplication::applicationFilePath();
	while(keys[keys.size()-1]!=QChar('/'))
	{
		keys.remove(keys.size()-1, 1);
	}
	keys+=QString("keys.txt");

	//�������� ������ ����� �������� ����
	hasFile = getTextFileByLines(keys, keysText);

	//���� ���� �� ������� �������
	if(!hasFile)
	{
		//������� ������ � ������� �������
		cout << "Error: No keywords found"<<endl;
		return false;
	}

	//������� ������ ������ � ����� ������� ��������� �����(\n)
	for(QStringList::Iterator i = keysText.begin(); i!=keysText.end(); i++)
	{
		i->remove(i->size()-1,1);
	}

	//���������������� �������� �����
	sentence::InitKeys(keysText);
}

partOfSpeech strToPOS(QString & _POS)
{
	//��������� ���������� ������ �� ���������� � �����������
	if(_POS == QString("CC")) return CC;
	if(_POS == QString("CD")) return CD;
	if(_POS == QString("DT")) return DT;
	if(_POS == QString("EX")) return EX;
	if(_POS == QString("FW")) return FW;
	if(_POS == QString("IN")) return IN;
	if(_POS == QString("JJ")) return JJ;
	if(_POS == QString("JJR"))return JJR;
	if(_POS == QString("JJS"))return JJS;
	if(_POS == QString("LS")) return LS;
	if(_POS == QString("MD")) return MD;
	if(_POS == QString("NN")) return NN;
	if(_POS == QString("NNS")) return NNS;
	if(_POS == QString("NNP")) return NNP;
	if(_POS == QString("NNPS")) return NNPS;
	if(_POS == QString("PDT")) return PDT;
	if(_POS == QString("POS")) return POS;
	if(_POS == QString("PRP")) return PRP;
	if(_POS == QString("PP$")) return PPdollar;
	if(_POS == QString("PRP$")) return PRPdollar;
	if(_POS == QString("RB")) return RB;
	if(_POS == QString("RBR")) return RBR;
	if(_POS == QString("RBS")) return RBS;
	if(_POS == QString("RP")) return RP;
	if(_POS == QString("SYM")) return SYM;
	if(_POS == QString("TO")) return TO;
	if(_POS == QString("UH")) return UH;
	if(_POS == QString("VB")) return VB;
	if(_POS == QString("VBD")) return VBD;
	if(_POS == QString("VBG")) return VBG;
	if(_POS == QString("VBN")) return VBN;
	if(_POS == QString("VBP")) return VBP;
	if(_POS == QString("VBZ")) return VBZ;
	if(_POS == QString("WDT")) return WDT;
	if(_POS == QString("WP")) return WP;
	if(_POS == QString("WP$")) return WPdollar;
	if(_POS == QString("WRB")) return WRB;
	if(_POS == QString("#")) return PoundSign;
	if(_POS == QString("$")) return DollarSign;
	if(_POS == QString("."))return SentFinPunct;
	if(_POS == QString(",")) return Comma;
	if(_POS == QString(":")) return Colon;
	if(_POS == QString("``")) return quotation;
	if(_POS == QString("''")) return quotation;
	if(_POS == QString("-LRB-")) return LRB;
	if(_POS == QString("-RRB-")) return RRB;
}

bool getTextFileByLines(QString & filename, QStringList & text)
{
	//������� ����
	QFile file(filename);
    if(file.open(QIODevice::ReadOnly |QIODevice::Text))
    {
		//������� ��������� ����
        while(!file.atEnd())
        {
            //������ ������
            QString str = file.readLine();

            text << str;
        }
    }
    else //���� �� �����
    {
		//������� ������
        return false;
    }
	//������� ���������� ��������
	return true;
}

void parseXML(QStringList & XMLtext, QMap<QString ,StanfordWord*> & mapText, QList<StanfordWord*> & markedText)
{
	//���������� ������ �����
	for(QStringList::Iterator i = XMLtext.begin(); i != XMLtext.end(); i++)
	{
		//���� ������ ��� ����� �����������
		if(i->contains(QString("<sentence id=")))
		{
			//��������� ID �����������
			QStringList tmp = i->split("<sentence id=\"");
			tmp = tmp[1].split("\">");

			int sentID = tmp[0].toInt();//ID �����������

			//���� �� ������ ����� ����������� - ������ ����
			while(!i->contains(QString("</sentence>")))
			{
				//���� ������ ���, ����������� �����
				if(i->contains(QString("<token id=\"")))
				{
					//�������� ID �����
					QStringList tmp1 = i->split("<token id=\"");
					tmp1 = tmp1[1].split("\">");
					QString word, lemma;//����� � �����
					int wordID = tmp1[0].toInt();//ID �����
					partOfSpeech wordPartOfSpeech;//����� ����

					//���� �� ������ ��� �������� ����� - ������ �����
					while(!i->contains(QString("</token>")))
					{
						//���� ������ ��� �����
						if(i->contains(QString("<word>")))
						{
							//�������� �����
							word = i->split("<word>")[1];
							word = word.split("</word>")[0];
						}
						//���� ������ ��� �����
						if(i->contains(QString("<lemma>")))
						{
							//�������� �����
							lemma = i->split("<lemma>")[1];
							lemma = lemma.split("</lemma>")[0];
						}
						//���� ������ ��� ����� ����
						if(i->contains(QString("<POS>")))
						{
							//�������� ����� ����
							QString POS;
							POS = i->split("<POS>")[1];
							POS = POS.split("</POS>")[0];
							wordPartOfSpeech = strToPOS(POS);
						}
						i++;
					}

					//������� ����� �����(���� ��� ������)
					markedText.push_back(new StanfordWord(sentID, wordID, word, lemma, wordPartOfSpeech, WORD, QVector<corefCoord>()));
					mapText.insert(markedText.value(markedText.size()-1)->doKey(), markedText.value(markedText.size()-1));
				}
				i++;
			}
		}
		//���� ����� ������������� ��� ������
		if(i->contains(QString("<coreference>")))
		{
			QVector<corefCoord> a;
			//���� �� ����� ������������� ��� ������
			while(!i->contains(QString("</coreference>")))
			{
				corefCoord b;
				//���� �� ������ ��� �������� ������ �����
				while(!i->contains(QString("</mention>")))
				{
					//���� ������ ��� ID �����������
					if(i->contains(QString("<sentence>")))
					{
						//�������� ID �����������
						QString sent;
						sent = i->split("<sentence>")[1];
						sent = sent.split("</sentence>")[0];
						b.sentenceID = sent.toInt();
					}
					//���� ������ ��� ID ������ � �����������
					if(i->contains(QString("<start>")))
					{
						//�������� ID ������
						QString start;
						start = i->split("<start>")[1];
						start = start.split("</start>")[0];
						b.startID = start.toInt();
					}
					//���� ������ ��� ID ����� � �����������
					if(i->contains(QString("<end>")))
					{
						//�������� ID �����
						QString end;
						end = i->split("<end>")[1];
						end = end.split("</end>")[0];
						b.endID = end.toInt();
					}
					//���� ������ ��� ID �������� �����
					if(i->contains(QString("<head>")))
					{
						//�������� ID �������� �����
						QString head;
						head = i->split("<head>")[1];
						head = head.split("</head>")[0];
						b.rootID = head.toInt();
					}
					i++;
				}

				a << b;
				i++;
			}

			//���������� ���������� ������ � �����
			for(int j = 0; j < a.size(); j++)
			{
				for(int k = a[j].startID; k < a[j].endID; k++)
				{
					for(int l = 0; l < a.size(); l++)
						mapText[QString::number(a[j].sentenceID)+QString(" ")+QString::number(k)]->corefs << a[l];
				}
			}

		}
	}
}

void parseDOT(QStringList & DOTtext)
{
	//�������� ������ ������ ����������
	person::allPersons.clear();

	//��������� ������ ����������
	QStringList::Iterator i = DOTtext.begin()+1;

	//���� �� ��������� ������ ����
	while(!i->contains(QString("->")) && !i->contains(QString("}")))
	{
		//��������� ����� ���������
		QStringList a = i->split(QString("[label=\""));
		QString name = a[0];

		//������� ����� �� ������ ��������
		while(name[0].isSpace())
		{
			name.remove(0,1);
		}
		while(name[name.size()-1].isSpace())
		{
			name.remove(name.size()-1,1);
		}

		//��������� ������ ����������� ��������������
		a = a[1].split(QString("\"];"));
		a = a[0].split(QString(","));

		//������� ����������� �������������� �� ������ ��������
		for(int j = 0; j < a.size(); j++)
		{
			while(a[j][0].isSpace())
			{
				a[j].remove(0,1);
			}
			while(a[j][a[j].size()-1].isSpace())
			{
				a[j].remove(a[j].size()-1,1);
			}
		}

		//�������� ���������
		person::allPersons << person(name, a, QStringList(), QString());
		i++;
	}

	//���� � ������ ���� ����, ��������� ������������ ��������������
	while(i->contains(QString("->")))
	{
		//��������� ����� ���������
		QStringList a = i->split(QString("->"));
		
		while(a[0][0].isSpace())
		{
			a[0].remove(0,1);
		}
		while(a[0][a[0].size()-1].isSpace())
		{
			a[0].remove(a[0].size()-1,1);
		}
		QString name = a[0];

		//��������� ������ ������������ �������������� ���������
		a = a[1].split(QString("[label=\""));
		a = a[1].split(QString("\"];"));
		a = a[0].split(QString(","));

		//������� ������������ �������������� �� ������ ��������
		for(int j = 0; j < a.size(); j++)
		{
			while(a[j][0].isSpace())
			{
				a[j].remove(0,1);
			}
			while(a[j][a[j].size()-1].isSpace())
			{
				a[j].remove(a[j].size()-1,1);
			}
		}

		//�������� ��������� �� ������ ����������
		person per0 = person::personByName(name);

		person per1 = per0;

		//���������� ������ ������������ �������������� ���������
		for(int j = 0; j < a.size(); j++)
		{
			per1.canUseSubjSpecifiers << a[j];
		}

		//�������� ������ ����� ���������
		person::allPersons.remove(per0);

		//�������� ����� ����� ���������
		person::allPersons << per1;
		i++;
	}
}

bool getTextFileByLine(QString & filename, QString & text)
{
	//������� ����
	QFile file(filename);
    if(file.open(QIODevice::ReadOnly |QIODevice::Text))
    {
		//������� ��������� ���� � ���� ������ ����� �������
        while(!file.atEnd())
        {
            //������ ������
            QString str = file.readLine();

            text +=QString(" ") + str;
        }
    }
    else //���� �� �����
    {
		//������� ������
        return false;
    }
	//������� ���������� ��������
	return true;
}

void colorDistributionSearch() 
{
	QStringList colours;//��������� �����, ������������ �� ����������� �������
	colours /*<< QString("000033")*/
		/*<< QString("000066")*/
		<< QString("000099")
		<< QString("0000CC")
		<< QString("330000")
		<< QString("330033")
		<< QString("330066")
		<< QString("330099")
		<< QString("3300CC")
		<< QString("3300FF")
		<< QString("660000")
		<< QString("660033")
		<< QString("660066")
		<< QString("660099")
		<< QString("6600CC")
		<< QString("6600FF")
		<< QString("990000")
		<< QString("990033")
		<< QString("990066")
		<< QString("990099")
		<< QString("9900CC")
		<< QString("9900FF")
		<< QString("CC0000")
		<< QString("CC0033")
		<< QString("CC0066")
		<< QString("CC0099")
		<< QString("CC00CC")
		<< QString("CC00FF")
		<< QString("FF0000")
		<< QString("FF0033")
		<< QString("FF0066")
		<< QString("FF0099")
		<< QString("FF00CC")
		<< QString("FF00FF")
		<< QString("003300")
		<< QString("003333")
		<< QString("003366")
		<< QString("003399")
		<< QString("0033CC")
		<< QString("0033FF")
		<< QString("333300")
		<< QString("333333")
		<< QString("333366")
		<< QString("333399")
		<< QString("3333CC")
		<< QString("3333FF")
		<< QString("663300")
		<< QString("663333")
		<< QString("663366")
		<< QString("663399")
		<< QString("6633CC")
		<< QString("6633FF")
		<< QString("993300")
		<< QString("993333")
		<< QString("993366")
		<< QString("993399")
		<< QString("9933CC")
		<< QString("9933FF")
		<< QString("CC3300")
		<< QString("CC3333")
		<< QString("CC3366")
		<< QString("CC3399")
		<< QString("CC33CC")
		<< QString("CC33FF")
		<< QString("FF3300")
		<< QString("FF3333")
		<< QString("FF3366")
		<< QString("FF3399")
		<< QString("FF33CC")
		<< QString("FF33FF")
		<< QString("006600")
		<< QString("006633")
		<< QString("006666")
		<< QString("006699")
		<< QString("0066CC")
		<< QString("0066FF")
		<< QString("336600")
		<< QString("336633")
		<< QString("336666")
		<< QString("336699")
		<< QString("3366CC")
		<< QString("3366FF")
		<< QString("666600")
		<< QString("666633")
		<< QString("666666")
		<< QString("666699")
		<< QString("6666CC")
		<< QString("6666FF")
		<< QString("996600")
		<< QString("996633")
		<< QString("996666")
		<< QString("996699")
		<< QString("9966CC")
		<< QString("9966FF")
		<< QString("CC6600")
		<< QString("CC6633")
		<< QString("CC6666")
		<< QString("CC6699")
		<< QString("CC66CC")
		<< QString("CC66FF")
		<< QString("FF6600")
		<< QString("FF6633")
		<< QString("FF6666")
		<< QString("FF6699")
		<< QString("FF66CC")
		<< QString("FF66FF")
		<< QString("009900")
		<< QString("009933")
		<< QString("009966")
		<< QString("009999")
		<< QString("0099CC")
		<< QString("0099FF")
		<< QString("339900")
		<< QString("339933")
		<< QString("339966")
		<< QString("339999")
		<< QString("3399CC")
		<< QString("3399FF")
		<< QString("669900")
		<< QString("669933")
		<< QString("669966")
		<< QString("669999")
		<< QString("6699CC")
		<< QString("6699FF")
		<< QString("999900")
		<< QString("999933")
		<< QString("999966")
		<< QString("999999")
		<< QString("9999CC")
		<< QString("9999FF")
		<< QString("CC9900")
		<< QString("CC9933")
		<< QString("CC9966")
		<< QString("CC9999")
		<< QString("CC99CC")
		<< QString("CC99FF")
		<< QString("FF9900")
		<< QString("FF9933")
		<< QString("FF9966")
		<< QString("FF9999")
		<< QString("FF99CC")
		<< QString("FF99FF")
		<< QString("00CC00")
		<< QString("00CC33")
		<< QString("00CC66")
		<< QString("00CC99")
		<< QString("00CCCC")
		<< QString("00CCFF")
		<< QString("33CC00")
		<< QString("33CC33")
		<< QString("33CC66")
		<< QString("33CC99")
		<< QString("33CCCC")
		<< QString("33CCFF")
		<< QString("66CC00")
		<< QString("66CC33")
		<< QString("66CC66")
		<< QString("66CC99")
		<< QString("66CCCC")
		<< QString("66CCFF")
		<< QString("99CC00")
		<< QString("99CC33")
		<< QString("99CC66")
		<< QString("99CC99")
		<< QString("99CCCC")
		<< QString("99CCFF")
		<< QString("CCCC00")
		<< QString("CCCC33")
		<< QString("CCCC66")
		<< QString("CCCC99")
		<< QString("CCCCCC")
		<< QString("CCCCFF")
		<< QString("FFCC00")
		<< QString("FFCC33")
		<< QString("FFCC66")
		<< QString("FFCC99")
		<< QString("FFCCCC")
		<< QString("FFCCFF")
		<< QString("00FF00")
		<< QString("00FF33")
		<< QString("00FF66")
		<< QString("00FF99")
		<< QString("00FFCC")
		<< QString("00FFFF")
		<< QString("33FF00")
		<< QString("33FF33")
		<< QString("33FF66")
		<< QString("33FF99")
		<< QString("33FFCC")
		<< QString("33FFFF")
		<< QString("66FF00")
		<< QString("66FF33")
		<< QString("66FF66")
		<< QString("66FF99")
		<< QString("66FFCC")
		<< QString("66FFFF")
		<< QString("99FF00")
		<< QString("99FF33")
		<< QString("99FF66")
		<< QString("99FF99")
		<< QString("99FFCC")
		<< QString("99FFFF")
		<< QString("CCFF00")
		<< QString("CCFF33")
		<< QString("CCFF66")
		<< QString("CCFF99")
		<< QString("CCFFCC")
		<< QString("CCFFFF")
		<< QString("FFFF00")
		<< QString("FFFF33")
		<< QString("FFFF66")
		<< QString("FFFF99")
		<< QString("FFFFCC");

		int step = colours.size()/person::allPersons.size();//���, � ������� ����� ������������ ���������
		int j = 0;
		QSet<person> tmp;//������ ����������

		//�������� ������� ��������� �� ��������� ���������, ������������� �����
		for(QSet<person>::iterator i = person::allPersons.begin(); i != person::allPersons.end(); i++)
		{
			person per = *i;
			per.colour = colours[j * step];
			tmp << per;
			j++;
		}
		
		//���������������� ������ ���������� ���������� ����������
		person::InitPer(tmp);
}

void TextFromParsingAndHTML(QList<StanfordWord *> & markedText, QString HTMLtext, textTree** treeText)
{
	int posInList = 0;//������� ������� ����� � ����� ����

	//���� ���� ���� HTML �� ������������
	while(HTMLtext.size() != 0)
	{
		//���� ��������� ������ ����
		if(HTMLtext[0] == QChar('<'))
		{
			//������� ���
			QString tag;
			while(HTMLtext[0] != QChar('>'))
			{
				tag.push_back(HTMLtext[0]);
				HTMLtext.remove(0,1);
			}
			tag.push_back(HTMLtext[0]);
			HTMLtext.remove(0,1);

			//�������� ID ����� � ����������� ��� ����
			int wordID, sentID;
			//��� �������������� ����
			if(tag.contains("/"))
			{
				//ID �������� ����������� � �����
				wordID = markedText[posInList - 1]->wordID;
				sentID = markedText[posInList - 1]->sentID;
			}else // ��� ������������ ����
			{
				//ID ���������� ����������� � �����
				wordID = markedText[posInList]->wordID;
				sentID = markedText[posInList]->sentID;
			}

			//�������� ��� � ������������� �����
			markedText.insert(posInList,new StanfordWord(sentID, wordID, tag, tag, CC, MARK, QVector<corefCoord>()));
			posInList++;

		}else if(HTMLtext[0].isSpace())//���� ������� ������ - ������
		{
			//������� ��� �������
			while(HTMLtext[0].isSpace())
			{
				HTMLtext.remove(0,1);
			}

		}else if(markedText[posInList]->wordPOS == quotation)//���� ������� ����� - �������
		{
			//������� ���� ������� � ���� word �����
			QChar quot = HTMLtext[0];
			markedText[posInList]->word = QString(quot);
			HTMLtext.remove(0,1);
			posInList++;
		}else //���� ������� �����
		{
			//������� ������� ����� �� ����� HTML
			QString a = markedText[posInList]->word;
			while(a.size()!=0)
			{
				if(HTMLtext[0] == a[0])
				{
					HTMLtext.remove(0,1);
					a.remove(0,1);
				}else
				{
					HTMLtext.remove(0,1);
				}
			}
			posInList++;
		}
	}

	//������������ �����
	QListIterator<StanfordWord *> i(markedText);
	*treeText = new textTree(i);
}

void output(textTree* treeText, QString filename)
{
	QString answer;//������, ��������������� � ������

	//�������� ����� �� ������
	treeToStr(treeText, answer);
	QFile MyFile(filename);

	//������� ����
	if ( MyFile.open(QIODevice::WriteOnly) )
	{
		//������� ������ ���������� � �������� �� � ����������� �����
		QTextStream output( &MyFile );
		output << QString("<p><span style=\"color:\#0000FF\">UNKNOWN</span></p>");
		for(QSet<person>::Iterator i = person::allPersons.begin(); i!=person::allPersons.end(); i++)
		{
			output << QString("<p><span style=\"color:\#") + i->colour + QString("\">") + i->name + QString("</span></p>");
		}
		
		//������� ����� � ����
		output << answer;

		//������� ����
		MyFile.close();
	}
}

void treeToStr(textTree* treeText, QString & result)
{
	result+=treeText->startingTag;//�������� � ��������������� ������ ��������� ���
	
	//���� ������ �� ������������
	for(QList<sentence>::Iterator i = treeText->mainText.begin(); i!=treeText->mainText.end(); i++)
	{
		QList<textTree*>::Iterator k = i->childList.begin();//�������� �� �������� ������� �����������

		//��������� ������ �� ������
		for(QList<StanfordWord*>::Iterator j = i->words.begin(); j!=i->words.end(); j++)
		{
			//���� ������ ����� - �������
			if((*j)->type == CHILD)
			{
				//������� ���������� ������� ��� �������� �������
				treeToStr((*k), result);
				//������� � ���������� �������
				k++;
			}else//���� ������� ����� ��� ���
			{
				//���� ������ ����� ��������� ���������, �� ��������� �������� ���� �����
				if((*j)->isPunct() && (*j)->word!=QString("-") && (*j)->wordPOS != LRB && (*j)->lemma!=QString("''"))
				{
					//������ ������ ������
					if(result[result.size()-1]==QChar(' '))
					{
						result.remove(result.size()-1,1);
					}
				}

				//��������� ����� � ������
				result+= (*j)->word;

				//���� ����� ������� ����� ���� �������
				if((*j)->wordPOS!=LRB && (*j)->lemma!=QString("`"))
				{
					//��������� ������
					result+= QString(" ");
				}
			}
		}
	}

	//�������� � ���������� �������� ���
	result+=treeText->endingTag;
}

void dilogMode(int argc, QString & XMLfile, QString & HTMLfile, QString & DOTfile)
{
	//�������� ������� ����� � ������
	QTextStream cout(stdout);
	QTextStream cin(stdin);

	//���� ��� ���� � DOT �����
	if(argc == 1)
	{
		//��������� � ������������ ����
		cout << "Enter the path to the DOT file"<<endl;

		//������� ���� � �����
		cin >> DOTfile;

		//����������, ��� ���� � DOT ����� ����
		argc++;
	}

	//���� ��� ���� � XML �����
	if(argc == 2)
	{
		//��������� � ������������ ����
		cout << "Enter the path to the XML file"<<endl;

		//������� ���� � �����
		cin >> XMLfile;

		//����������, ��� ���� � XML ����� ����
		argc++;
	}
	//���� ��� ���� � HTML �����
	if(argc == 3)
	{
		//��������� � ������������ ����
		cout << "Enter the path to the HTML file"<<endl;

		//������� ���� � �����
		cin >> HTMLfile;
	}
}

DOTERRORS checkDOTerrors(QStringList DOTtext, int* str)
{
	//���� � ������ ������ ��� '{'
	if(!DOTtext[0].contains(QString("{")))
	{
		//������� ��� ��������������� ������
		*str = 0;
		return OPENING_BODY;
	}

	//���� � ��������� ������ ��� '}'
	if(!DOTtext[DOTtext.size()-1].contains(QString("}")))
	{
		//������� ��� ��������������� ������
		*str = DOTtext.size()-1;
		return CLOSING_BODY;
	}

	bool isEdges = false;//����������� ����
	QStringList edges;//������ ����������

	//����� ������ � ������ �� ������ �� ������������� ������
	for(int pos = 1; pos < DOTtext.size()-1; pos++)
	{
		//���� ��������� ������ � �����
		if(DOTtext[pos].contains(QString("->")))
		{
			//����� �� ������ ���� ���������� ������
			isEdges = true;
		}

		//������� ���� � ������
		if(DOTtext[pos].contains(QString("-")) && !DOTtext[pos].contains(QString("->")))
		{
			QStringList a = DOTtext[pos].split(QString("-"));
			if(!a[0].contains(QString("[")))
			{
				*str = pos;
				return EDGE;
			}
		}

		//�������� ���������� ���������� ��� �� ����� ������
		if(DOTtext[pos].contains(QString("->")))
		{
			QStringList a = DOTtext[pos].split(QString("->"));
			if(a.size()>2)
			{
				*str = pos;
				return A_LOT_OF_ARC;
			}
		}

		//�������� �� ���������� ������ ����� ������ ����
		if(isEdges && !DOTtext[pos].contains(QString("->")))
		{
			*str = pos;
			return TOP_ANNOUNCEMENT;
		}

		//�������� �� ������� ; � ������
		if(!DOTtext[pos].contains(QString(";")))
		{
			*str = pos;
			return SEMICOLON;
		} else //�������� �� ������� ��������, ����� ����� ������������, ����� ;
		{
			QStringList a = DOTtext[pos].split(";");
			while(a[1][0].isSpace())
			{
				a[1].remove(0,1);
			}

			if(a[1].size() > 1)
			{
				*str = pos;
				return A_LOT_OF_VERTEX;
			}
		}

		//�������� �� ������� [
		if(!DOTtext[pos].contains(QString("[")))
		{
			*str = pos;
			return OPENING_SQUARE_BRACKET;
		}

		//�������� �� ������� ]
		if(!DOTtext[pos].contains(QString("]")))
		{
			*str = pos;
			return CLOSING_SQUARE_BRACKET;
		}
		
		//�������� �� ������� " ����� label=
		QStringList a = DOTtext[pos].split("label=");
		while(a[1][0].isSpace())
		{
			a[1].remove(0,1);
		}
		if(a[1][0] != QChar('\"'))
		{
			*str = pos;
			return OPENING_HELP;
		}

		a = a[1].split(QString("]"));

		while(a[0][a[0].size()-1].isSpace())
		{
			a[0].remove(a[0].size()-1,1);
		}

		//�������� �� ������� ����������� " ����� label=
		if(a[0][a[0].size()-1] != QChar('\"'))
		{
			*str = pos;
			return CLOSING_HELP;
		}

		//���� �� � ���������� ������
		if(!isEdges)
		{
			//��������� ������ �������
			QStringList b = DOTtext[pos].split(QString("["));
			while(b[0][0].isSpace())
			{
				b[0].remove(0,1);
			}
			while(b[0][b[0].size()-1].isSpace())
			{
				b[0].remove(b[0].size()-1,1);
			}
			edges << b[0];
		} else//���� � ���������� ���
		{
			//�������� ������� ��������� �� ������� � ������ ����������� 
			QStringList b = DOTtext[pos].split(QString("->"));
			while(b[0][0].isSpace())
			{
				b[0].remove(0,1);
			}
			while(b[0][b[0].size()-1].isSpace())
			{
				b[0].remove(b[0].size()-1,1);
			}
			if(!edges.contains(b[0]))
			{
				*str = pos;
				return UNKNOWN_VERTEX;
			}

			//�������� ������� ��������� �� ������� � ������ ����������� 
			b = b[1].split(QString("["));

			while(b[0][0].isSpace())
			{
				b[0].remove(0,1);
			}
			while(b[0][b[0].size()-1].isSpace())
			{
				b[0].remove(b[0].size()-1,1);
			}
			if(!edges.contains(b[0]))
			{
				*str = pos;
				return UNKNOWN_VERTEX;
			}
		}
	}
	//������ �� �������
	*str = -1;
	return NO_ERRORS;
}

void showError(DOTERRORS error, int str)
{
	QString message = QString("Input is incorrect: ");//��������� �� �����

	//������������ ��������� � ������������ � ����� ������
	if(error == EDGE)
		message += QString("In the input graph the edge instead of the arc (");
	if(error == UNKNOWN_VERTEX)
		message += QString("In the input graph, the arc is associated with an undeclared vertex (");
	if(error == A_LOT_OF_ARC)
		message += QString("in the input graph more than one arc in a row (");
	if(error == TOP_ANNOUNCEMENT)
		message += QString("In the input graph, the declaration of the vertex follows the arc (");
	if(error == CLOSING_BODY)
		message += QString("there is no \"}\" in the input field (");
	if(error == OPENING_BODY)
		message += QString("there is no \"{\" in the input field (");
	if(error == SEMICOLON)
		message += QString("absent \";\" (");
	if(error == OPENING_SQUARE_BRACKET)
		message += QString("absent \"[\" (");
	if(error == CLOSING_SQUARE_BRACKET)
		message += QString("absent \"]\" (");
	if(error == OPENING_HELP)
		message += QString("incorrect specification of qualifiers (there is no opening double quotation mark after  \"label=\") (");
	if(error == CLOSING_HELP)
		message += QString("incorrect specification of qualifiers (there is no closing double quotation mark after  \"label=\") (");
	if(error == A_LOT_OF_VERTEX)
		message += QString("more than one declaration of the vertex or edge (");

	//��������� ����� ������ � ������������
	message += QString("line ") + QString::number(str) + QString(")");

	//������� ��������� ������������
	QTextStream cout(stdout);

	cout << message << endl;
}

bool hasChildConflict(QList<textTree*> & children)
{
	QList<QSet<person>> allVariants;//��� ��������� ���������
	QVector<bool> hasConflict;//������� ���������� �� ����� ��������� ��������

	//��������� ���������� � ������ �������
	for(QList<textTree*>::Iterator i = children.begin(); i != children.end(); i++)
	{
		//��������� ���������� � ������ �����������
		for(QList<sentence>::Iterator j = (*i)->mainText.begin(); j != (*i)->mainText.end(); j++)
		{
			QSet  <person> possibleSpeakers;
			//�������� ���������� � ��������� ������� ����������
			hasConflict << addressFinder(*j, possibleSpeakers);
			//���� ��������� �� ������
			if(!possibleSpeakers.isEmpty())
			{
				//�������� ��� � ������ ��������
				allVariants << possibleSpeakers;
			}
		}
	}

	//���� �� ������� �� ������ ���������
	if(allVariants.isEmpty())
	{
		//����������� �������: �������� �� �������� �� ������ ������?
		bool hasConfl = false;
		for(int i = 0; i < hasConflict.size(); i++)
		{
			hasConfl += !hasConflict[i];
		}
		return hasConfl;
	}

	//����� ����������� ���������� ��������
	QSet<person> a = allVariants[0];
	for(int i = 0; i < allVariants.size(); i++)
	{
		QSet<person> b;
		TwoSetHavePersons(a, allVariants[i], b);

		a.clear();

		QSetIterator<person> j(b);
		while(j.hasNext())
			a << j.next();
	}

	//���� ����������� ������ ��� �������� ���������
	if(a.isEmpty() && !allVariants.isEmpty())
	{
		//������ ��������
		return true;
	}

	//���� ������ ���� �� ���� �������
	if(!a.isEmpty())
	{
		//��������� �� ����
		return false;
	}
}

void clearSetByUnikSpec(QSet  <person> & PossibleSpeakers, int startPunct, int endPunct, sentence & context)
{
	QString specifier;
	//������� ������������
	for(int i = startPunct+1; i < endPunct; i++)
	{
		specifier+=context.words[i]->word + QString(" ");
	}

	while(specifier[0] == QChar(' '))
	{
		specifier.remove(0, 1);
	}

	if(specifier.isEmpty())
	{
		return;
	}

	while(specifier[specifier.size() - 1] == QChar(' '))
	{
		specifier.remove(specifier.size() - 1, 1);
	}
	
	//��������� �� ������������ ������������
	int perCount = 0;
	person per;
	QSetIterator<person> j(person::allPersons);
	while(j.hasNext())
	{
		person pertmp = j.next();
		if(pertmp.hasObjSpecifier(specifier))
		{
			per = pertmp;
			perCount++;
		}
	}

	if(perCount != 1)
	{
		return;
	}

	//������ ��������� � ����� ��������������, ���� �� ����
	if(PossibleSpeakers.contains(per))
	{
		PossibleSpeakers.remove(per);
	}
}