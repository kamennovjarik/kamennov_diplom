/*!
\file
\brief ���� ����������� ������ ���������

������ ���� �������� � ���� ���������� ������ ���������
*/
#include "person.h"

person::person(QObject *parent)
	: QObject(parent)
{

}

person::person(const person &other)
{
	name = other.name; // ����������� ��� ���������
	
	//��������� ����������� �������������
	QStringListIterator i (other.objSpecifiers);
	while(i.hasNext())
	{
		objSpecifiers.push_back(i.next());
	}

	//��������� ������������ �������������
	QStringListIterator j (other.canUseSubjSpecifiers);
	while(j.hasNext())
	{
		canUseSubjSpecifiers.push_back(j.next());
	}
	
	colour = other.colour; // ����������� ����, ������� �������� ������������ � html �����
}

person::person(QString _name, QStringList _objSpecifiers, QStringList _canUseSubjSpecifiers, QString _colour)
{
	//���������� ��������������� ����
	name = _name;
	QMutableStringListIterator i (_objSpecifiers);
	while(i.hasNext())
	{
		objSpecifiers.push_back(i.next());
	}

	QMutableStringListIterator j (_canUseSubjSpecifiers);
	while(j.hasNext())
	{
		canUseSubjSpecifiers.push_back(j.next());
	}

	colour = _colour;
}

person::~person()
{

}

void person::InitPer(QSet <person> & allPersons1)
{
	//�������� ������ ��������� ����������
	person::allPersons.clear();

	//��������� ��������� �� ����������� ���������
	QMutableSetIterator<person> j(allPersons1);
	while(j.hasNext())
	{
		person::allPersons.insert(j.next());
	}
}

bool person::operator==(const person & right)const
{
	//��������� �� ������ ������������ ������� ���� ������ ����������� �������
	if (name!=right.name)
	{
		return false;
	}
	if(objSpecifiers.size()!=right.objSpecifiers.size())
	{
		return false;
	}
	
	QStringList::const_iterator j = right.objSpecifiers.constBegin();
	for(QStringList::const_iterator i = objSpecifiers.constBegin(); i != objSpecifiers.constEnd(); i++)
	{
		if((*i) != (*j))
		{
			return false;
		}
		j++;
	}

	if(canUseSubjSpecifiers.size()!=right.canUseSubjSpecifiers.size())
	{
		return false;
	}

	QStringList::const_iterator k = right.canUseSubjSpecifiers.constBegin();
	for(QStringList::const_iterator i = canUseSubjSpecifiers.constBegin(); i != canUseSubjSpecifiers.constEnd(); i++)
	{
		if((*i) != (*k))
		{
			return false;
		}
		k++;
	}

	if (colour!=right.colour)
	{
		return false;
	}

	return true;
}

bool person::operator!=(const person & right)const
{
	//��������� �� ������������ ������� ���� ������� ��������� �����������
	if (name!=right.name)
	{
		return true;
	}
	if(objSpecifiers.size()!=right.objSpecifiers.size())
	{
		return true;
	}
	
	QStringList::const_iterator j = right.objSpecifiers.constBegin();
	for(QStringList::const_iterator i = objSpecifiers.constBegin(); i != objSpecifiers.constEnd(); i++)
	{
		if((*i) != (*j))
		{
			return true;
		}
		j++;
	}

	if(canUseSubjSpecifiers.size()!=right.canUseSubjSpecifiers.size())
	{
		return false;
	}

	QStringList::const_iterator k = right.canUseSubjSpecifiers.constBegin();
	for(QStringList::const_iterator i = canUseSubjSpecifiers.constBegin(); i != canUseSubjSpecifiers.constEnd(); i++)
	{
		if((*i) != (*k))
		{
			return true;
		}
		k++;
	}

	if (colour!=right.colour)
	{
		return true;
	}

	return false;
}

person person::operator = (const person & other)
{
	//���������� ������� ���� ����������� ��������� ������� ���������
	name = other.name; 
	
	QStringListIterator i (other.objSpecifiers);
	objSpecifiers.clear();
	while(i.hasNext())
	{
		objSpecifiers.push_back(i.next());
	}

	QStringListIterator j (other.canUseSubjSpecifiers);
	canUseSubjSpecifiers.clear();
	while(j.hasNext())
	{
		canUseSubjSpecifiers.push_back(j.next());
	}
	
	colour = other.colour;

	return *this;
}

uint qHash(const person & k)
	{
		//������������ � �������� ����� ��� �����
		uint a = qHash(k.name);
		//�������� ��� ����� �� ��� �����
		a*=qHash(k.colour);

		//�������� �� ���� ������� �������������
		for(QStringList::const_iterator i = k.canUseSubjSpecifiers.constBegin(); i != k.canUseSubjSpecifiers.constEnd(); i++)
		{
			a*=	qHash(*i);
		}

		for(QStringList::const_iterator i = k.objSpecifiers.constBegin(); i != k.objSpecifiers.constEnd(); i++)
		{
			a*=	qHash(*i);
		}

		return a;
	}

bool person::hasSubjSpecifier (QString Specifier)
{
	//����� �� ���� ������������ �������������� ����������
	return canUseSubjSpecifiers.contains(Specifier);
}

bool person::hasObjSpecifier (QString Specifier)
{
	//����� �� ���� ������������ �������������� ����������
	return objSpecifiers.contains(Specifier);
}

person person::personByColour (QString colour)
{
	//������ ���������� ���� � ���� ����������
	QSetIterator<person> i(allPersons);
	while(i.hasNext())
	{
		person per = i.next();

		//���� ���� ������
		if(per.colour == colour)
		{
			//������� �������� ���������
			return per;
		}
	}

	//������� "�������" ���������
	return person();
}

QString person::generateStartingTag()
{
	return (QString("<span style=\"color:\#") + colour + QString("\">"));
}

person person::personByName (QString name)
{
	//������ ���������� ��� � ���� ����������
	QSetIterator<person> i(allPersons);
	while(i.hasNext())
	{
		person per = i.next();

		//���� ��� �������
		if(per.name == name)
		{
			//������� �������� ���������
			return per;
		}
	}

	//������� "�������" ���������
	return person();
}