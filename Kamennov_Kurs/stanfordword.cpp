/*!
\file
\brief ������������ ���� ���������� ������ �����

������ ���� �������� � ���� ���������� ������ �����
*/
#include "stanfordword.h"

StanfordWord::StanfordWord(QObject *parent)
	: QObject(parent)
{

}

StanfordWord::StanfordWord(int _sentID, int _wordID, QString _word, QString _lemma, partOfSpeech _wordPOS, wordType _type, QVector< corefCoord > & _corefs)
{
	//��������� ��������������� ����
	sentID = _sentID;
	wordID = _wordID;
	word = _word;
	lemma = _lemma;
	wordPOS = _wordPOS;
	type = _type;
	for(int i = 0; i < _corefs.size(); i++)
	{
		corefs.push_back(_corefs[i]);
	}
}

StanfordWord & StanfordWord::operator=(const StanfordWord& right)
{
	//��������� ��������������� ���� ����������� ����� ��������
	sentID = right.sentID;
	wordID = right.wordID;
	word = right.word;
	lemma = right.lemma;
	wordPOS = right.wordPOS;
	type = right.type;

	//�������� ������ ������
	corefs.clear();

	//��������� �����
	for(int i = 0; i < right.corefs.size(); i++)
	{
		corefs.push_back(right.corefs[i]);
	}

	//������� ���������� �����
	return *this;
}

bool StanfordWord::operator==(const StanfordWord& right)const
{
	//��������� ������ ���� �� ������ ����������
	if(sentID!=right.sentID)
	{
		return false;
	}
	if(wordID!=right.wordID)
	{
		return false;
	}
	if(word!=right.word)
	{
		return false;
	}
	if(lemma!=right.lemma)
	{
		return false;
	}
	if(wordPOS!=right.wordPOS)
	{
		return false;
	}
	if(type!=right.type)
	{
		return false;
	}
	if(corefs.size()!=right.corefs.size())
	{
		return false;
	}

	QVectorIterator<corefCoord> i(corefs);
	QVectorIterator<corefCoord> j(right.corefs);

	while(i.hasNext())
	{
		corefCoord a, b;
		a = i.next();
		b = j.next();
		if(a.endID != b.endID)
		{
			return false;
		}
		
		if(a.rootID != b.rootID)
		{
			return false;
		}
		
		if(a.sentenceID != b.sentenceID)
		{
			return false;
		}
		
		if(a.startID != b.startID)
		{
			return false;
		}
	}
	
	return true;
}

bool StanfordWord::operator!=(const StanfordWord& right)const
{
	//���� ��� ����� �����
	if(*this == right)
	{
		//������� ������� ���������
		return false;
	}
	//������� ������� �����������
	return true;
}

QString StanfordWord::doKey()
{
	//��������� ���� ��� ID ����������� � ID ����� ����� ������
	return QString::number(sentID)+QString(" ")+QString::number(wordID);
}

bool StanfordWord::isAbjective()
{
	//��������� ����� ���� �� ���������� � ������� ���� Stanford ��������������� ��� ��������������
	return wordPOS == JJ || wordPOS == JJR || wordPOS == JJS;
}

bool StanfordWord::isVerb()
{
	//��������� ����� ���� �� ���������� � ������� ���� Stanford ��������������� ��� ������
	return wordPOS == VB || wordPOS == VBD || wordPOS == VBG || wordPOS == VBN || wordPOS == VBP || wordPOS == VBZ;
}

bool StanfordWord::isNoun()
{
	//��������� ����� ���� �� ���������� � ������� ���� Stanford ��������������� ��� ���������������
	return wordPOS == NN || wordPOS == NNS || wordPOS == NNP || wordPOS == NNPS;
}

bool StanfordWord::isPunct()
{
	//��������� ����� ���� �� ���������� � ������� ���� Stanford ��������������� ��� ���� ����������
	return wordPOS == SentFinPunct || wordPOS == Comma || wordPOS == Colon || wordPOS == quotation || wordPOS == LRB || wordPOS == RRB;
}

StanfordWord::~StanfordWord()
{

}
