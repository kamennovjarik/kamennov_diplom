/*!
\file
\brief ���� � ����������� ������� ������ ������ �������

������ ���� �������� � ���� ���������� ������ ������, ��������������� � ���� ������, � ������� ������ ���� - ������� �����������
*/
#include "texttree.h"

textTree::textTree()
{

}

textTree::textTree(const textTree &other)
{

}

textTree::textTree(QListIterator<StanfordWord*> & other)
{
	//���� ���� ����� - ������ �� ������
	highLevel = true;
	while(other.hasNext())
	{
		int prevID = other.next()->sentID;
		other.previous();
		QList<StanfordWord*> tmp; // ������ �� ����� (������������� ������ � ���������)
	
		QList< textTree *> childList;

		//���� � ������ ���������� ID ����������� - �������� �����������
		while(other.hasNext() && other.next()->sentID == prevID)
		{
			other.previous();

			StanfordWord * b =other.next();
			//���� �������� ����������� ��� ���� 
			if(b->word.contains(QString("<span")))
			{
				//������� ���������� ����������� ��� �������� ����� ��������
				childList.push_back(new textTree(other));
				childList.value(childList.size()-1)->startingTag = b->word;
				childList.value(childList.size()-1)->highLevel = false;
				b = new StanfordWord(prevID,b->wordID, QString(""),QString(""),CC, CHILD, QVector<corefCoord>());
			}
			//���� �������� ����������� ���  
			if(b->word.contains(QString("</span>")))
			{
				//������ ��� � ��������������� ����
				endingTag = b->word;
				//������ ����������� � �����
				mainText.push_back(sentence(tmp, childList));
				//�����
				return;
			}
			tmp.push_back(b);
		}
		if(other.hasNext())
		{
			other.previous();
		}
		//�������� �����������
		mainText.push_back(sentence(tmp, childList));
	}
}

textTree::~textTree()
{

}