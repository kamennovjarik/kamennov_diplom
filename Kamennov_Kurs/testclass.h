/*!
\file
\brief ������������ ���� � ��������� ��������� ������

������ ���� �������� � ���� �������� ��������� ������
*/
#ifndef TESTCLASS_H
#define TESTCLASS_H

#include <QObject>
#include <QtTest/QtTest>
#include <texttree.h>
#include <person.h>
#include <extractFunctions.h>

Q_DECLARE_METATYPE(textTree)
Q_DECLARE_METATYPE(sentence)
Q_DECLARE_METATYPE(person)
Q_DECLARE_METATYPE(corefCoord)
typedef QMap<QString, StanfordWord*> newQMapName;

/*!�������� �����

*\brief �������� �����

�������� �����
*/
class TestClass : public QObject
{
	Q_OBJECT

public:
	TestClass(QObject *parent=0);
	~TestClass();

private slots:
	/*! ����� �������� ������ ������������ ������� substructSubjectBySubjQualifier

	*\brief ����� �������� ������ ������������ ������� substructSubjectBySubjQualifier
	
	����� �������� ������ ������������ ������� substructSubjectBySubjQualifier
	*/
	void testSubstructSubjectBySubjQualifier_data();

	/*! ����� ������������ ������� substructSubjectBySubjQualifier

	*\brief ����� ������������ ������� substructSubjectBySubjQualifier
	
	����� ������������ ������� substructSubjectBySubjQualifier
	*/
	void testSubstructSubjectBySubjQualifier();
	
	/*! ����� �������� ������ ������������ ������� substructSubjectByObjQualifier

	*\brief ����� �������� ������ ������������ ������� substructSubjectByObjQualifier
	
	����� �������� ������ ������������ ������� substructSubjectByObjQualifier
	*/
	void testsubstructSubjectByObjQualifier_data();
	
	/*! ����� ������������ ������� substructSubjectByObjQualifier

	*\brief ����� ������������ ������� substructSubjectByObjQualifier
	
	����� ������������ ������� substructSubjectByObjQualifier
	*/
	void testsubstructSubjectByObjQualifier();
	
	/*! ����� �������� ������ ������������ ������� corefFinder

	*\brief ����� �������� ������ ������������ ������� corefFinder
	
	����� �������� ������ ������������ ������� corefFinder
	*/
	void testCorefFinder_data();

	/*! ����� ������������ ������� corefFinder

	*\brief ����� ������������ ������� corefFinder
	
	����� ������������ ������� corefFinder
	*/
	void testCorefFinder();

	/*! ����� �������� ������ ������������ ������� appealSearch

	*\brief ����� �������� ������ ������������ ������� appealSearch
	
	����� �������� ������ ������������ ������� appealSearch
	*/
	void testappealSearch_data();

	/*! ����� ������������ ������� appealSearch

	*\brief ����� ������������ ������� appealSearch
	
	����� ������������ ������� appealSearch
	*/
	void testappealSearch();

	/*! ����� �������� ������ ������������ ������� substructSubject

	*\brief ����� �������� ������ ������������ ������� substructSubject
	
	����� �������� ������ ������������ ������� substructSubject
	*/
	void testsubstructSubject_data();

	/*! ����� ������������ ������� substructSubject

	*\brief ����� ������������ ������� substructSubject
	
	����� ������������ ������� substructSubject
	*/
	void testsubstructSubject();

	/*! ����� �������� ������ ������������ ������� expansionCharacterZone

	*\brief ����� �������� ������ ������������ ������� expansionCharacterZone
	
	����� �������� ������ ������������ ������� expansionCharacterZone
	*/
	void testexpansionCharacterZone_data();

	/*! ����� ������������ ������� expansionCharacterZone

	*\brief ����� ������������ ������� expansionCharacterZone
	
	����� ������������ ������� expansionCharacterZone
	*/
	void testexpansionCharacterZone();
	
	/*! ����� �������� ������ ������������ ������� inOrderFinder

	*\brief ����� �������� ������ ������������ ������� inOrderFinder
	
	����� �������� ������ ������������ ������� inOrderFinder
	*/
	void testinOrderFinder_data();

	/*! ����� ������������ ������� inOrderFinder

	*\brief ����� ������������ ������� inOrderFinder
	
	����� ������������ ������� inOrderFinder
	*/
	void testinOrderFinder();

	/*! ����� �������� ������ ������������ ������� textMarkup

	*\brief ����� �������� ������ ������������ ������� textMarkup
	
	����� �������� ������ ������������ ������� textMarkup
	*/
	void testtextMarkup_data();

	/*! ����� ������������ ������� textMarkup

	*\brief ����� ������������ ������� textMarkup
	
	����� ������������ ������� textMarkup
	*/
	void testtextMarkup();

	/*! ����� ��������� ���� �������� ����������
	*\param [in] first - ��������� ����������
	*\param [in] second - ��������� ����������
	*\param [out] Forgotten - ����� ����������, �������������� � ������ ��������� � ������������� �� ������
	*\param [out] superfluous - ����� ����������, �������������� �� ������ ��������� � ������������� � ������
	*\return - ������� ��������������� ���� ��������

	*\brief ����� ��������� ���� �������� ����������
	
	����� ��������� ���� �������� ����������
	*/
	bool isEqual(QSet <person> first, QSet <person> second, QStringList & Forgotten, QStringList & superfluous);

	/*! ����� ��������� ���� �������� ������
	*\param [in] main - ������ ������
	*\param [in] second - ������ ������
	*\param [out] Forgotten - �����, �������������� � ������ ������� � ������������� �� ������
	*\param [out] superfluous - �����, �������������� �� ������ ������� � ������������� � ������
	*\return - ������� ��������������� ���� ��������

	*\brief ����� ��������� ���� �������� ������
	
	 ��������� ���������� ��� ����� ������� � �������� �.�. ����� ������ ������ ��������������
	*/
	bool isEqualVectors(QVector<corefCoord> main, QVector<corefCoord> second, QVector<corefCoord> & Forgotten, QVector<corefCoord> & superfluous);

	/*! ����� ��������� ���� ���������
	*\param [in] a - ������ ����������
	*\param [in] b - ������ ����������
	*\return - ������� ��������������� ���� ���������

	*\brief ����� ��������� ���� ���������
	
	����� ��������� ���� ���������
	*/
	bool compareCoords(corefCoord a, corefCoord b);

	/*! ����� ��������� ���� ��������
	*\param [in] a - ������ ������
	*\param [in] b - ������ ������
	*\return - ������, � ������� ���������� ������ ����� ����������� ��������

	*\brief ����� ��������� ���� ��������
	
	���� ������� ������������(��������� ��� �� ���������), �� ������������ ������ ������
	*/
	QString CmpTrees(textTree* a, textTree* b);
	
};

#endif // TESTCLASS_H
