/****************************************************************************
** Meta object code from reading C++ file 'testclass.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../testclass.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QSet>
#include <QtCore/QVector>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'testclass.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_TestClass_t {
    QByteArrayData data[35];
    char stringdata0[599];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_TestClass_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_TestClass_t qt_meta_stringdata_TestClass = {
    {
QT_MOC_LITERAL(0, 0, 9), // "TestClass"
QT_MOC_LITERAL(1, 10, 40), // "testSubstructSubjectBySubjQua..."
QT_MOC_LITERAL(2, 51, 0), // ""
QT_MOC_LITERAL(3, 52, 35), // "testSubstructSubjectBySubjQua..."
QT_MOC_LITERAL(4, 88, 39), // "testsubstructSubjectByObjQual..."
QT_MOC_LITERAL(5, 128, 34), // "testsubstructSubjectByObjQual..."
QT_MOC_LITERAL(6, 163, 20), // "testCorefFinder_data"
QT_MOC_LITERAL(7, 184, 15), // "testCorefFinder"
QT_MOC_LITERAL(8, 200, 21), // "testappealSearch_data"
QT_MOC_LITERAL(9, 222, 16), // "testappealSearch"
QT_MOC_LITERAL(10, 239, 25), // "testsubstructSubject_data"
QT_MOC_LITERAL(11, 265, 20), // "testsubstructSubject"
QT_MOC_LITERAL(12, 286, 31), // "testexpansionCharacterZone_data"
QT_MOC_LITERAL(13, 318, 26), // "testexpansionCharacterZone"
QT_MOC_LITERAL(14, 345, 22), // "testinOrderFinder_data"
QT_MOC_LITERAL(15, 368, 17), // "testinOrderFinder"
QT_MOC_LITERAL(16, 386, 19), // "testtextMarkup_data"
QT_MOC_LITERAL(17, 406, 14), // "testtextMarkup"
QT_MOC_LITERAL(18, 421, 7), // "isEqual"
QT_MOC_LITERAL(19, 429, 12), // "QSet<person>"
QT_MOC_LITERAL(20, 442, 5), // "first"
QT_MOC_LITERAL(21, 448, 6), // "second"
QT_MOC_LITERAL(22, 455, 12), // "QStringList&"
QT_MOC_LITERAL(23, 468, 9), // "Forgotten"
QT_MOC_LITERAL(24, 478, 11), // "superfluous"
QT_MOC_LITERAL(25, 490, 14), // "isEqualVectors"
QT_MOC_LITERAL(26, 505, 19), // "QVector<corefCoord>"
QT_MOC_LITERAL(27, 525, 4), // "main"
QT_MOC_LITERAL(28, 530, 20), // "QVector<corefCoord>&"
QT_MOC_LITERAL(29, 551, 13), // "compareCoords"
QT_MOC_LITERAL(30, 565, 10), // "corefCoord"
QT_MOC_LITERAL(31, 576, 1), // "a"
QT_MOC_LITERAL(32, 578, 1), // "b"
QT_MOC_LITERAL(33, 580, 8), // "CmpTrees"
QT_MOC_LITERAL(34, 589, 9) // "textTree*"

    },
    "TestClass\0testSubstructSubjectBySubjQualifier_data\0"
    "\0testSubstructSubjectBySubjQualifier\0"
    "testsubstructSubjectByObjQualifier_data\0"
    "testsubstructSubjectByObjQualifier\0"
    "testCorefFinder_data\0testCorefFinder\0"
    "testappealSearch_data\0testappealSearch\0"
    "testsubstructSubject_data\0"
    "testsubstructSubject\0"
    "testexpansionCharacterZone_data\0"
    "testexpansionCharacterZone\0"
    "testinOrderFinder_data\0testinOrderFinder\0"
    "testtextMarkup_data\0testtextMarkup\0"
    "isEqual\0QSet<person>\0first\0second\0"
    "QStringList&\0Forgotten\0superfluous\0"
    "isEqualVectors\0QVector<corefCoord>\0"
    "main\0QVector<corefCoord>&\0compareCoords\0"
    "corefCoord\0a\0b\0CmpTrees\0textTree*"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_TestClass[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      20,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  114,    2, 0x08 /* Private */,
       3,    0,  115,    2, 0x08 /* Private */,
       4,    0,  116,    2, 0x08 /* Private */,
       5,    0,  117,    2, 0x08 /* Private */,
       6,    0,  118,    2, 0x08 /* Private */,
       7,    0,  119,    2, 0x08 /* Private */,
       8,    0,  120,    2, 0x08 /* Private */,
       9,    0,  121,    2, 0x08 /* Private */,
      10,    0,  122,    2, 0x08 /* Private */,
      11,    0,  123,    2, 0x08 /* Private */,
      12,    0,  124,    2, 0x08 /* Private */,
      13,    0,  125,    2, 0x08 /* Private */,
      14,    0,  126,    2, 0x08 /* Private */,
      15,    0,  127,    2, 0x08 /* Private */,
      16,    0,  128,    2, 0x08 /* Private */,
      17,    0,  129,    2, 0x08 /* Private */,
      18,    4,  130,    2, 0x08 /* Private */,
      25,    4,  139,    2, 0x08 /* Private */,
      29,    2,  148,    2, 0x08 /* Private */,
      33,    2,  153,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Bool, 0x80000000 | 19, 0x80000000 | 19, 0x80000000 | 22, 0x80000000 | 22,   20,   21,   23,   24,
    QMetaType::Bool, 0x80000000 | 26, 0x80000000 | 26, 0x80000000 | 28, 0x80000000 | 28,   27,   21,   23,   24,
    QMetaType::Bool, 0x80000000 | 30, 0x80000000 | 30,   31,   32,
    QMetaType::QString, 0x80000000 | 34, 0x80000000 | 34,   31,   32,

       0        // eod
};

void TestClass::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        TestClass *_t = static_cast<TestClass *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->testSubstructSubjectBySubjQualifier_data(); break;
        case 1: _t->testSubstructSubjectBySubjQualifier(); break;
        case 2: _t->testsubstructSubjectByObjQualifier_data(); break;
        case 3: _t->testsubstructSubjectByObjQualifier(); break;
        case 4: _t->testCorefFinder_data(); break;
        case 5: _t->testCorefFinder(); break;
        case 6: _t->testappealSearch_data(); break;
        case 7: _t->testappealSearch(); break;
        case 8: _t->testsubstructSubject_data(); break;
        case 9: _t->testsubstructSubject(); break;
        case 10: _t->testexpansionCharacterZone_data(); break;
        case 11: _t->testexpansionCharacterZone(); break;
        case 12: _t->testinOrderFinder_data(); break;
        case 13: _t->testinOrderFinder(); break;
        case 14: _t->testtextMarkup_data(); break;
        case 15: _t->testtextMarkup(); break;
        case 16: { bool _r = _t->isEqual((*reinterpret_cast< QSet<person>(*)>(_a[1])),(*reinterpret_cast< QSet<person>(*)>(_a[2])),(*reinterpret_cast< QStringList(*)>(_a[3])),(*reinterpret_cast< QStringList(*)>(_a[4])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 17: { bool _r = _t->isEqualVectors((*reinterpret_cast< QVector<corefCoord>(*)>(_a[1])),(*reinterpret_cast< QVector<corefCoord>(*)>(_a[2])),(*reinterpret_cast< QVector<corefCoord>(*)>(_a[3])),(*reinterpret_cast< QVector<corefCoord>(*)>(_a[4])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 18: { bool _r = _t->compareCoords((*reinterpret_cast< corefCoord(*)>(_a[1])),(*reinterpret_cast< corefCoord(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 19: { QString _r = _t->CmpTrees((*reinterpret_cast< textTree*(*)>(_a[1])),(*reinterpret_cast< textTree*(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 16:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QSet<person> >(); break;
            }
            break;
        case 17:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<corefCoord> >(); break;
            }
            break;
        case 18:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< corefCoord >(); break;
            }
            break;
        }
    }
}

const QMetaObject TestClass::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_TestClass.data,
      qt_meta_data_TestClass,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *TestClass::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *TestClass::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_TestClass.stringdata0))
        return static_cast<void*>(const_cast< TestClass*>(this));
    return QObject::qt_metacast(_clname);
}

int TestClass::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 20)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 20;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 20)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 20;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
