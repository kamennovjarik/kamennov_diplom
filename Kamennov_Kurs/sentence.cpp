/*!
\file
\brief ���� � ����������� ������� ������ �����������

������ ���� �������� � ���� ���������� ������ �����������
*/
#include "sentence.h"

sentence::sentence()
{

}

sentence::sentence(const sentence &other)
{
	//����������� ����� �� ���������
	QListIterator<StanfordWord*> i(other.words);
	while(i.hasNext())
	{
		words.push_back(i.next());
	}

	//����������� �������� �� ���������
	QListIterator<textTree *> j(other.childList);
	while(j.hasNext())
	{
		childList.push_back(j.next());
	}
}

sentence::sentence(QList <StanfordWord*> & _words, QList< textTree *> & _childList)
{
	//����������� ��������� �� �����
	QMutableListIterator<StanfordWord*> i(_words);
	while(i.hasNext())
	{
		words.push_back(i.next());
	}

	//����������� ��������� �� ��������
	QMutableListIterator<textTree *> j(_childList);
	while(j.hasNext())
	{
		childList.push_back(j.next());
	}
}

sentence::~sentence()
{

}

void sentence::InitKeys(QStringList & keys1)
{
	//�������� ������ �������� �����
	sentence::keys.clear();

	//����������� ����� ����� � ����������� ����
	QMutableStringListIterator i(keys1);
	while(i.hasNext())
	{
		sentence::keys.push_back(i.next());
	}
}

sentence sentence::operator = (const sentence &other)
{
	//��������� ������ �����������
	sentence sent;

	//����������� ��� ����� ����������� �����������
	QListIterator<StanfordWord*> i(other.words);
	while(i.hasNext())
	{
		sent.words.push_back(i.next());
	}

	//����������� ��� �������� ����������� �����������
	QListIterator<textTree*> j(other.childList);
	while(j.hasNext())
	{
		sent.childList.push_back(j.next());
	}

	//������� ���������� �����������
	return sent;
}

int sentence::size()
{
	//������� ����� ������� ����
	return words.size();
}

bool sentence::hasOnlyChilds()
{
	//��������� ������ ����� �� ��, �������� �� ��� ������ �� ����
	for(QList<StanfordWord*>::Iterator i = words.begin(); i != words.end(); i++)
	{
		//���� ������� ����� �� ����
		if((*i)->type == WORD && (*i)->wordPOS != quotation)
		{
			//������� ������� ���� � �����������
			return false;
		}
	}
	//���� � ����������� ���
	return true;
}