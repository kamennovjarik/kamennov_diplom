/*!
\file
\brief ������������ ���� ���������� ��������� ������

������ ���� �������� � ���� ���������� ��������� ������
*/
#include "testclass.h"
#include <QtTest>

TestClass::TestClass(QObject *parent)
	: QObject(parent)
{

}

TestClass::~TestClass()
{

}

void TestClass::testappealSearch_data()
{
	QTest::addColumn<sentence>("context");
	QTest::addColumn< QVector< corefCoord >  >("appealCoords");

	/////////////////////////////////////���� 1
	/////////////////////////////////////OneAppeal

	StanfordWord* arr = new StanfordWord [6]; //������ - �����������

	arr[0] = StanfordWord(1,1, QString("Give"),QString("give"),VB, WORD, QVector<corefCoord>());
	arr[1] = StanfordWord(1,2, QString("us"),QString("we"),PRP, WORD, QVector<corefCoord>());
	arr[2] = StanfordWord(1,3, QString("that"),QString("that"),IN, WORD, QVector<corefCoord>());
	arr[3] = StanfordWord(1,4, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr[4] = StanfordWord(1,5, QString("friend"),QString("friend"),NN, WORD, QVector<corefCoord>());
	arr[5] = StanfordWord(1,6, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp; // ������ �� ����� (������������� ������ � ���������)
	tmp.push_back(arr+0);
	tmp.push_back(arr+1);
	tmp.push_back(arr+2);
	tmp.push_back(arr+3);
	tmp.push_back(arr+4);
	tmp.push_back(arr+5);

	QList< textTree *> childList;

	sentence context(tmp, childList);

	QVector< corefCoord > res;
	corefCoord temp;
	
	temp.startID = 5;
	temp.endID = 6;
	temp.rootID = 5;
	temp.sentenceID = 1;

	res.push_back(temp);

	QTest::newRow("OneAppeal")<< context << res;

	/////////////////////////////////////���� 2
	/////////////////////////////////////NameAsAppeal

	StanfordWord* arr1 = new StanfordWord [6]; //������ - �����������

	arr1[0] = StanfordWord(1,1, QString("Give"),QString("give"),VB, WORD, QVector<corefCoord>());
	arr1[1] = StanfordWord(1,2, QString("us"),QString("we"),PRP, WORD, QVector<corefCoord>());
	arr1[2] = StanfordWord(1,3, QString("that"),QString("that"),IN, WORD, QVector<corefCoord>());
	arr1[3] = StanfordWord(1,4, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr1[4] = StanfordWord(1,5, QString("Deagol"),QString("Deagol"),NNP, WORD, QVector<corefCoord>());
	arr1[5] = StanfordWord(1,6, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp1; // ������ �� ����� (������������� ������ � ���������)
	tmp1.push_back(arr1+0);
	tmp1.push_back(arr1+1);
	tmp1.push_back(arr1+2);
	tmp1.push_back(arr1+3);
	tmp1.push_back(arr1+4);
	tmp1.push_back(arr1+5);

	QList< textTree *> childList1;

	sentence context1(tmp1, childList1);

	QVector< corefCoord > res1;
	
	temp.startID = 5;
	temp.endID = 6;
	temp.rootID = 5;
	temp.sentenceID = 1;

	res1.push_back(temp);

	QTest::newRow("NameAsAppeal")<< context1 << res1;

	/////////////////////////////////////���� 3
	/////////////////////////////////////TwoAppeals
	
	int wordCount = 8;
	StanfordWord* arr2 = new StanfordWord [wordCount]; //������ - �����������
	
	arr2[0] = StanfordWord(1,1, QString("Give"),QString("give"),VB, WORD, QVector<corefCoord>());
	arr2[1] = StanfordWord(1,2, QString("us"),QString("we"),PRP, WORD, QVector<corefCoord>());
	arr2[2] = StanfordWord(1,3, QString("that"),QString("that"),IN, WORD, QVector<corefCoord>());
	arr2[3] = StanfordWord(1,4, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr2[4] = StanfordWord(1,5, QString("Deagol"),QString("Deagol"),NNP, WORD, QVector<corefCoord>());
	arr2[5] = StanfordWord(1,6, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr2[6] = StanfordWord(1,7, QString("friend"),QString("friend"),NN, WORD, QVector<corefCoord>());
	arr2[7] = StanfordWord(1,8, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp2; // ������ �� ����� (������������� ������ � ���������)

	for(int i = 0; i < wordCount; i++)
	{
		tmp2.push_back(arr2+i);
	}

	QList< textTree *> childList2;

	sentence context2(tmp2, childList2);

	QVector< corefCoord > res2;
	
	temp.startID = 5;
	temp.endID = 6;
	temp.rootID = 5;
	temp.sentenceID = 1;

	res2.push_back(temp);

	temp.startID = 7;
	temp.endID = 8;
	temp.rootID = 7;
	temp.sentenceID = 1;

	res2.push_back(temp);

	QTest::newRow("TwoAppeals")<< context2 << res2;

	/////////////////////////////////////���� 4
	/////////////////////////////////////NoAppeals

	wordCount = 4;
	StanfordWord* arr3 = new StanfordWord [wordCount]; //������ - �����������
	
	arr3[0] = StanfordWord(1,1, QString("Give"),QString("give"),VB, WORD, QVector<corefCoord>());
	arr3[1] = StanfordWord(1,2, QString("us"),QString("we"),PRP, WORD, QVector<corefCoord>());
	arr3[2] = StanfordWord(1,3, QString("that"),QString("that"),IN, WORD, QVector<corefCoord>());
	arr3[3] = StanfordWord(1,4, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	
	QList<StanfordWord*> tmp3; // ������ �� ����� (������������� ������ � ���������)

	for(int i = 0; i < wordCount; i++)
	{
		tmp3.push_back(arr3+i);
	}

	QList< textTree *> childList3;

	sentence context3(tmp3, childList3);

	QVector< corefCoord > res3;
	
	QTest::newRow("NoAppeals")<< context3 << res3;

	/////////////////////////////////////���� 5
	/////////////////////////////////////complexAppealAdjective

	wordCount = 7;
	StanfordWord* arr4 = new StanfordWord [wordCount]; //������ - �����������
	
	arr4[0] = StanfordWord(1,1, QString("Give"),QString("give"),VB, WORD, QVector<corefCoord>());
	arr4[1] = StanfordWord(1,2, QString("us"),QString("we"),PRP, WORD, QVector<corefCoord>());
	arr4[2] = StanfordWord(1,3, QString("that"),QString("that"),IN, WORD, QVector<corefCoord>());
	arr4[3] = StanfordWord(1,4, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr4[4] = StanfordWord(1,5, QString("dear"),QString("dear"),RB, WORD, QVector<corefCoord>());
	arr4[5] = StanfordWord(1,6, QString("friend"),QString("friend"),NN, WORD, QVector<corefCoord>());
	arr4[6] = StanfordWord(1,7, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp4; // ������ �� ����� (������������� ������ � ���������)

	for(int i = 0; i < wordCount; i++)
	{
		tmp4.push_back(arr4+i);
	}

	QList< textTree *> childList4;

	sentence context4(tmp4, childList4);

	QVector< corefCoord > res4;

	temp.startID = 5;
	temp.endID = 7;
	temp.rootID = 6;
	temp.sentenceID = 1;
	
	res4.push_back(temp);

	QTest::newRow("complexAppealAdjective")<< context4 << res4;

	/////////////////////////////////////���� 6
	/////////////////////////////////////complexAppealYou

	wordCount = 7;
	StanfordWord* arr5 = new StanfordWord [wordCount]; //������ - �����������
	
	arr5[0] = StanfordWord(1,1, QString("Give"),QString("give"),VB, WORD, QVector<corefCoord>());
	arr5[1] = StanfordWord(1,2, QString("us"),QString("we"),PRP, WORD, QVector<corefCoord>());
	arr5[2] = StanfordWord(1,3, QString("that"),QString("that"),IN, WORD, QVector<corefCoord>());
	arr5[3] = StanfordWord(1,4, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr5[4] = StanfordWord(1,5, QString("you"),QString("you"),PRP, WORD, QVector<corefCoord>());
	arr5[5] = StanfordWord(1,6, QString("friend"),QString("friend"),NN, WORD, QVector<corefCoord>());
	arr5[6] = StanfordWord(1,7, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp5; // ������ �� ����� (������������� ������ � ���������)

	for(int i = 0; i < wordCount; i++)
	{
		tmp5.push_back(arr5+i);
	}

	QList< textTree *> childList5;

	sentence context5(tmp5, childList5);

	QVector< corefCoord > res5;

	temp.startID = 5;
	temp.endID = 7;
	temp.rootID = 6;
	temp.sentenceID = 1;
	
	res5.push_back(temp);

	QTest::newRow("complexAppealYou")<< context5 << res5;

	/////////////////////////////////////���� 7
	/////////////////////////////////////complexAppealOfficialAppeal

	wordCount = 7;
	StanfordWord* arr6 = new StanfordWord [wordCount]; //������ - �����������
	
	arr6[0] = StanfordWord(1,1, QString("Give"),QString("give"),VB, WORD, QVector<corefCoord>());
	arr6[1] = StanfordWord(1,2, QString("us"),QString("we"),PRP, WORD, QVector<corefCoord>());
	arr6[2] = StanfordWord(1,3, QString("that"),QString("that"),IN, WORD, QVector<corefCoord>());
	arr6[3] = StanfordWord(1,4, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr6[4] = StanfordWord(1,5, QString("Mr."),QString("Mr."),NNP, WORD, QVector<corefCoord>());
	arr6[5] = StanfordWord(1,6, QString("Andersen"),QString("Andersen"),NNP, WORD, QVector<corefCoord>());
	arr6[6] = StanfordWord(1,7, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp6; // ������ �� ����� (������������� ������ � ���������)

	for(int i = 0; i < wordCount; i++)
	{
		tmp6.push_back(arr6+i);
	}

	QList< textTree *> childList6;

	sentence context6(tmp6, childList6);

	QVector< corefCoord > res6;

	temp.startID = 5;
	temp.endID = 7;
	temp.rootID = 6;
	temp.sentenceID = 1;
	
	res6.push_back(temp);

	QTest::newRow("complexAppealOfficialAppeal")<< context6 << res6;

	/////////////////////////////////////���� 8
	/////////////////////////////////////complexAppealTwoAdjective

	wordCount = 8;
	StanfordWord* arr7 = new StanfordWord [wordCount]; //������ - �����������
	
	arr7[0] = StanfordWord(1,1, QString("Give"),QString("give"),VB, WORD, QVector<corefCoord>());
	arr7[1] = StanfordWord(1,2, QString("us"),QString("we"),PRP, WORD, QVector<corefCoord>());
	arr7[2] = StanfordWord(1,3, QString("that"),QString("that"),IN, WORD, QVector<corefCoord>());
	arr7[3] = StanfordWord(1,4, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr7[4] = StanfordWord(1,5, QString("dear"),QString("dear"),RB, WORD, QVector<corefCoord>());
	arr7[5] = StanfordWord(1,6, QString("old"),QString("old"),RB, WORD, QVector<corefCoord>());
	arr7[6] = StanfordWord(1,7, QString("friend"),QString("friend"),NN, WORD, QVector<corefCoord>());
	arr7[7] = StanfordWord(1,8, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp7; // ������ �� ����� (������������� ������ � ���������)

	for(int i = 0; i < wordCount; i++)
	{
		tmp7.push_back(arr7+i);
	}

	QList< textTree *> childList7;

	sentence context7(tmp7, childList7);

	QVector< corefCoord > res7;

	temp.startID = 5;
	temp.endID = 8;
	temp.rootID = 7;
	temp.sentenceID = 1;
	
	res7.push_back(temp);

	QTest::newRow("complexAppealTwoAdjective")<< context7 << res7;

	/////////////////////////////////////���� 9
	/////////////////////////////////////AddressAtTheBeginning

	wordCount = 6;
	StanfordWord* arr8 = new StanfordWord [wordCount]; //������ - �����������

	arr8[0] = StanfordWord(1,1, QString("Deagol"),QString("Deagol"),NNP, WORD, QVector<corefCoord>());
	arr8[1] = StanfordWord(1,2, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr8[2] = StanfordWord(1,3, QString("Give"),QString("give"),VB, WORD, QVector<corefCoord>());
	arr8[3] = StanfordWord(1,4, QString("us"),QString("we"),PRP, WORD, QVector<corefCoord>());
	arr8[4] = StanfordWord(1,5, QString("that"),QString("that"),IN, WORD, QVector<corefCoord>());
	arr8[5] = StanfordWord(1,6, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());


	QList<StanfordWord*> tmp8; // ������ �� ����� (������������� ������ � ���������)

	for(int i = 0; i < wordCount; i++)
	{
		tmp8.push_back(arr8+i);
	}

	QList< textTree *> childList8;

	sentence context8(tmp8, childList8);

	QVector< corefCoord > res8;

	temp.startID = 1;
	temp.endID = 2;
	temp.rootID = 1;
	temp.sentenceID = 1;
	
	res8.push_back(temp);

	QTest::newRow("AddressAtTheBeginning")<< context8 << res8;

	/////////////////////////////////////���� 10
	/////////////////////////////////////AddressAtTheEnd

	StanfordWord* arr9 = new StanfordWord [6]; //������ - �����������

	arr9[0] = StanfordWord(1,1, QString("Give"),QString("give"),VB, WORD, QVector<corefCoord>());
	arr9[1] = StanfordWord(1,2, QString("us"),QString("we"),PRP, WORD, QVector<corefCoord>());
	arr9[2] = StanfordWord(1,3, QString("that"),QString("that"),IN, WORD, QVector<corefCoord>());
	arr9[3] = StanfordWord(1,4, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr9[4] = StanfordWord(1,5, QString("Deagol"),QString("Deagol"),NNP, WORD, QVector<corefCoord>());
	arr9[5] = StanfordWord(1,6, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp9; // ������ �� ����� (������������� ������ � ���������)
	tmp9.push_back(arr9+0);
	tmp9.push_back(arr9+1);
	tmp9.push_back(arr9+2);
	tmp9.push_back(arr9+3);
	tmp9.push_back(arr9+4);
	tmp9.push_back(arr9+5);

	QList< textTree *> childList9;

	sentence context9(tmp9, childList9);

	QVector< corefCoord > res9;
	
	temp.startID = 5;
	temp.endID = 6;
	temp.rootID = 5;
	temp.sentenceID = 1;

	res9.push_back(temp);

	QTest::newRow("AddressAtTheEnd")<< context9 << res9;

	/////////////////////////////////////���� 11
	/////////////////////////////////////EndOfSentenceIsExclamationPoint

	StanfordWord* arr10 = new StanfordWord [6]; //������ - �����������

	arr10[0] = StanfordWord(1,1, QString("Give"),QString("give"),VB, WORD, QVector<corefCoord>());
	arr10[1] = StanfordWord(1,2, QString("us"),QString("we"),PRP, WORD, QVector<corefCoord>());
	arr10[2] = StanfordWord(1,3, QString("that"),QString("that"),IN, WORD, QVector<corefCoord>());
	arr10[3] = StanfordWord(1,4, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr10[4] = StanfordWord(1,5, QString("Deagol"),QString("Deagol"),NNP, WORD, QVector<corefCoord>());
	arr10[5] = StanfordWord(1,6, QString("!"),QString("!"),SentFinPunct, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp10; // ������ �� ����� (������������� ������ � ���������)
	tmp10.push_back(arr10+0);
	tmp10.push_back(arr10+1);
	tmp10.push_back(arr10+2);
	tmp10.push_back(arr10+3);
	tmp10.push_back(arr10+4);
	tmp10.push_back(arr10+5);

	QList< textTree *> childList10;

	sentence context10(tmp10, childList10);

	QVector< corefCoord > res10;
	
	temp.startID = 5;
	temp.endID = 6;
	temp.rootID = 5;
	temp.sentenceID = 1;

	res10.push_back(temp);

	QTest::newRow("EndOfSentenceIsExclamationPoint")<< context10 << res10;

	/////////////////////////////////////���� 12
	/////////////////////////////////////EndOfSentenceIsQuestionMark

	wordCount = 6;
	StanfordWord* arr11 = new StanfordWord [wordCount]; //������ - �����������

	arr11[0] = StanfordWord(1,1, QString("Give"),QString("give"),VB, WORD, QVector<corefCoord>());
	arr11[1] = StanfordWord(1,2, QString("us"),QString("we"),PRP, WORD, QVector<corefCoord>());
	arr11[2] = StanfordWord(1,3, QString("that"),QString("that"),IN, WORD, QVector<corefCoord>());
	arr11[3] = StanfordWord(1,4, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr11[4] = StanfordWord(1,5, QString("Deagol"),QString("Deagol"),NNP, WORD, QVector<corefCoord>());
	arr11[5] = StanfordWord(1,6, QString("?"),QString("?"),SentFinPunct, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp11; // ������ �� ����� (������������� ������ � ���������)
	for(int i = 0; i < wordCount; i++)
	{
		tmp11.push_back(arr11+i);
	}

	QList< textTree *> childList11;

	sentence context11(tmp11, childList11);

	QVector< corefCoord > res11;
	
	temp.startID = 5;
	temp.endID = 6;
	temp.rootID = 5;
	temp.sentenceID = 1;

	res11.push_back(temp);

	QTest::newRow("EndOfSentenceIsQuestionMark")<< context11 << res11;

	/////////////////////////////////////���� 13
	/////////////////////////////////////EndOfSentenceIsSemicolon

	wordCount = 6;
	StanfordWord* arr12 = new StanfordWord [wordCount]; //������ - �����������

	arr12[0] = StanfordWord(1,1, QString("Give"),QString("give"),VB, WORD, QVector<corefCoord>());
	arr12[1] = StanfordWord(1,2, QString("us"),QString("we"),PRP, WORD, QVector<corefCoord>());
	arr12[2] = StanfordWord(1,3, QString("that"),QString("that"),IN, WORD, QVector<corefCoord>());
	arr12[3] = StanfordWord(1,4, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr12[4] = StanfordWord(1,5, QString("Deagol"),QString("Deagol"),NNP, WORD, QVector<corefCoord>());
	arr12[5] = StanfordWord(1,6, QString(";"),QString(";"),Colon, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp12; // ������ �� ����� (������������� ������ � ���������)
	for(int i = 0; i < wordCount; i++)
	{
		tmp12.push_back(arr12+i);
	}

	QList< textTree *> childList12;

	sentence context12(tmp12, childList12);

	QVector< corefCoord > res12;
	
	temp.startID = 5;
	temp.endID = 6;
	temp.rootID = 5;
	temp.sentenceID = 1;

	res12.push_back(temp);

	QTest::newRow("EndOfSentenceIsSemicolon")<< context12 << res12;

	/////////////////////////////////////���� 14
	/////////////////////////////////////AllSentenceIsAppeal

	wordCount = 2;
	StanfordWord* arr13 = new StanfordWord [wordCount]; //������ - �����������

	arr13[0] = StanfordWord(1,1, QString("Deagol"),QString("Deagol"),NNP, WORD, QVector<corefCoord>());
	arr13[1] = StanfordWord(1,2, QString("!"),QString("!"),SentFinPunct, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp13; // ������ �� ����� (������������� ������ � ���������)
	for(int i = 0; i < wordCount; i++)
	{
		tmp13.push_back(arr13+i);
	}

	QList< textTree *> childList13;

	sentence context13(tmp13, childList13);

	QVector< corefCoord > res13;
	
	temp.startID = 1;
	temp.endID = 2;
	temp.rootID = 1;
	temp.sentenceID = 1;

	res13.push_back(temp);

	QTest::newRow("AllSentenceIsAppeal")<< context13 << res13;

	/////////////////////////////////////���� 15
	/////////////////////////////////////EndOfSentenceIsDash

	wordCount = 6;
	StanfordWord* arr14 = new StanfordWord [wordCount]; //������ - �����������

	arr14[0] = StanfordWord(1,1, QString("Give"),QString("give"),VB, WORD, QVector<corefCoord>());
	arr14[1] = StanfordWord(1,2, QString("us"),QString("we"),PRP, WORD, QVector<corefCoord>());
	arr14[2] = StanfordWord(1,3, QString("that"),QString("that"),IN, WORD, QVector<corefCoord>());
	arr14[3] = StanfordWord(1,4, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr14[4] = StanfordWord(1,5, QString("Deagol"),QString("Deagol"),NNP, WORD, QVector<corefCoord>());
	arr14[5] = StanfordWord(1,6, QString("-"),QString("-"),Colon, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp14; // ������ �� ����� (������������� ������ � ���������)
	for(int i = 0; i < wordCount; i++)
	{
		tmp14.push_back(arr14+i);
	}

	QList< textTree *> childList14;

	sentence context14(tmp14, childList14);

	QVector< corefCoord > res14;
	
	temp.startID = 5;
	temp.endID = 6;
	temp.rootID = 5;
	temp.sentenceID = 1;

	res14.push_back(temp);

	QTest::newRow("EndOfSentenceIsDash")<< context14 << res14;

	/////////////////////////////////////���� 16
	/////////////////////////////////////EndOfSentenceIsColon

	wordCount = 6;
	StanfordWord* arr15 = new StanfordWord [wordCount]; //������ - �����������

	arr15[0] = StanfordWord(1,1, QString("Give"),QString("give"),VB, WORD, QVector<corefCoord>());
	arr15[1] = StanfordWord(1,2, QString("us"),QString("we"),PRP, WORD, QVector<corefCoord>());
	arr15[2] = StanfordWord(1,3, QString("that"),QString("that"),IN, WORD, QVector<corefCoord>());
	arr15[3] = StanfordWord(1,4, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr15[4] = StanfordWord(1,5, QString("Deagol"),QString("Deagol"),NNP, WORD, QVector<corefCoord>());
	arr15[5] = StanfordWord(1,6, QString(":"),QString(":"),Colon, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp15; // ������ �� ����� (������������� ������ � ���������)
	for(int i = 0; i < wordCount; i++)
	{
		tmp15.push_back(arr15+i);
	}

	QList< textTree *> childList15;

	sentence context15(tmp15, childList15);

	QVector< corefCoord > res15;
	
	temp.startID = 5;
	temp.endID = 6;
	temp.rootID = 5;
	temp.sentenceID = 1;

	res15.push_back(temp);

	QTest::newRow("EndOfSentenceIsColon")<< context15 << res15;

	/////////////////////////////////////���� 17
	/////////////////////////////////////EndOfSentenceIsEllipsis

	wordCount = 6;
	StanfordWord* arr16 = new StanfordWord [wordCount]; //������ - �����������

	arr16[0] = StanfordWord(1,1, QString("Give"),QString("give"),VB, WORD, QVector<corefCoord>());
	arr16[1] = StanfordWord(1,2, QString("us"),QString("we"),PRP, WORD, QVector<corefCoord>());
	arr16[2] = StanfordWord(1,3, QString("that"),QString("that"),IN, WORD, QVector<corefCoord>());
	arr16[3] = StanfordWord(1,4, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr16[4] = StanfordWord(1,5, QString("Deagol"),QString("Deagol"),NNP, WORD, QVector<corefCoord>());
	arr16[5] = StanfordWord(1,6, QString("..."),QString("..."),Colon, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp16; // ������ �� ����� (������������� ������ � ���������)
	for(int i = 0; i < wordCount; i++)
	{
		tmp16.push_back(arr16+i);
	}

	QList< textTree *> childList16;

	sentence context16(tmp16, childList16);

	QVector< corefCoord > res16;
	
	temp.startID = 5;
	temp.endID = 6;
	temp.rootID = 5;
	temp.sentenceID = 1;

	res16.push_back(temp);

	QTest::newRow("EndOfSentenceIsEllipsis")<< context16 << res16;

	/////////////////////////////////////���� 18
	/////////////////////////////////////EndOfSentenceIsBracket

	wordCount = 6;
	StanfordWord* arr17 = new StanfordWord [wordCount]; //������ - �����������

	arr17[0] = StanfordWord(1,1, QString("Give"),QString("give"),VB, WORD, QVector<corefCoord>());
	arr17[1] = StanfordWord(1,2, QString("us"),QString("we"),PRP, WORD, QVector<corefCoord>());
	arr17[2] = StanfordWord(1,3, QString("that"),QString("that"),IN, WORD, QVector<corefCoord>());
	arr17[3] = StanfordWord(1,4, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr17[4] = StanfordWord(1,5, QString("Deagol"),QString("Deagol"),NNP, WORD, QVector<corefCoord>());
	arr17[5] = StanfordWord(1,6, QString(")"),QString("-lrb-"),LRB, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp17; // ������ �� ����� (������������� ������ � ���������)
	for(int i = 0; i < wordCount; i++)
	{
		tmp17.push_back(arr17+i);
	}

	QList< textTree *> childList17;

	sentence context17(tmp17, childList17);

	QVector< corefCoord > res17;
	
	temp.startID = 5;
	temp.endID = 6;
	temp.rootID = 5;
	temp.sentenceID = 1;

	res17.push_back(temp);

	QTest::newRow("EndOfSentenceIsBracket")<< context17 << res17;

	/////////////////////////////////////���� 19
	/////////////////////////////////////AddressBeginsWithSemicolon

	wordCount = 6;
	StanfordWord* arr18 = new StanfordWord [wordCount]; //������ - �����������

	arr18[0] = StanfordWord(1,1, QString("Give"),QString("give"),VB, WORD, QVector<corefCoord>());
	arr18[1] = StanfordWord(1,2, QString("us"),QString("we"),PRP, WORD, QVector<corefCoord>());
	arr18[2] = StanfordWord(1,3, QString("that"),QString("that"),IN, WORD, QVector<corefCoord>());
	arr18[3] = StanfordWord(1,4, QString(";"),QString(";"),Colon, WORD, QVector<corefCoord>());
	arr18[4] = StanfordWord(1,5, QString("Deagol"),QString("Deagol"),NNP, WORD, QVector<corefCoord>());
	arr18[5] = StanfordWord(1,6, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp18; // ������ �� ����� (������������� ������ � ���������)
	for(int i = 0; i < wordCount; i++)
	{
		tmp18.push_back(arr18+i);
	}

	QList< textTree *> childList18;

	sentence context18(tmp18, childList18);

	QVector< corefCoord > res18;
	
	temp.startID = 5;
	temp.endID = 6;
	temp.rootID = 5;
	temp.sentenceID = 1;

	res18.push_back(temp);

	QTest::newRow("AddressBeginsWithSemicolon")<< context18 << res18;

	/////////////////////////////////////���� 20
	/////////////////////////////////////AddressBeginsWithDash

	wordCount = 6;
	StanfordWord* arr19 = new StanfordWord [wordCount]; //������ - �����������

	arr19[0] = StanfordWord(1,1, QString("Give"),QString("give"),VB, WORD, QVector<corefCoord>());
	arr19[1] = StanfordWord(1,2, QString("us"),QString("we"),PRP, WORD, QVector<corefCoord>());
	arr19[2] = StanfordWord(1,3, QString("that"),QString("that"),IN, WORD, QVector<corefCoord>());
	arr19[3] = StanfordWord(1,4, QString("-"),QString("-"),Colon, WORD, QVector<corefCoord>());
	arr19[4] = StanfordWord(1,5, QString("Deagol"),QString("Deagol"),NNP, WORD, QVector<corefCoord>());
	arr19[5] = StanfordWord(1,6, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp19; // ������ �� ����� (������������� ������ � ���������)
	for(int i = 0; i < wordCount; i++)
	{
		tmp19.push_back(arr19+i);
	}

	QList< textTree *> childList19;

	sentence context19(tmp19, childList19);

	QVector< corefCoord > res19;
	
	temp.startID = 5;
	temp.endID = 6;
	temp.rootID = 5;
	temp.sentenceID = 1;

	res19.push_back(temp);

	QTest::newRow("AddressBeginsWithDash")<< context19 << res19;

	/////////////////////////////////////���� 21
	/////////////////////////////////////AddressBeginsWithColon

	wordCount = 6;
	StanfordWord* arr20 = new StanfordWord [wordCount]; //������ - �����������

	arr20[0] = StanfordWord(1,1, QString("Give"),QString("give"),VB, WORD, QVector<corefCoord>());
	arr20[1] = StanfordWord(1,2, QString("us"),QString("we"),PRP, WORD, QVector<corefCoord>());
	arr20[2] = StanfordWord(1,3, QString("that"),QString("that"),IN, WORD, QVector<corefCoord>());
	arr20[3] = StanfordWord(1,4, QString(":"),QString(":"),Colon, WORD, QVector<corefCoord>());
	arr20[4] = StanfordWord(1,5, QString("Deagol"),QString("Deagol"),NNP, WORD, QVector<corefCoord>());
	arr20[5] = StanfordWord(1,6, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp20; // ������ �� ����� (������������� ������ � ���������)
	for(int i = 0; i < wordCount; i++)
	{
		tmp20.push_back(arr20+i);
	}

	QList< textTree *> childList20;

	sentence context20(tmp20, childList20);

	QVector< corefCoord > res20;
	
	temp.startID = 5;
	temp.endID = 6;
	temp.rootID = 5;
	temp.sentenceID = 1;

	res20.push_back(temp);

	QTest::newRow("AddressBeginsWithColon")<< context20 << res20;

	/////////////////////////////////////���� 22
	/////////////////////////////////////AddressBeginsWithEllipsis

	wordCount = 6;
	StanfordWord* arr21 = new StanfordWord [wordCount]; //������ - �����������

	arr21[0] = StanfordWord(1,1, QString("Give"),QString("give"),VB, WORD, QVector<corefCoord>());
	arr21[1] = StanfordWord(1,2, QString("us"),QString("we"),PRP, WORD, QVector<corefCoord>());
	arr21[2] = StanfordWord(1,3, QString("that"),QString("that"),IN, WORD, QVector<corefCoord>());
	arr21[3] = StanfordWord(1,4, QString("..."),QString("..."),Colon, WORD, QVector<corefCoord>());
	arr21[4] = StanfordWord(1,5, QString("Deagol"),QString("Deagol"),NNP, WORD, QVector<corefCoord>());
	arr21[5] = StanfordWord(1,6, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp21; // ������ �� ����� (������������� ������ � ���������)
	for(int i = 0; i < wordCount; i++)
	{
		tmp21.push_back(arr21+i);
	}

	QList< textTree *> childList21;

	sentence context21(tmp21, childList21);

	QVector< corefCoord > res21;
	
	temp.startID = 5;
	temp.endID = 6;
	temp.rootID = 5;
	temp.sentenceID = 1;

	res21.push_back(temp);

	QTest::newRow("AddressBeginsWithEllipsis")<< context21 << res21;

	/////////////////////////////////////���� 23
	/////////////////////////////////////AddressBetweenDashes

	wordCount = 6;
	StanfordWord* arr22 = new StanfordWord [wordCount]; //������ - �����������

	arr22[0] = StanfordWord(1,1, QString("Give"),QString("give"),VB, WORD, QVector<corefCoord>());
	arr22[1] = StanfordWord(1,2, QString("us"),QString("we"),PRP, WORD, QVector<corefCoord>());
	arr22[2] = StanfordWord(1,3, QString("that"),QString("that"),IN, WORD, QVector<corefCoord>());
	arr22[3] = StanfordWord(1,4, QString("-"),QString("-"),Colon, WORD, QVector<corefCoord>());
	arr22[4] = StanfordWord(1,5, QString("Deagol"),QString("Deagol"),NNP, WORD, QVector<corefCoord>());
	arr22[5] = StanfordWord(1,6, QString("-"),QString("-"),Colon, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp22; // ������ �� ����� (������������� ������ � ���������)
	for(int i = 0; i < wordCount; i++)
	{
		tmp22.push_back(arr22+i);
	}

	QList< textTree *> childList22;

	sentence context22(tmp22, childList22);

	QVector< corefCoord > res22;
	
	temp.startID = 5;
	temp.endID = 6;
	temp.rootID = 5;
	temp.sentenceID = 1;

	res22.push_back(temp);

	QTest::newRow("AddressBetweenDashes")<< context22 << res22;
}

void TestClass::testappealSearch()
{
	QFETCH(sentence, context);
	QFETCH(QVector< corefCoord > , appealCoords);

	QVector<corefCoord > coordsTmp;

	appealSearch(context, coordsTmp);

	QVector<corefCoord> Forgotten, superfluous;

	bool isEqu = isEqualVectors(appealCoords, coordsTmp, Forgotten, superfluous);

	QString message = QString("\nResult checking failed\n\nForgotten: \n");

	for(int i = 0; i < Forgotten.size(); i++)
	{
		message+= QString("\nstartID:") + QString(QString::number(Forgotten[i].startID)) + QString("\nendID:") + QString(QString::number(Forgotten[i].endID))  + QString("\nrootID:") + QString(QString::number(Forgotten[i].rootID))+ QString("\nsentID:") + QString(QString::number(Forgotten[i].sentenceID)) + QString("\n");
	}
	message+= QString("\n\nSuperfluous: \n");
	
	for(int i = 0; i < superfluous.size(); i++)
	{
		message+= QString("\nstartID:") + QString(QString::number(superfluous[i].startID)) + QString("\nendID:") + QString(QString::number(superfluous[i].endID))  + QString("\nrootID:") + QString(QString::number(superfluous[i].rootID))+ QString("\nsentID:") + QString(QString::number(superfluous[i].sentenceID)) + QString("\n");
	}

	QVERIFY2(isEqu, message.toUtf8().constData());

	delete [] context.words[0];
}

void TestClass::testCorefFinder_data()
{
	QTest::addColumn<StanfordWord*>("Word");
	QTest::addColumn<QMap <QString, StanfordWord*> >("Text");
	QTest::addColumn<QSet <person> >("allPersons");
	QTest::addColumn<person>("Person");

	StanfordWord* arr = new StanfordWord [5];
	QVector<corefCoord> he1;
	corefCoord temp;
	temp.startID = 2;
	temp.endID = 3;
	temp.rootID = 2;
	temp.sentenceID = 1;

	he1.push_back(temp);

	temp.startID = 3;
	temp.endID = 4;
	temp.rootID = 3;
	temp.sentenceID = 1;

	he1.push_back(temp);

	QVector<corefCoord> he2;
	temp.startID = 2;
	temp.endID = 3;
	temp.rootID = 2;
	temp.sentenceID = 1;

	he2.push_back(temp);

	temp.startID = 3;
	temp.endID = 4;
	temp.rootID = 3;
	temp.sentenceID = 1;

	he2.push_back(temp);

	temp.startID = 4;
	temp.endID = 5;
	temp.rootID = 4;
	temp.sentenceID = 1;

	he2.push_back(temp);

	arr[0] = StanfordWord(1,1, QString("he"),QString("he"),PRP, WORD, he1);
	arr[1] = StanfordWord(1,2, QString("friend"),QString("friend"),NN, WORD, QVector<corefCoord>());
	arr[2] = StanfordWord(1,3, QString("hobbit"),QString("hobbit"),NN, WORD, QVector<corefCoord>());
	arr[3] = StanfordWord(1,4, QString("Smeagol"),QString("smeagol"),NN, WORD, QVector<corefCoord>());
	arr[4] = StanfordWord(1,5, QString("he"),QString("he"),PRP, WORD, he2);

	QMap<QString, StanfordWord*> Text;

	for(int i = 0; i < 5; i++)
		Text.insert(arr[i].doKey(),arr+i);
	
	QSet  <person> allPersons;

	allPersons.insert(person(QString("Smeagol"), QStringList(QString("Smeagol")), QStringList(QString("friend")), QString("0000ff")));
	allPersons.insert(person(QString("Deagol"), QStringList(QString("Deagol")), QStringList(), QString("0000ff")));

	/////////////////////////////////////���� 2
	/////////////////////////////////////wordHasNotConnectionWithName

	person res;

	QTest::newRow("wordHasNotConnectionWithName")<< arr << Text << allPersons << res;

	/////////////////////////////////////���� 1
	/////////////////////////////////////wordHasConnectionWithName

	res = person(QString("Smeagol"), QStringList(QString("Smeagol")), QStringList(QString("friend")), QString("0000ff"));

	QTest::newRow("wordHasConnectionWithName")<< arr + 4 << Text << allPersons << res;
}

void TestClass::testCorefFinder()
{
	QFETCH(StanfordWord*, Word);
	QFETCH(newQMapName,Text);
	QFETCH(QSet <person>, allPersons);
	QFETCH(person, Person);

	person::InitPer(allPersons);

	person per;
	corefFinder(Text, Word, per);

	QVERIFY2(Person == per, QString(QString("Result checking failed: real:") + per.name + QString("res:")+ Person.name).toUtf8().constData());//�����!!!
	
	if(Person == person(QString("Smeagol"), QStringList(QString("Smeagol")), QStringList(QString("friend")), QString("0000ff")))
		delete [] Text[QString("1 1")];
}

void TestClass::testSubstructSubjectBySubjQualifier_data()
{
	QTest::addColumn<int>("wordID");
	QTest::addColumn<int>("sentID");
	QTest::addColumn<sentence>("context");
	QTest::addColumn<QSet <person> >("allPersons");
	QTest::addColumn<QSet <person> >("speakersList");

	/////////////////////////////////////���� 1
	/////////////////////////////////////TwoCharactersOneQualifier

	StanfordWord* arr = new StanfordWord [6]; //������ - �����������

	arr[0] = StanfordWord(1,1, QString("Give"),QString("give"),VB, WORD, QVector<corefCoord>());
	arr[1] = StanfordWord(1,2, QString("us"),QString("we"),PRP, WORD, QVector<corefCoord>());
	arr[2] = StanfordWord(1,3, QString("that"),QString("that"),IN, WORD, QVector<corefCoord>());
	arr[3] = StanfordWord(1,4, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr[4] = StanfordWord(1,5, QString("friend"),QString("friend"),NN, WORD, QVector<corefCoord>());
	arr[5] = StanfordWord(1,6, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp; // ������ �� ����� (������������� ������ � ���������)
	tmp.push_back(arr+0);
	tmp.push_back(arr+1);
	tmp.push_back(arr+2);
	tmp.push_back(arr+3);
	tmp.push_back(arr+4);
	tmp.push_back(arr+5);

	QList< textTree *> childList;

	sentence context(tmp, childList);

	QSet  <person> allPersons, speakersList;
	QStringList first1;
	first1 << QString("friend");

	allPersons.insert(person(QString("Smeagol"), QStringList(QString("Smeagol")), first1, QString("0000ff")));
	allPersons.insert(person(QString("Deagol"), QStringList(QString("Deagol")), QStringList(), QString("0000ff")));

	speakersList.insert(person(QString("Smeagol"), QStringList(QString("Smeagol")), first1, QString("0000ff")));

	QTest::newRow("TwoCharactersOneQualifier")<< 5 << 1 << context << allPersons << speakersList;
	
	/////////////////////////////////////���� 2
	/////////////////////////////////////severalSubjectiveSpecifiersInherentCharacter

	StanfordWord* arr1 = new StanfordWord [6]; //������ - �����������

	arr1[0] = StanfordWord(1,1, QString("Give"),QString("give"),VB, WORD, QVector<corefCoord>());
	arr1[1] = StanfordWord(1,2, QString("us"),QString("we"),PRP, WORD, QVector<corefCoord>());
	arr1[2] = StanfordWord(1,3, QString("that"),QString("that"),IN, WORD, QVector<corefCoord>());
	arr1[3] = StanfordWord(1,4, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr1[4] = StanfordWord(1,5, QString("friend"),QString("friend"),NN, WORD, QVector<corefCoord>());
	arr1[5] = StanfordWord(1,6, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp1; // ������ �� ����� (������������� ������ � ���������)
	tmp1.push_back(arr1+0);
	tmp1.push_back(arr1+1);
	tmp1.push_back(arr1+2);
	tmp1.push_back(arr1+3);
	tmp1.push_back(arr1+4);
	tmp1.push_back(arr1+5);

	QList< textTree *> childList1;

	sentence context1(tmp1, childList1);

	QSet  <person> allPersons1, speakersList1;

	QStringList first;
	first.push_back(QString("friend"));
	first.push_back(QString("my love"));

	allPersons1.insert(person(QString("Smeagol"), QStringList(QString("Smeagol")), first, QString("0000ff")));
	allPersons1.insert(person(QString("Deagol"), QStringList(QString("Deagol")), QStringList(), QString("0000ff")));

	speakersList1.insert(person(QString("Smeagol"), QStringList(QString("Smeagol")), first, QString("0000ff")));

	QTest::newRow("severalSubjectiveSpecifiersInherentCharacter")<< 5 << 1 << context1 << allPersons1 << speakersList1;

	/////////////////////////////////////���� 3
	/////////////////////////////////////TwoCharacterHasOneQualifier

	StanfordWord* arr2 = new StanfordWord [6]; //������ - �����������

	arr2[0] = StanfordWord(1,1, QString("Give"),QString("give"),VB, WORD, QVector<corefCoord>());
	arr2[1] = StanfordWord(1,2, QString("us"),QString("we"),PRP, WORD, QVector<corefCoord>());
	arr2[2] = StanfordWord(1,3, QString("that"),QString("that"),IN, WORD, QVector<corefCoord>());
	arr2[3] = StanfordWord(1,4, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr2[4] = StanfordWord(1,5, QString("friend"),QString("friend"),NN, WORD, QVector<corefCoord>());
	arr2[5] = StanfordWord(1,6, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp2; // ������ �� ����� (������������� ������ � ���������)
	tmp2.push_back(arr2+0);
	tmp2.push_back(arr2+1);
	tmp2.push_back(arr2+2);
	tmp2.push_back(arr2+3);
	tmp2.push_back(arr2+4);
	tmp2.push_back(arr2+5);

	QList< textTree *> childList2;

	sentence context2(tmp2, childList2);

	QSet  <person> allPersons2, speakersList2;

	QStringList first2;
	first2.push_back(QString("friend"));

	QStringList second2;
	second2.push_back(QString("friend"));

	allPersons2.insert(person(QString("Smeagol"), QStringList(QString("Smeagol")), first2, QString("0000ff")));
	allPersons2.insert(person(QString("Deagol"), QStringList(QString("Deagol")), second2, QString("0000ff")));

	speakersList2.insert(person(QString("Smeagol"), QStringList(QString("Smeagol")), first2, QString("0000ff")));
	speakersList2.insert(person(QString("Deagol"), QStringList(QString("Deagol")), second2, QString("0000ff")));

	QTest::newRow("TwoCharacterHasOneQualifier")<< 5 << 1 << context2 << allPersons2 << speakersList2;

	/////////////////////////////////////���� 5
	/////////////////////////////////////CharactersHasNotQualifier

	StanfordWord* arr3 = new StanfordWord [6]; //������ - �����������

	arr3[0] = StanfordWord(1,1, QString("Give"),QString("give"),VB, WORD, QVector<corefCoord>());
	arr3[1] = StanfordWord(1,2, QString("us"),QString("we"),PRP, WORD, QVector<corefCoord>());
	arr3[2] = StanfordWord(1,3, QString("that"),QString("that"),IN, WORD, QVector<corefCoord>());
	arr3[3] = StanfordWord(1,4, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr3[4] = StanfordWord(1,5, QString("friend"),QString("friend"),NN, WORD, QVector<corefCoord>());
	arr3[5] = StanfordWord(1,6, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp3; // ������ �� ����� (������������� ������ � ���������)
	tmp3.push_back(arr3+0);
	tmp3.push_back(arr3+1);
	tmp3.push_back(arr3+2);
	tmp3.push_back(arr3+3);
	tmp3.push_back(arr3+4);
	tmp3.push_back(arr3+5);

	QList< textTree *> childList3;

	sentence context3(tmp3, childList3);

	QSet  <person> allPersons3, speakersList3;

	QStringList first3;
	
	QStringList second3;

	allPersons3.insert(person(QString("Smeagol"), QStringList(QString("Smeagol")), first3, QString("0000ff")));
	allPersons3.insert(person(QString("Deagol"), QStringList(QString("Deagol")), second3, QString("0000ff")));

	QTest::newRow("CharactersHasNotQualifier")<< 5 << 1 << context3 << allPersons3 << speakersList3;
	
	/////////////////////////////////////���� 6
	/////////////////////////////////////qualifierHasAdjective

	StanfordWord* arr4 = new StanfordWord [7]; //������ - �����������

	arr4[0] = StanfordWord(1,1, QString("Give"),QString("give"),VB, WORD, QVector<corefCoord>());
	arr4[1] = StanfordWord(1,2, QString("us"),QString("we"),PRP, WORD, QVector<corefCoord>());
	arr4[2] = StanfordWord(1,3, QString("that"),QString("that"),IN, WORD, QVector<corefCoord>());
	arr4[3] = StanfordWord(1,4, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr4[4] = StanfordWord(1,5, QString("best"),QString("best"),JJS, WORD, QVector<corefCoord>());
	arr4[5] = StanfordWord(1,6, QString("friend"),QString("friend"),NN, WORD, QVector<corefCoord>());
	arr4[6] = StanfordWord(1,7, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp4; // ������ �� ����� (������������� ������ � ���������)
	tmp4.push_back(arr4+0);
	tmp4.push_back(arr4+1);
	tmp4.push_back(arr4+2);
	tmp4.push_back(arr4+3);
	tmp4.push_back(arr4+4);
	tmp4.push_back(arr4+5);
	tmp4.push_back(arr4+6);

	QList< textTree *> childList4;

	sentence context4(tmp4, childList4);

	QSet  <person> allPersons4, speakersList4;

	QStringList first4;
	first4.push_back(QString("friend"));
	first4.push_back(QString("my love"));

	QStringList second4;

	allPersons4.insert(person(QString("Smeagol"), QStringList(QString("Smeagol")), first4, QString("0000ff")));
	allPersons4.insert(person(QString("Deagol"), QStringList(QString("Deagol")), second4, QString("0000ff")));

	speakersList4.insert(person(QString("Smeagol"), QStringList(QString("Smeagol")), first4, QString("0000ff")));

	QTest::newRow("qualifierHasAdjective")<< 6 << 1 << context4 << allPersons4 << speakersList4;

	/////////////////////////////////////���� 7
	/////////////////////////////////////qualifierIsNotFull

	StanfordWord* arr5 = new StanfordWord [6]; //������ - �����������

	arr5[0] = StanfordWord(1,1, QString("Give"),QString("give"),VB, WORD, QVector<corefCoord>());
	arr5[1] = StanfordWord(1,2, QString("us"),QString("we"),PRP, WORD, QVector<corefCoord>());
	arr5[2] = StanfordWord(1,3, QString("that"),QString("that"),IN, WORD, QVector<corefCoord>());
	arr5[3] = StanfordWord(1,4, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr5[4] = StanfordWord(1,5, QString("love"),QString("love"),NN, WORD, QVector<corefCoord>());
	arr5[5] = StanfordWord(1,6, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp5; // ������ �� ����� (������������� ������ � ���������)
	tmp5.push_back(arr5+0);
	tmp5.push_back(arr5+1);
	tmp5.push_back(arr5+2);
	tmp5.push_back(arr5+3);
	tmp5.push_back(arr5+4);
	tmp5.push_back(arr5+5);

	QList< textTree *> childList5;

	sentence context5(tmp5, childList5);

	QSet  <person> allPersons5, speakersList5;

	QStringList first5;
	first5.push_back(QString("friend"));
	first5.push_back(QString("my love"));

	QStringList second5;

	allPersons5.insert(person(QString("Smeagol"), QStringList(QString("Smeagol")), first5, QString("0000ff")));
	allPersons5.insert(person(QString("Deagol"), QStringList(QString("Deagol")), second5, QString("0000ff")));

	speakersList5.insert(person(QString("Smeagol"), QStringList(QString("Smeagol")), first5, QString("0000ff")));

	QTest::newRow("qualifierIsNotFull")<< 5 << 1 << context5 << allPersons5 << speakersList5;

	/////////////////////////////////////���� 8
	/////////////////////////////////////callByName//////////////////////////////////////////////////////////////////////////////////////////////////

	StanfordWord* arr6 = new StanfordWord [6]; //������ - �����������

	arr6[0] = StanfordWord(1,1, QString("Give"),QString("give"),VB, WORD, QVector<corefCoord>());
	arr6[1] = StanfordWord(1,2, QString("us"),QString("we"),PRP, WORD, QVector<corefCoord>());
	arr6[2] = StanfordWord(1,3, QString("that"),QString("that"),IN, WORD, QVector<corefCoord>());
	arr6[3] = StanfordWord(1,4, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr6[4] = StanfordWord(1,5, QString("Deagol"),QString("Deagol"),NNP, WORD, QVector<corefCoord>());
	arr6[5] = StanfordWord(1,6, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp6; // ������ �� ����� (������������� ������ � ���������)
	tmp6.push_back(arr6+0);
	tmp6.push_back(arr6+1);
	tmp6.push_back(arr6+2);
	tmp6.push_back(arr6+3);
	tmp6.push_back(arr6+4);
	tmp6.push_back(arr6+5);

	QList< textTree *> childList6;

	sentence context6(tmp6, childList6);

	QSet  <person> allPersons6, speakersList6;

	QStringList first6;
	first6.push_back(QString("friend"));
	first6.push_back(QString("my love"));

	QStringList second6;

	allPersons6.insert(person(QString("Smeagol"), QStringList(QString("Smeagol")), first6, QString("0000ff")));
	allPersons6.insert(person(QString("Deagol"), QStringList(QString("Deagol")), second6, QString("0000ff")));
	allPersons6.insert(person(QString("Frodo"), QStringList(QString("Frodo")), QStringList(), QString("0000ff")));

	speakersList6.insert(person(QString("Smeagol"), QStringList(QString("Smeagol")), first6, QString("0000ff")));
	speakersList6.insert(person(QString("Frodo"), QStringList(QString("Frodo")), QStringList(), QString("0000ff")));

	QTest::newRow("callByName")<< 5 << 1 << context6 << allPersons6 << speakersList6;

	/////////////////////////////////////���� 9
	/////////////////////////////////////CharactersListPartialTruncationBySubjQualifiers

	StanfordWord* arr7 = new StanfordWord [6]; //������ - �����������

	arr7[0] = StanfordWord(1,1, QString("Give"),QString("give"),VB, WORD, QVector<corefCoord>());
	arr7[1] = StanfordWord(1,2, QString("us"),QString("we"),PRP, WORD, QVector<corefCoord>());
	arr7[2] = StanfordWord(1,3, QString("that"),QString("that"),IN, WORD, QVector<corefCoord>());
	arr7[3] = StanfordWord(1,4, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr7[4] = StanfordWord(1,5, QString("friend"),QString("friend"),NN, WORD, QVector<corefCoord>());
	arr7[5] = StanfordWord(1,6, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp7; // ������ �� ����� (������������� ������ � ���������)
	tmp7.push_back(arr7+0);
	tmp7.push_back(arr7+1);
	tmp7.push_back(arr7+2);
	tmp7.push_back(arr7+3);
	tmp7.push_back(arr7+4);
	tmp7.push_back(arr7+5);

	QList< textTree *> childList7;

	sentence context7(tmp7, childList7);

	QSet  <person> allPersons7, speakersList7;

	QStringList first7;
	first7.push_back(QString("friend"));
	first7.push_back(QString("my love"));

	QStringList second7;

	allPersons7.insert(person(QString("Smeagol"), QStringList(QString("Smeagol")), first7, QString("0000ff")));
	allPersons7.insert(person(QString("Deagol"), QStringList(QString("Deagol")), second7, QString("0000ff")));
	allPersons7.insert(person(QString("Frodo"), QStringList(QString("Frodo")), QStringList(QString("friend")), QString("0000ff")));
	allPersons7.insert(person(QString("Mery"), QStringList(QString("Mery")), QStringList(QString("brother")), QString("0000ff")));

	speakersList7.insert(person(QString("Smeagol"), QStringList(QString("Smeagol")), first7, QString("0000ff")));
	speakersList7.insert(person(QString("Frodo"), QStringList(QString("Frodo")), QStringList(QString("friend")), QString("0000ff")));

	QTest::newRow("CharactersListPartialTruncationBySubjQualifiers")<< 5 << 1 << context7 << allPersons7 << speakersList7;

	/////////////////////////////////////���� 10
	/////////////////////////////////////TwoCharactersHasSimilarSubjQualifiers

	StanfordWord* arr8 = new StanfordWord [7]; //������ - �����������

	arr8[0] = StanfordWord(1,1, QString("Give"),QString("give"),VB, WORD, QVector<corefCoord>());
	arr8[1] = StanfordWord(1,2, QString("us"),QString("we"),PRP, WORD, QVector<corefCoord>());
	arr8[2] = StanfordWord(1,3, QString("that"),QString("that"),IN, WORD, QVector<corefCoord>());
	arr8[3] = StanfordWord(1,4, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr8[4] = StanfordWord(1,5, QString("best"),QString("best"),JJS, WORD, QVector<corefCoord>());
	arr8[5] = StanfordWord(1,6, QString("friend"),QString("friend"),NN, WORD, QVector<corefCoord>());
	arr8[6] = StanfordWord(1,7, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp8; // ������ �� ����� (������������� ������ � ���������)
	tmp8.push_back(arr8+0);
	tmp8.push_back(arr8+1);
	tmp8.push_back(arr8+2);
	tmp8.push_back(arr8+3);
	tmp8.push_back(arr8+4);
	tmp8.push_back(arr8+5);
	tmp8.push_back(arr8+6);

	QList< textTree *> childList8;

	sentence context8(tmp8, childList8);

	QSet  <person> allPersons8, speakersList8;

	QStringList first8;
	first8.push_back(QString("old friend"));

	QStringList second8;

	QStringList therd8;
	therd8.push_back(QString("best friend"));

	allPersons8.insert(person(QString("Smeagol"), QStringList(QString("Smeagol")), first8, QString("0000ff")));
	allPersons8.insert(person(QString("Deagol"), QStringList(QString("Deagol")), second8, QString("0000ff")));
	allPersons8.insert(person(QString("Frodo"), QStringList(QString("Frodo")), therd8, QString("0000ff")));

	speakersList8.insert(person(QString("Frodo"), QStringList(QString("Frodo")), therd8, QString("0000ff")));

	QTest::newRow("TwoCharactersHasSimilarSubjQualifiers")<< 6 << 1 << context8 << allPersons8 << speakersList8;

	/////////////////////////////////////���� 11
	/////////////////////////////////////CrossingTheSpecifiersChoosingSmallerOne

	StanfordWord* arr9 = new StanfordWord [6]; //������ - �����������

	arr9[0] = StanfordWord(1,1, QString("Give"),QString("give"),VB, WORD, QVector<corefCoord>());
	arr9[1] = StanfordWord(1,2, QString("us"),QString("we"),PRP, WORD, QVector<corefCoord>());
	arr9[2] = StanfordWord(1,3, QString("that"),QString("that"),IN, WORD, QVector<corefCoord>());
	arr9[3] = StanfordWord(1,4, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr9[4] = StanfordWord(1,5, QString("friend"),QString("friend"),NN, WORD, QVector<corefCoord>());
	arr9[5] = StanfordWord(1,6, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp9; // ������ �� ����� (������������� ������ � ���������)
	tmp9.push_back(arr9+0);
	tmp9.push_back(arr9+1);
	tmp9.push_back(arr9+2);
	tmp9.push_back(arr9+3);
	tmp9.push_back(arr9+4);
	tmp9.push_back(arr9+5);

	QList< textTree *> childList9;

	sentence context9(tmp9, childList9);

	QSet  <person> allPersons9, speakersList9;

	QStringList first9;
	first9.push_back(QString("friend"));

	QStringList second9;

	QStringList therd9;
	therd9.push_back(QString("best friend"));

	allPersons9.insert(person(QString("Smeagol"), QStringList(QString("Smeagol")), first9, QString("0000ff")));
	allPersons9.insert(person(QString("Deagol"), QStringList(QString("Deagol")), second9, QString("0000ff")));
	allPersons9.insert(person(QString("Frodo"), QStringList(QString("Frodo")), therd9, QString("0000ff")));

	speakersList9.insert(person(QString("Smeagol"), QStringList(QString("Smeagol")), first9, QString("0000ff")));

	QTest::newRow("CrossingTheSpecifiersChoosingSmallerOne")<< 5 << 1 << context9 << allPersons9 << speakersList9;

	/////////////////////////////////////���� 12
	/////////////////////////////////////CrossingTheSpecifiersChoosingLargerOne

	StanfordWord* arr10 = new StanfordWord [7]; //������ - �����������

	arr10[0] = StanfordWord(1,1, QString("Give"),QString("give"),VB, WORD, QVector<corefCoord>());
	arr10[1] = StanfordWord(1,2, QString("us"),QString("we"),PRP, WORD, QVector<corefCoord>());
	arr10[2] = StanfordWord(1,3, QString("that"),QString("that"),IN, WORD, QVector<corefCoord>());
	arr10[3] = StanfordWord(1,4, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr10[4] = StanfordWord(1,5, QString("best"),QString("best"),JJS, WORD, QVector<corefCoord>());
	arr10[5] = StanfordWord(1,6, QString("friend"),QString("friend"),NN, WORD, QVector<corefCoord>());
	arr10[6] = StanfordWord(1,7, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp10; // ������ �� ����� (������������� ������ � ���������)
	tmp10.push_back(arr10+0);
	tmp10.push_back(arr10+1);
	tmp10.push_back(arr10+2);
	tmp10.push_back(arr10+3);
	tmp10.push_back(arr10+4);
	tmp10.push_back(arr10+5);
	tmp10.push_back(arr10+6);

	QList< textTree *> childList10;

	sentence context10(tmp10, childList10);

	QSet  <person> allPersons10, speakersList10;

	QStringList first10;
	first10.push_back(QString("friend"));

	QStringList second10;

	QStringList therd10;
	therd10.push_back(QString("best friend"));

	allPersons10.insert(person(QString("Smeagol"), QStringList(QString("Smeagol")), first10, QString("0000ff")));
	allPersons10.insert(person(QString("Deagol"), QStringList(QString("Deagol")), second10, QString("0000ff")));
	allPersons10.insert(person(QString("Frodo"), QStringList(QString("Frodo")), therd10, QString("0000ff")));

	speakersList10.insert(person(QString("Frodo"), QStringList(QString("Frodo")), therd10, QString("0000ff")));

	QTest::newRow("CrossingTheSpecifiersChoosingLargerOne")<< 6 << 1 << context10 << allPersons10 << speakersList10;
	
	/////////////////////////////////////���� 13
	/////////////////////////////////////wordIsIncludedInSpecifiers

	StanfordWord* arr11 = new StanfordWord [6]; //������ - �����������

	arr11[0] = StanfordWord(1,1, QString("Give"),QString("give"),VB, WORD, QVector<corefCoord>());
	arr11[1] = StanfordWord(1,2, QString("us"),QString("we"),PRP, WORD, QVector<corefCoord>());
	arr11[2] = StanfordWord(1,3, QString("that"),QString("that"),IN, WORD, QVector<corefCoord>());
	arr11[3] = StanfordWord(1,4, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr11[4] = StanfordWord(1,5, QString("friend"),QString("friend"),NN, WORD, QVector<corefCoord>());
	arr11[5] = StanfordWord(1,6, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp11; // ������ �� ����� (������������� ������ � ���������)
	tmp11.push_back(arr11+0);
	tmp11.push_back(arr11+1);
	tmp11.push_back(arr11+2);
	tmp11.push_back(arr11+3);
	tmp11.push_back(arr11+4);
	tmp11.push_back(arr11+5);

	QList< textTree *> childList11;

	sentence context11(tmp11, childList11);

	QSet  <person> allPersons11, speakersList11;

	QStringList first11;
	first11.push_back(QString("old friend"));

	QStringList second11;

	QStringList therd11;
	therd11.push_back(QString("best friend"));

	allPersons11.insert(person(QString("Smeagol"), QStringList(QString("Smeagol")), first11, QString("0000ff")));
	allPersons11.insert(person(QString("Deagol"), QStringList(QString("Deagol")), second11, QString("0000ff")));
	allPersons11.insert(person(QString("Frodo"), QStringList(QString("Frodo")), therd11, QString("0000ff")));

	speakersList11.insert(person(QString("Smeagol"), QStringList(QString("Smeagol")), first11, QString("0000ff")));
	speakersList11.insert(person(QString("Frodo"), QStringList(QString("Frodo")), therd11, QString("0000ff")));

	QTest::newRow("wordIsIncludedInSpecifiers")<< 5 << 1 << context11 << allPersons11 << speakersList11;
	
	/////////////////////////////////////���� 4
	/////////////////////////////////////complexQualifier

	StanfordWord* arr12 = new StanfordWord [7]; //������ - �����������

	arr12[0] = StanfordWord(1,1, QString("Give"),QString("give"),VB, WORD, QVector<corefCoord>());
	arr12[1] = StanfordWord(1,2, QString("us"),QString("we"),PRP, WORD, QVector<corefCoord>());
	arr12[2] = StanfordWord(1,3, QString("that"),QString("that"),IN, WORD, QVector<corefCoord>());
	arr12[3] = StanfordWord(1,4, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr12[4] = StanfordWord(1,5, QString("my"),QString("I"),PRPdollar, WORD, QVector<corefCoord>());
	arr12[5] = StanfordWord(1,6, QString("love"),QString("love"),NN, WORD, QVector<corefCoord>());
	arr12[6] = StanfordWord(1,7, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp12; // ������ �� ����� (������������� ������ � ���������)
	tmp12.push_back(arr12+0);
	tmp12.push_back(arr12+1);
	tmp12.push_back(arr12+2);
	tmp12.push_back(arr12+3);
	tmp12.push_back(arr12+4);
	tmp12.push_back(arr12+5);
	tmp12.push_back(arr12+6);

	QList< textTree *> childList12;

	sentence context12(tmp12, childList12);

	QSet  <person> allPersons12, speakersList12;

	QStringList first12;
	first12.push_back(QString("friend"));
	first12.push_back(QString("my love"));

	QStringList second12;

	allPersons12.insert(person(QString("Smeagol"), QStringList(QString("Smeagol")), first12, QString("0000ff")));
	allPersons12.insert(person(QString("Deagol"), QStringList(QString("Deagol")), second12, QString("0000ff")));

	speakersList12.insert(person(QString("Smeagol"), QStringList(QString("Smeagol")), first12, QString("0000ff")));

	QTest::newRow("complexQualifier")<< 6 << 1 << context12 << allPersons12 << speakersList12;
}

void TestClass::testSubstructSubjectBySubjQualifier()
{
	QFETCH(int, wordID);
	QFETCH(int, sentID);
	QFETCH(sentence, context);
	QFETCH(QSet <person>, allPersons);
	QFETCH(QSet <person>, speakersList);
	
	QSet <person> tmp;
	
	person::InitPer(allPersons);

	substructSubjectBySubjQualifier (wordID, sentID, context, tmp);

	QStringList Forgotten, superfluous;

	QString message = QString("\nResult checking failed\n\nForgotten: \n");

	bool isEqu = isEqual(tmp, speakersList, Forgotten, superfluous);

	if(!Forgotten.isEmpty() || !superfluous.isEmpty())
	{
		QMutableStringListIterator j(Forgotten);
		while(j.hasNext())
			message+= j.next() + QString("\n");

		message+=QString("\nSuperfluous: \n");

		QMutableStringListIterator k(superfluous);
		while(k.hasNext())
			message+= k.next() + QString("\n");
	}

	QVERIFY2(isEqu, message.toUtf8().constData());
	delete []context.words[0];
}

void TestClass::testsubstructSubjectByObjQualifier_data()
{
	QTest::addColumn<int>("mode");
	QTest::addColumn<int>("wordID");
	QTest::addColumn<int>("sentID");
	QTest::addColumn<sentence>("context");
	QTest::addColumn<QSet <person> >("allPersons");
	QTest::addColumn<QSet <person> >("speakersList");

	/////////////////////////////////////���� 1
	/////////////////////////////////////TwoCharactersOneObjQualifierRightFind

	StanfordWord* arr = new StanfordWord [3]; //������ - �����������

	arr[0] = StanfordWord(1,1, QString("said"),QString("say"),VBD, WORD, QVector<corefCoord>());
	arr[1] = StanfordWord(1,2, QString("hobbit"),QString("hobbit"),NN, WORD, QVector<corefCoord>());
	arr[2] = StanfordWord(1,3, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp; // ������ �� ����� (������������� ������ � ���������)
	tmp.push_back(arr+0);
	tmp.push_back(arr+1);
	tmp.push_back(arr+2);

	QList< textTree *> childList;

	sentence context(tmp, childList);

	QSet  <person> allPersons, speakersList;

	QStringList objQualif;
	objQualif.push_back(QString("Smeagol"));
	objQualif.push_back(QString("hobbit"));

	allPersons.insert(person(QString("Smeagol"), objQualif, QStringList(), QString("0000ff")));
	allPersons.insert(person(QString("Deagol"), QStringList(QString("Deagol")), QStringList(), QString("0000ff")));

	speakersList.insert(person(QString("Smeagol"), objQualif, QStringList(), QString("0000ff")));

	QTest::newRow("TwoCharactersOneObjQualifierRightFind")<< 0 << 2 << 1 << context << allPersons << speakersList;

	/////////////////////////////////////���� 2
	/////////////////////////////////////TwoCharactersOneObjQualifierLeftFind

	StanfordWord* arr1 = new StanfordWord [3]; //������ - �����������

	arr1[0] = StanfordWord(1,1, QString("hobbit"),QString("hobbit"),NN, WORD, QVector<corefCoord>());
	arr1[1] = StanfordWord(1,2, QString("said"),QString("say"),VBD, WORD, QVector<corefCoord>());
	arr1[2] = StanfordWord(1,3, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp1; // ������ �� ����� (������������� ������ � ���������)
	tmp1.push_back(arr1+0);
	tmp1.push_back(arr1+1);
	tmp1.push_back(arr1+2);

	QList< textTree *> childList1;

	sentence context1(tmp1, childList1);

	QSet  <person> allPersons1, speakersList1;

	QStringList objQualif1;
	objQualif1.push_back(QString("Smeagol"));
	objQualif1.push_back(QString("hobbit"));

	allPersons1.insert(person(QString("Smeagol"), objQualif1, QStringList(), QString("0000ff")));
	allPersons1.insert(person(QString("Deagol"), QStringList(QString("Deagol")), QStringList(), QString("0000ff")));

	speakersList1.insert(person(QString("Smeagol"), objQualif, QStringList(), QString("0000ff")));

	QTest::newRow("TwoCharactersOneObjQualifierLeftFind")<< 1 << 1 << 1 << context1 << allPersons1 << speakersList1;

	/////////////////////////////////////���� 3
	/////////////////////////////////////TwoCharactersHasOneObjQualifier

	StanfordWord* arr2 = new StanfordWord [3]; //������ - �����������

	arr2[0] = StanfordWord(1,1, QString("said"),QString("say"),VBD, WORD, QVector<corefCoord>());
	arr2[1] = StanfordWord(1,2, QString("hobbit"),QString("hobbit"),NN, WORD, QVector<corefCoord>());
	arr2[2] = StanfordWord(1,3, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp2; // ������ �� ����� (������������� ������ � ���������)
	tmp2.push_back(arr2+0);
	tmp2.push_back(arr2+1);
	tmp2.push_back(arr2+2);

	QList< textTree *> childList2;

	sentence context2(tmp2, childList2);

	QSet  <person> allPersons2, speakersList2;

	QStringList objQualif2FirstPer;
	objQualif2FirstPer.push_back(QString("Smeagol"));
	objQualif2FirstPer.push_back(QString("hobbit"));

	QStringList objQualif2SecondPer;
	objQualif2SecondPer.push_back(QString("Deagol"));
	objQualif2SecondPer.push_back(QString("hobbit"));

	allPersons2.insert(person(QString("Smeagol"), objQualif2FirstPer, QStringList(), QString("0000ff")));
	allPersons2.insert(person(QString("Deagol"), objQualif2SecondPer, QStringList(), QString("0000ff")));

	speakersList2.insert(person(QString("Smeagol"), objQualif2FirstPer, QStringList(), QString("0000ff")));
	speakersList2.insert(person(QString("Deagol"), objQualif2SecondPer, QStringList(), QString("0000ff")));

	QTest::newRow("TwoCharactersHasOneObjQualifier")<< 0 << 2 << 1 << context2 << allPersons2 << speakersList2;

	/////////////////////////////////////���� 4
	/////////////////////////////////////CharactersListPartialTruncationByObjQualifiers

	StanfordWord* arr3 = new StanfordWord [3]; //������ - �����������

	arr3[0] = StanfordWord(1,1, QString("said"),QString("say"),VBD, WORD, QVector<corefCoord>());
	arr3[1] = StanfordWord(1,2, QString("hobbit"),QString("hobbit"),NN, WORD, QVector<corefCoord>());
	arr3[2] = StanfordWord(1,3, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp3; // ������ �� ����� (������������� ������ � ���������)
	tmp3.push_back(arr3+0);
	tmp3.push_back(arr3+1);
	tmp3.push_back(arr3+2);

	QList< textTree *> childList3;

	sentence context3(tmp3, childList3);

	QSet  <person> allPersons3, speakersList3;

	QStringList objQualif3FirstPer;
	objQualif3FirstPer.push_back(QString("Smeagol"));
	objQualif3FirstPer.push_back(QString("hobbit"));

	QStringList objQualif3SecondPer;
	objQualif3SecondPer.push_back(QString("Gandalf"));
	objQualif3SecondPer.push_back(QString("wizard"));

	QStringList objQualif3ThirdPer;
	objQualif3ThirdPer.push_back(QString("Deagol"));
	objQualif3ThirdPer.push_back(QString("hobbit"));

	QStringList objQualif3FourthPer;
	objQualif3FourthPer.push_back(QString("Legolas"));
	objQualif3FourthPer.push_back(QString("archer"));

	allPersons3.insert(person(QString("Smeagol"), objQualif3FirstPer, QStringList(), QString("0000ff")));
	allPersons3.insert(person(QString("Gandalf"), objQualif3SecondPer, QStringList(), QString("0000ff")));
	allPersons3.insert(person(QString("Deagol"), objQualif3ThirdPer, QStringList(), QString("0000ff")));
	allPersons3.insert(person(QString("Legolas"), objQualif3FourthPer, QStringList(), QString("0000ff")));

	speakersList3.insert(person(QString("Smeagol"), objQualif3FirstPer, QStringList(), QString("0000ff")));
	speakersList3.insert(person(QString("Deagol"), objQualif3ThirdPer, QStringList(), QString("0000ff")));

	QTest::newRow("CharactersListPartialTruncationByObjQualifiers")<< 0 << 2 << 1 << context3 << allPersons3 << speakersList3;

	/////////////////////////////////////���� 5
	/////////////////////////////////////CharactersHasComplexObjQualifier

	StanfordWord* arr4 = new StanfordWord [4]; //������ - �����������

	arr4[0] = StanfordWord(1,1, QString("said"),QString("say"),VBD, WORD, QVector<corefCoord>());
	arr4[1] = StanfordWord(1,2, QString("old"),QString("old"),JJ, WORD, QVector<corefCoord>());
	arr4[2] = StanfordWord(1,3, QString("hobbit"),QString("hobbit"),NN, WORD, QVector<corefCoord>());
	arr4[3] = StanfordWord(1,4, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp4; // ������ �� ����� (������������� ������ � ���������)
	tmp4.push_back(arr4+0);
	tmp4.push_back(arr4+1);
	tmp4.push_back(arr4+2);
	tmp4.push_back(arr4+3);

	QList< textTree *> childList4;

	sentence context4(tmp4, childList4);

	QSet  <person> allPersons4, speakersList4;

	QStringList objQualif4FirstPer;
	objQualif4FirstPer.push_back(QString("Smeagol"));
	objQualif4FirstPer.push_back(QString("old hobbit"));

	QStringList objQualif4SecondPer;
	objQualif4SecondPer.push_back(QString("Deagol"));
	objQualif4SecondPer.push_back(QString("hobbit"));

	allPersons4.insert(person(QString("Smeagol"), objQualif4FirstPer, QStringList(), QString("0000ff")));
	allPersons4.insert(person(QString("Deagol"), objQualif4SecondPer, QStringList(), QString("0000ff")));

	speakersList4.insert(person(QString("Smeagol"), objQualif4FirstPer, QStringList(), QString("0000ff")));

	QTest::newRow("CharactersHasComplexObjQualifier")<< 0 << 2 << 1 << context4 << allPersons4 << speakersList4;
	
	/////////////////////////////////////���� 6
	/////////////////////////////////////qualifierHasAdjective

	StanfordWord* arr5 = new StanfordWord [4]; //������ - �����������

	arr5[0] = StanfordWord(1,1, QString("said"),QString("say"),VBD, WORD, QVector<corefCoord>());
	arr5[1] = StanfordWord(1,2, QString("old"),QString("old"),JJ, WORD, QVector<corefCoord>());
	arr5[2] = StanfordWord(1,3, QString("hobbit"),QString("hobbit"),NN, WORD, QVector<corefCoord>());
	arr5[3] = StanfordWord(1,4, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp5; // ������ �� ����� (������������� ������ � ���������)
	tmp5.push_back(arr5+0);
	tmp5.push_back(arr5+1);
	tmp5.push_back(arr5+2);
	tmp5.push_back(arr5+3);

	QList< textTree *> childList5;

	sentence context5(tmp5, childList5);

	QSet  <person> allPersons5, speakersList5;

	QStringList objQualif5FirstPer;
	objQualif5FirstPer.push_back(QString("Smeagol"));
	objQualif5FirstPer.push_back(QString("hobbit"));

	QStringList objQualif5SecondPer;
	objQualif5SecondPer.push_back(QString("Deagol"));
	objQualif5SecondPer.push_back(QString("hobbit"));

	allPersons5.insert(person(QString("Smeagol"), objQualif5FirstPer, QStringList(), QString("0000ff")));
	allPersons5.insert(person(QString("Deagol"), objQualif5SecondPer, QStringList(), QString("0000ff")));

	speakersList5.insert(person(QString("Smeagol"), objQualif5FirstPer, QStringList(), QString("0000ff")));
	speakersList5.insert(person(QString("Deagol"), objQualif5SecondPer, QStringList(), QString("0000ff")));

	QTest::newRow("qualifierHasAdjective")<< 0 << 2 << 1 << context5 << allPersons5 << speakersList5;

	/////////////////////////////////////���� 7
	/////////////////////////////////////TwoCharactersHasNotThisObjQualifier

	StanfordWord* arr6 = new StanfordWord [3]; //������ - �����������

	arr6[0] = StanfordWord(1,1, QString("said"),QString("say"),VBD, WORD, QVector<corefCoord>());
	arr6[1] = StanfordWord(1,2, QString("monster"),QString("monster"),NN, WORD, QVector<corefCoord>());
	arr6[2] = StanfordWord(1,3, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp6; // ������ �� ����� (������������� ������ � ���������)
	tmp6.push_back(arr6+0);
	tmp6.push_back(arr6+1);
	tmp6.push_back(arr6+2);

	QList< textTree *> childList6;

	sentence context6(tmp6, childList6);

	QSet  <person> allPersons6, speakersList6;

	QStringList objQualif6FirstPer;
	objQualif6FirstPer.push_back(QString("Smeagol"));
	objQualif6FirstPer.push_back(QString("hobbit"));

	QStringList objQualif6SecondPer;
	objQualif6SecondPer.push_back(QString("Deagol"));
	objQualif6SecondPer.push_back(QString("hobbit"));

	allPersons6.insert(person(QString("Smeagol"), objQualif6FirstPer, QStringList(), QString("0000ff")));
	allPersons6.insert(person(QString("Deagol"), objQualif6SecondPer, QStringList(), QString("0000ff")));

	QTest::newRow("TwoCharactersHasNotThisObjQualifier")<< 0 << 2 << 1 << context6 << allPersons6 << speakersList6;

	/////////////////////////////////////���� 8
	/////////////////////////////////////NameAsObjQualifier

	StanfordWord* arr7 = new StanfordWord [3]; //������ - �����������

	arr7[0] = StanfordWord(1,1, QString("said"),QString("say"),VBD, WORD, QVector<corefCoord>());
	arr7[1] = StanfordWord(1,2, QString("Smeagol"),QString("Smeagol"),NNP, WORD, QVector<corefCoord>());
	arr7[2] = StanfordWord(1,3, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp7; // ������ �� ����� (������������� ������ � ���������)
	tmp7.push_back(arr7+0);
	tmp7.push_back(arr7+1);
	tmp7.push_back(arr7+2);
	
	QList< textTree *> childList7;

	sentence context7(tmp7, childList7);

	QSet  <person> allPersons7, speakersList7;

	QStringList objQualif7FirstPer;
	objQualif7FirstPer.push_back(QString("Smeagol"));
	objQualif7FirstPer.push_back(QString("hobbit"));

	QStringList objQualif7SecondPer;
	objQualif7SecondPer.push_back(QString("Deagol"));
	objQualif7SecondPer.push_back(QString("hobbit"));

	allPersons7.insert(person(QString("Smeagol"), objQualif7FirstPer, QStringList(), QString("0000ff")));
	allPersons7.insert(person(QString("Deagol"), objQualif7SecondPer, QStringList(), QString("0000ff")));

	speakersList7.insert(person(QString("Smeagol"), objQualif7FirstPer, QStringList(), QString("0000ff")));

	QTest::newRow("NameAsObjQualifier")<< 0 << 2 << 1 << context7 << allPersons7 << speakersList7;
}

void TestClass::testsubstructSubjectByObjQualifier()
{
	QFETCH(int, mode);
	QFETCH(int, wordID);
	QFETCH(int, sentID);
	QFETCH(sentence, context);
	QFETCH(QSet <person>, allPersons);
	QFETCH(QSet <person>, speakersList);
	
	QSet <person> tmp;
	
	person::InitPer(allPersons);

	substructSubjectByObjQualifier (mode, wordID, sentID, context, tmp);

	QString message = QString("\nResult checking failed\n\nForgotten: \n");

	QStringList Forgotten, superfluous;

	bool isEqu = isEqual(tmp, speakersList, Forgotten, superfluous);

	if(!Forgotten.isEmpty() || !superfluous.isEmpty())
	{
		QMutableStringListIterator j(Forgotten);
		while(j.hasNext())
			message+= j.next() + QString("\n");

		message+=QString("\nSuperfluous: \n");

		QMutableStringListIterator k(superfluous);
		while(k.hasNext())
			message+= k.next() + QString("\n");
	}

	QVERIFY2(isEqu, message.toUtf8().constData());
		

	delete [] context.words[0];
}

void TestClass::testsubstructSubject_data()
{
	QTest::addColumn<sentence>("context");
	QTest::addColumn<QSet <person> >("allPersons");
	QTest::addColumn<QSet <person> >("speakersList");

	/////////////////////////////////////���� 1
	/////////////////////////////////////TwoCharactersOneObjQualifier

	StanfordWord* arr = new StanfordWord [3]; //������ - �����������

	arr[0] = StanfordWord(1,1, QString("said"),QString("say"),VBD, WORD, QVector<corefCoord>());
	arr[1] = StanfordWord(1,2, QString("hobbit"),QString("hobbit"),NN, WORD, QVector<corefCoord>());
	arr[2] = StanfordWord(1,3, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp; // ������ �� ����� (������������� ������ � ���������)
	tmp.push_back(arr+0);
	tmp.push_back(arr+1);
	tmp.push_back(arr+2);

	QList< textTree *> childList;

	sentence context(tmp, childList);

	QSet  <person> allPersons, speakersList;

	QStringList objQualif;
	objQualif.push_back(QString("Smeagol"));
	objQualif.push_back(QString("hobbit"));

	allPersons.insert(person(QString("Smeagol"), objQualif, QStringList(), QString("0000ff")));
	allPersons.insert(person(QString("Deagol"), QStringList(QString("Deagol")), QStringList(), QString("0000ff")));

	speakersList.insert(person(QString("Smeagol"), objQualif, QStringList(), QString("0000ff")));

	QTest::newRow("TwoCharactersOneObjQualifier") << context << allPersons << speakersList;

	/////////////////////////////////////���� 2
	/////////////////////////////////////TwoCharactersHasOneObjQualifier

	StanfordWord* arr1 = new StanfordWord [3]; //������ - �����������

	arr1[0] = StanfordWord(1,1, QString("ask"),QString("ask"),VBD, WORD, QVector<corefCoord>());
	arr1[1] = StanfordWord(1,2, QString("hobbit"),QString("hobbit"),NN, WORD, QVector<corefCoord>());
	arr1[2] = StanfordWord(1,3, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp1; // ������ �� ����� (������������� ������ � ���������)
	tmp1.push_back(arr1+0);
	tmp1.push_back(arr1+1);
	tmp1.push_back(arr1+2);

	QList< textTree *> childList1;

	sentence context1(tmp1, childList1);

	QSet  <person> allPersons1, speakersList1;

	QStringList FirstobjQualif1;
	FirstobjQualif1.push_back(QString("Smeagol"));
	FirstobjQualif1.push_back(QString("hobbit"));

	QStringList SecondobjQualif1;
	SecondobjQualif1.push_back(QString("Deagol"));
	SecondobjQualif1.push_back(QString("hobbit"));

	allPersons1.insert(person(QString("Smeagol"), FirstobjQualif1, QStringList(), QString("0000ff")));
	allPersons1.insert(person(QString("Deagol"), SecondobjQualif1, QStringList(), QString("0000ff")));

	speakersList1.insert(person(QString("Smeagol"), FirstobjQualif1, QStringList(), QString("0000ff")));
	speakersList1.insert(person(QString("Deagol"), SecondobjQualif1, QStringList(), QString("0000ff")));

	QTest::newRow("TwoCharactersOneObjQualifier") << context1 << allPersons1 << speakersList1;

	/////////////////////////////////////���� 3
	/////////////////////////////////////NoKeyWords

	int wCount = 11;
	StanfordWord* arr2 = new StanfordWord [wCount]; //������ - �����������

	arr2[0] = StanfordWord(1,1, QString("A"),QString("a"),DT, WORD, QVector<corefCoord>());
	arr2[1] = StanfordWord(1,2, QString("heavy"),QString("heavy"),JJ, WORD, QVector<corefCoord>());
	arr2[2] = StanfordWord(1,3, QString("silence"),QString("silence"),NN, WORD, QVector<corefCoord>());
	arr2[3] = StanfordWord(1,4, QString("fell"),QString("fall"),VBD, WORD, QVector<corefCoord>());
	arr2[4] = StanfordWord(1,5, QString("in"),QString("in"),IN, WORD, QVector<corefCoord>());
	arr2[5] = StanfordWord(1,6, QString("the"),QString("the"),DT, WORD, QVector<corefCoord>());
	arr2[6] = StanfordWord(1,7, QString("room"),QString("room"),NN, WORD, QVector<corefCoord>());
	arr2[7] = StanfordWord(1,8, QString("when"),QString("when"),WRB, WORD, QVector<corefCoord>());
	arr2[8] = StanfordWord(1,9, QString("hobbit"),QString("hobbit"),NN, WORD, QVector<corefCoord>());
	arr2[9] = StanfordWord(1,10, QString("sat"),QString("sit"),VBD, WORD, QVector<corefCoord>());
	arr2[10] = StanfordWord(1,11, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp2; // ������ �� ����� (������������� ������ � ���������)
	for(int i = 0; i < wCount; i++)
		tmp2.push_back(arr2+i);
	

	QList< textTree *> childList2;

	sentence context2(tmp2, childList2);

	QSet  <person> allPersons2, speakersList2;

	QStringList FirstobjQualif2;
	FirstobjQualif2.push_back(QString("Smeagol"));
	FirstobjQualif2.push_back(QString("hobbit"));

	QStringList SecondobjQualif2;
	SecondobjQualif2.push_back(QString("Deagol"));
	SecondobjQualif2.push_back(QString("hobbit"));
	
	QTest::newRow("NoKeyWords") << context2 << allPersons2 << speakersList2;

	/////////////////////////////////////���� 4
	/////////////////////////////////////wordsAfterObjQualifier

	wCount = 6;
	StanfordWord* arr3 = new StanfordWord [wCount]; //������ - �����������

	arr3[0] = StanfordWord(1,1, QString("asked"),QString("ask"),VB, WORD, QVector<corefCoord>());
	arr3[1] = StanfordWord(1,2, QString("hobbit"),QString("hobbit"),NN, WORD, QVector<corefCoord>());
	arr3[2] = StanfordWord(1,3, QString("and"),QString("and"),CC, WORD, QVector<corefCoord>());
	arr3[3] = StanfordWord(1,4, QString("came"),QString("come"),VBD, WORD, QVector<corefCoord>());
	arr3[4] = StanfordWord(1,5, QString("out"),QString("out"),RP, WORD, QVector<corefCoord>());
	arr3[5] = StanfordWord(1,6, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp3; // ������ �� ����� (������������� ������ � ���������)
	for(int i = 0; i < wCount; i++)
		tmp3.push_back(arr3+i);

	QList< textTree *> childList3;

	sentence context3(tmp3, childList3);

	QSet  <person> allPersons3, speakersList3;

	QStringList FirstobjQualif3;
	FirstobjQualif3.push_back(QString("Smeagol"));
	FirstobjQualif3.push_back(QString("hobbit"));

	QStringList SecondobjQualif3;
	SecondobjQualif3.push_back(QString("Deagol"));

	allPersons3.insert(person(QString("Smeagol"), FirstobjQualif3, QStringList(), QString("0000ff")));
	allPersons3.insert(person(QString("Deagol"), SecondobjQualif3, QStringList(), QString("0000ff")));

	speakersList3.insert(person(QString("Smeagol"), FirstobjQualif3, QStringList(), QString("0000ff")));
	
	QTest::newRow("wordsAfterObjQualifier") << context3 << allPersons3 << speakersList3;

	/////////////////////////////////////���� 5
	/////////////////////////////////////subjectToTheLeftOfTheKeyword

	wCount = 3;
	StanfordWord* arr4 = new StanfordWord [wCount]; //������ - �����������
	
	arr4[0] = StanfordWord(1,1, QString("hobbit"),QString("hobbit"),NN, WORD, QVector<corefCoord>());
	arr4[1] = StanfordWord(1,2, QString("said"),QString("say"),VBD, WORD, QVector<corefCoord>());
	arr4[2] = StanfordWord(1,3, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp4; // ������ �� ����� (������������� ������ � ���������)
	for(int i = 0; i < wCount; i++)
		tmp4.push_back(arr4+i);

	QList< textTree *> childList4;

	sentence context4(tmp4, childList4);

	QSet  <person> allPersons4, speakersList4;

	QStringList FirstobjQualif4;
	FirstobjQualif4.push_back(QString("Smeagol"));
	FirstobjQualif4.push_back(QString("hobbit"));

	QStringList SecondobjQualif4;
	SecondobjQualif4.push_back(QString("Deagol"));

	allPersons4.insert(person(QString("Smeagol"), FirstobjQualif4, QStringList(), QString("0000ff")));
	allPersons4.insert(person(QString("Deagol"), SecondobjQualif4, QStringList(), QString("0000ff")));

	speakersList4.insert(person(QString("Smeagol"), FirstobjQualif4, QStringList(), QString("0000ff")));
	
	QTest::newRow("subjectToTheLeftOfTheKeyword") << context4 << allPersons4 << speakersList4;

	/////////////////////////////////////���� 6
	/////////////////////////////////////ObjectiveQualifierOnBothSidesOfTheKeyword

	wCount = 4;
	StanfordWord* arr5 = new StanfordWord [wCount]; //������ - �����������
	
	arr5[0] = StanfordWord(1,1, QString("hobbit"),QString("hobbit"),NN, WORD, QVector<corefCoord>());
	arr5[1] = StanfordWord(1,2, QString("told"),QString("tell"),VBD, WORD, QVector<corefCoord>());
	arr5[2] = StanfordWord(1,3, QString("me"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr5[3] = StanfordWord(1,4, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp5; // ������ �� ����� (������������� ������ � ���������)
	for(int i = 0; i < wCount; i++)
		tmp5.push_back(arr5+i);

	QList< textTree *> childList5;

	sentence context5(tmp5, childList5);

	QSet  <person> allPersons5, speakersList5;

	QStringList FirstobjQualif5;
	FirstobjQualif5.push_back(QString("Smeagol"));
	FirstobjQualif5.push_back(QString("hobbit"));

	QStringList SecondobjQualif5;
	SecondobjQualif5.push_back(QString("Deagol"));
	SecondobjQualif5.push_back(QString("me"));

	allPersons5.insert(person(QString("Smeagol"), FirstobjQualif5, QStringList(), QString("0000ff")));
	allPersons5.insert(person(QString("Deagol"), SecondobjQualif5, QStringList(), QString("0000ff")));

	speakersList5.insert(person(QString("Smeagol"), FirstobjQualif5, QStringList(), QString("0000ff")));
	
	QTest::newRow("ObjectiveQualifierOnBothSidesOfTheKeyword") << context5 << allPersons5 << speakersList5;

	/////////////////////////////////////���� 7
	/////////////////////////////////////NameToldToName

	wCount = 5;
	StanfordWord* arr6 = new StanfordWord [wCount]; //������ - �����������
	
	arr6[0] = StanfordWord(1,1, QString("Smeagol"),QString("Smeagol"),NNP, WORD, QVector<corefCoord>());
	arr6[1] = StanfordWord(1,2, QString("told"),QString("tell"),VBD, WORD, QVector<corefCoord>());
	arr6[2] = StanfordWord(1,3, QString("to"),QString("to"),TO, WORD, QVector<corefCoord>());
	arr6[3] = StanfordWord(1,4, QString("Deagol"),QString("Deagol"),NNP, WORD, QVector<corefCoord>());
	arr6[4] = StanfordWord(1,5, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp6; // ������ �� ����� (������������� ������ � ���������)
	for(int i = 0; i < wCount; i++)
		tmp6.push_back(arr6+i);

	QList< textTree *> childList6;

	sentence context6(tmp6, childList6);

	QSet  <person> allPersons6, speakersList6;

	QStringList FirstobjQualif6;
	FirstobjQualif6.push_back(QString("Smeagol"));
	FirstobjQualif6.push_back(QString("hobbit"));

	QStringList SecondobjQualif6;
	SecondobjQualif6.push_back(QString("Deagol"));

	allPersons6.insert(person(QString("Smeagol"), FirstobjQualif6, QStringList(), QString("0000ff")));
	allPersons6.insert(person(QString("Deagol"), SecondobjQualif6, QStringList(), QString("0000ff")));

	speakersList6.insert(person(QString("Smeagol"), FirstobjQualif6, QStringList(), QString("0000ff")));
	
	QTest::newRow("ObjectiveQualifierOnBothSidesOfTheKeyword") << context6 << allPersons6 << speakersList6;
}

void TestClass::testsubstructSubject()
{
	QFETCH(sentence, context);
	QFETCH(QSet <person>, allPersons);
	QFETCH(QSet <person>, speakersList);

	QSet <person> tmp;
	
	person::InitPer(allPersons);

	QStringList a;
	a.push_back(QString("say"));
	a.push_back(QString("tell"));
	a.push_back(QString("ask"));

	sentence::InitKeys(a);
	
	substructSubject(context, tmp);

	QString message = QString("\nResult checking failed\n\nForgotten: \n");

	QStringList Forgotten, superfluous;

	bool isEqu = isEqual(tmp, speakersList, Forgotten, superfluous);

	if(!Forgotten.isEmpty() || !superfluous.isEmpty())
	{
		QMutableStringListIterator j(Forgotten);
		while(j.hasNext())
			message+= j.next() + QString("\n");

		message+=QString("\nSuperfluous: \n");

		QMutableStringListIterator k(superfluous);
		while(k.hasNext())
			message+= k.next() + QString("\n");
	}

	QVERIFY2(isEqu, message.toUtf8().constData());

	delete [] context.words[0];
}

void TestClass::testexpansionCharacterZone_data()
{
	QTest::addColumn<textTree*>("input");
	QTest::addColumn<textTree*>("output");
	QTest::addColumn<StanfordWord*>("dell");

	/////////////////////////////////////���� 1
	/////////////////////////////////////FirstPartOfDirectSpeechDefinedSecondUndefined
	
	StanfordWord* arr1 = new StanfordWord [18]; //������ - �����������

	arr1[0]= StanfordWord(1,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>());
	arr1[1] = StanfordWord(1,1, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr1[2] = StanfordWord(1,2, QString("Yes"),QString("yes"),UH, WORD, QVector<corefCoord>());
	arr1[3] = StanfordWord(1,3, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr1[4] = StanfordWord(1,4, QString("to"),QString("to"),TO, WORD, QVector<corefCoord>());
	arr1[5] = StanfordWord(1,5, QString("Mordor"),QString("Mordor"),NNP, WORD, QVector<corefCoord>());
	arr1[6] = StanfordWord(1,6, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr1[7] = StanfordWord(1,7, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr1[8]= StanfordWord(1,7, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());

	arr1[9] = StanfordWord(1,8, QString("asked"),QString("ask"),VBD, WORD, QVector<corefCoord>());
	arr1[10] = StanfordWord(1,9, QString("Gandalf"),QString("Gandalf"),NNP, WORD, QVector<corefCoord>());
	arr1[11] = StanfordWord(1,10, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	
	arr1[12]= StanfordWord(2,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());

	arr1[13] = StanfordWord(2,1, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr1[14] = StanfordWord(2,2, QString("There"),QString("there"),EX, WORD, QVector<corefCoord>());
	arr1[15] = StanfordWord(2,3, QString("!"),QString("!"),SentFinPunct, WORD, QVector<corefCoord>());
	arr1[16] = StanfordWord(2,4, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr1[17] = StanfordWord(2,4, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());
	
	QList<StanfordWord*> tmp1, res1; // ������ �� ����� (������������� ������ � ���������)
	for(int i = 0; i < 18; i++)
	{
		tmp1.push_back(arr1+i);
		res1.push_back(arr1+i);
	}
	
	res1.removeAt(12);
	res1.insert(12, new StanfordWord(2,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>()));

	QListIterator<StanfordWord*> i1(tmp1);
	textTree* text1 = new textTree(i1);

	QListIterator<StanfordWord*> j1(res1);
	textTree* result1 = new textTree(j1);
	delete res1[12];
	QTest::newRow("FirstPartOfDirectSpeechDefinedSecondUndefined")<< text1 << result1 << arr1;
	
	/////////////////////////////////////���� 2
	/////////////////////////////////////FirstPartOfDirectSpeechUndefinedSecondDefined/////////////////////////////////////////////////////
	
	int wCount = 42;

	StanfordWord* arr2 = new StanfordWord[wCount];

	arr2[0]= StanfordWord(1,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr2[1]= StanfordWord(1,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr2[2]= StanfordWord(1,2, QString("But"),QString("but"),CC, WORD, QVector<corefCoord>());
	arr2[3]= StanfordWord(1,3, QString("this"),QString("this"),DT, WORD, QVector<corefCoord>());
	arr2[4]= StanfordWord(1,4, QString("is"),QString("be"),VBZ, WORD, QVector<corefCoord>());
	arr2[5]= StanfordWord(1,5, QString("terrible"),QString("terrible"),JJ, WORD, QVector<corefCoord>());
	arr2[6]= StanfordWord(1,6, QString("!"),QString("!"),SentFinPunct, WORD, QVector<corefCoord>());
	arr2[7]= StanfordWord(1,7, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr2[8]= StanfordWord(1,7, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());
	arr2[9]= StanfordWord(2,1, QString("cried"),QString("cry"),VBD, WORD, QVector<corefCoord>());
	arr2[10]= StanfordWord(2,2, QString("Frodo"),QString("Frodo"),NNP, WORD, QVector<corefCoord>());
	arr2[11]= StanfordWord(2,3, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());

	arr2[12]= StanfordWord(3,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>());
	arr2[13]= StanfordWord(3,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr2[14]= StanfordWord(3,2, QString("Far"),QString("far"),RB, WORD, QVector<corefCoord>());
	arr2[15]= StanfordWord(3,3, QString("worse"),QString("worse"),JJR, WORD, QVector<corefCoord>());
	arr2[16]= StanfordWord(3,4, QString("than"),QString("than"),IN, WORD, QVector<corefCoord>());
	arr2[17]= StanfordWord(3,5, QString("the"),QString("the"),DT, WORD, QVector<corefCoord>());
	arr2[18]= StanfordWord(3,6, QString("worst"),QString("worst"),JJS, WORD, QVector<corefCoord>());
	arr2[19]= StanfordWord(3,7, QString("that"),QString("that"),IN, WORD, QVector<corefCoord>());
	arr2[20]= StanfordWord(3,8, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr2[21]= StanfordWord(3,9, QString("imagined"),QString("imagine"),VBD, WORD, QVector<corefCoord>());
	arr2[22]= StanfordWord(3,10, QString("from"),QString("from"),IN, WORD, QVector<corefCoord>());
	arr2[23]= StanfordWord(3,11, QString("your"),QString("you"),PRPdollar, WORD, QVector<corefCoord>());
	arr2[24]= StanfordWord(3,12, QString("hints"),QString("hint"),NNS, WORD, QVector<corefCoord>());
	arr2[25]= StanfordWord(3,13, QString("and"),QString("and"),CC, WORD, QVector<corefCoord>());
	arr2[26]= StanfordWord(3,14, QString("warnings"),QString("warning"),NNS, WORD, QVector<corefCoord>());
	arr2[27]= StanfordWord(3,15, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr2[28]= StanfordWord(4,1, QString("Gandalf"),QString("Gandalf"),NNP, WORD, QVector<corefCoord>());
	arr2[29]= StanfordWord(4,2, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr2[30]= StanfordWord(4,3, QString("best"),QString("best"),JJS, WORD, QVector<corefCoord>());
	arr2[31]= StanfordWord(4,4, QString("of"),QString("of"),IN, WORD, QVector<corefCoord>());
	arr2[32]= StanfordWord(4,5, QString("friends"),QString("friend"),NNS, WORD, QVector<corefCoord>());
	arr2[33]= StanfordWord(4,6, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr2[34]= StanfordWord(4,7, QString("what"),QString("what"),WP, WORD, QVector<corefCoord>());
	arr2[35]= StanfordWord(4,8, QString("am"),QString("be"),VBP, WORD, QVector<corefCoord>());
	arr2[36]= StanfordWord(4,9, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr2[37]= StanfordWord(4,10, QString("to"),QString("to"),TO, WORD, QVector<corefCoord>());
	arr2[38]= StanfordWord(4,11, QString("do"),QString("do"),VB, WORD, QVector<corefCoord>());
	arr2[39]= StanfordWord(4,12, QString("?"),QString("?"),SentFinPunct, WORD, QVector<corefCoord>());
	arr2[40]= StanfordWord(4,13, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr2[41]= StanfordWord(4,13, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());

	QList<StanfordWord*> tmp2, res2;

	for(int i = 0; i < wCount; i++)
	{
		tmp2.push_back(arr2+i);
		res2.push_back(arr2+i);
	}

	StanfordWord* rec = new StanfordWord(1,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>());;
	res2.pop_front();
	res2.push_front(rec);
	QListIterator<StanfordWord*> i(tmp2);
	textTree* text2 = new textTree(i);

	QListIterator<StanfordWord*> j(res2);
	textTree* result2 = new textTree(j);

	delete rec;
	QTest::newRow("FirstPartOfDirectSpeechUndefinedSecondDefined") << text2 << result2 << arr2;

	/////////////////////////////////////���� 3
	/////////////////////////////////////UndefinedParagraph

	wCount = 55;

	StanfordWord* arr3 = new StanfordWord[wCount];

	arr3[0]= StanfordWord(1,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>());
	arr3[1]= StanfordWord(1,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr3[2]= StanfordWord(1,2, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr3[3]= StanfordWord(1,3, QString("am"),QString("be"),VBP, WORD, QVector<corefCoord>());
	arr3[4]= StanfordWord(1,4, QString("sorry"),QString("sorry"),JJ, WORD, QVector<corefCoord>());
	arr3[5]= StanfordWord(1,5, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr3[6]= StanfordWord(1,6, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr3[7]= StanfordWord(1,6, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());
	arr3[8]= StanfordWord(1,7, QString("told"),QString("tell"),VBD, WORD, QVector<corefCoord>());
	arr3[9]= StanfordWord(1,8, QString("Frodo"),QString("Frodo"),NNP, WORD, QVector<corefCoord>());
	arr3[10]= StanfordWord(1,9, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());

	arr3[11]= StanfordWord(2,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr3[12]= StanfordWord(2,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr3[13]= StanfordWord(2,2, QString("But"),QString("but"),CC, WORD, QVector<corefCoord>());
	arr3[14]= StanfordWord(2,3, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr3[15]= StanfordWord(2,4, QString("am"),QString("be"),VBP, WORD, QVector<corefCoord>());
	arr3[16]= StanfordWord(2,5, QString("frightened"),QString("frighten"),VBN, WORD, QVector<corefCoord>());
	arr3[17]= StanfordWord(2,6, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr3[18]= StanfordWord(2,7, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr3[19]= StanfordWord(2,7, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());
	arr3[20]= StanfordWord(2,7, QString("</p>"),QString("</p>"),CC, MARK, QVector<corefCoord>());
	arr3[21]= StanfordWord(3,1, QString("<p>"),QString("<p>"),CC, MARK, QVector<corefCoord>());
	arr3[22]= StanfordWord(3,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr3[23]= StanfordWord(3,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr3[24]= StanfordWord(3,2, QString("You"),QString("you"),PRP, WORD, QVector<corefCoord>());
	arr3[25]= StanfordWord(3,3, QString("have"),QString("have"),VBP, WORD, QVector<corefCoord>());
	arr3[26]= StanfordWord(3,4, QString("not"),QString("not"),RB, WORD, QVector<corefCoord>());
	arr3[27]= StanfordWord(3,5, QString("seen"),QString("see"),VBN, WORD, QVector<corefCoord>());
	arr3[28]= StanfordWord(3,6, QString("him"),QString("he"),PRP, WORD, QVector<corefCoord>());
	arr3[29]= StanfordWord(3,7, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr3[30]= StanfordWord(3,8, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr3[31]= StanfordWord(3,8, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());
	arr3[32]= StanfordWord(3,9, QString("Gandalf"),QString("Gandalf"),NNP, WORD, QVector<corefCoord>());	
	arr3[33]= StanfordWord(3,10, QString("broke"),QString("break"),VBD, WORD, QVector<corefCoord>());
	arr3[34]= StanfordWord(3,11, QString("in"),QString("in"),IN, WORD, QVector<corefCoord>());
	arr3[35]= StanfordWord(3,12, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr3[36]= StanfordWord(3,12, QString("</p>"),QString("</p>"),CC, MARK, QVector<corefCoord>());
	arr3[37]= StanfordWord(4,1, QString("<p>"),QString("<p>"),CC, MARK, QVector<corefCoord>());
	arr3[38]= StanfordWord(4,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>());
	arr3[39]= StanfordWord(4,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr3[40]= StanfordWord(4,2, QString("No"),QString("no"),DT, WORD, QVector<corefCoord>());
	arr3[41]= StanfordWord(4,3, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr3[42]= StanfordWord(4,4, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr3[43]= StanfordWord(4,4, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());
	arr3[44]= StanfordWord(4,5, QString("said"),QString("say"),VBD, WORD, QVector<corefCoord>());
	arr3[45]= StanfordWord(4,6, QString("Frodo"),QString("Frodo"),NNP, WORD, QVector<corefCoord>());
	arr3[46]= StanfordWord(4,7, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr3[47]= StanfordWord(5,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr3[48]= StanfordWord(5,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr3[49]= StanfordWord(5,2, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr3[50]= StanfordWord(5,3, QString("ca"),QString("can"),MD, WORD, QVector<corefCoord>());
	arr3[51]= StanfordWord(5,4, QString("n't"),QString("not"),RB, WORD, QVector<corefCoord>());
	arr3[52]= StanfordWord(5,5, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr3[53]= StanfordWord(5,6, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr3[54]= StanfordWord(5,6, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());

	QList<StanfordWord*> tmp3, res3;

	for(int i = 0; i < wCount; i++)
	{
		tmp3.push_back(arr3+i);
		res3.push_back(arr3+i);
	}

	res3.removeAt(11);
	res3.insert(11,new StanfordWord(2,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>()));

	res3.removeAt(47);
	res3.insert(47,new StanfordWord(5,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>()));

	QListIterator<StanfordWord*> i3(tmp3);
	textTree* text3 = new textTree(i3);

	QListIterator<StanfordWord*> j3(res3);
	textTree* result3 = new textTree(j3);

	delete res3[47];
	delete res3[11];
	QTest::newRow("UndefinedParagraph") << text3 << result3 << arr3;

	/////////////////////////////////////���� 4
	/////////////////////////////////////TwoParagraphInDirectSpeech

	wCount = 37;

	StanfordWord* arr4 = new StanfordWord[wCount];

	arr4[0]= StanfordWord(1,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>());
	arr4[1]= StanfordWord(1,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr4[2]= StanfordWord(1,2, QString("Ah"),QString("ah"),UH, WORD, QVector<corefCoord>());
	arr4[3]= StanfordWord(1,3, QString("!"),QString("!"),SentFinPunct, WORD, QVector<corefCoord>());
	arr4[4]= StanfordWord(1,4, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr4[5]= StanfordWord(1,4, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());

	arr4[6]= StanfordWord(2,1, QString("said"),QString("say"),VBD, WORD, QVector<corefCoord>());
	arr4[7]= StanfordWord(2,2, QString("Gandalf"),QString("Gandalf"),NNP, WORD, QVector<corefCoord>());
	arr4[8]= StanfordWord(2,3, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());

	arr4[9]= StanfordWord(3,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr4[10]= StanfordWord(3,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr4[11]= StanfordWord(3,2, QString("That"),QString("that"),DT, WORD, QVector<corefCoord>());
	arr4[12]= StanfordWord(3,3, QString("is"),QString("be"),VBZ, WORD, QVector<corefCoord>());
	arr4[13]= StanfordWord(3,4, QString("a"),QString("a"),DT, WORD, QVector<corefCoord>());
	arr4[14]= StanfordWord(3,5, QString("very"),QString("very"),RB, WORD, QVector<corefCoord>());
	arr4[15]= StanfordWord(3,6, QString("long"),QString("long"),JJ, WORD, QVector<corefCoord>());
	arr4[16]= StanfordWord(3,7, QString("story"),QString("story"),NN, WORD, QVector<corefCoord>());
	arr4[17]= StanfordWord(3,8, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr4[18]= StanfordWord(3,8, QString("</p>"),QString("</p>"),CC, MARK, QVector<corefCoord>());
	
	arr4[19]= StanfordWord(4,1, QString("<p>"),QString("<p>"),CC, MARK, QVector<corefCoord>());
	arr4[20]= StanfordWord(4,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr4[21]= StanfordWord(4,2, QString("But"),QString("but"),CC, WORD, QVector<corefCoord>());
	arr4[22]= StanfordWord(4,3, QString("last"),QString("last"),JJ, WORD, QVector<corefCoord>());
	arr4[23]= StanfordWord(4,4, QString("night"),QString("night"),NN, WORD, QVector<corefCoord>());
	arr4[24]= StanfordWord(4,5, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr4[25]= StanfordWord(4,6, QString("told"),QString("tell"),VBD, WORD, QVector<corefCoord>());
	arr4[26]= StanfordWord(4,7, QString("you"),QString("you"),PRP, WORD, QVector<corefCoord>());
	arr4[27]= StanfordWord(4,8, QString("of"),QString("of"),IN, WORD, QVector<corefCoord>());
	arr4[28]= StanfordWord(4,9, QString("Sauron"),QString("Sauron"),NNP, WORD, QVector<corefCoord>());
	arr4[29]= StanfordWord(4,10, QString("the"),QString("the"),DT, WORD, QVector<corefCoord>());
	arr4[30]= StanfordWord(4,11, QString("Great"),QString("Great"),NNP, WORD, QVector<corefCoord>());
	arr4[31]= StanfordWord(4,12, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr4[32]= StanfordWord(4,13, QString("the"),QString("the"),DT, WORD, QVector<corefCoord>());
	arr4[33]= StanfordWord(4,14, QString("Dark"),QString("Dark"),JJ, WORD, QVector<corefCoord>());
	arr4[34]= StanfordWord(4,15, QString("Lord"),QString("Lord"),NNP, WORD, QVector<corefCoord>());
	arr4[35]= StanfordWord(4,16, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr4[36]= StanfordWord(4,16, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());


	QList<StanfordWord*> tmp4, res4;

	for(int i = 0; i < wCount; i++)
	{
		tmp4.push_back(arr4+i);
		res4.push_back(arr4+i);
	}

	res4.removeAt(9);
	res4.insert(9 ,new StanfordWord(3,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>()));

	QListIterator<StanfordWord*> i4(tmp4);
	textTree* text4 = new textTree(i4);

	QListIterator<StanfordWord*> j4(res4);
	textTree* result4 = new textTree(j4);

	delete res4[9];
	QTest::newRow("TwoParagraphInDirectSpeech") << text4 << result4 << arr4;

	/////////////////////////////////////���� 5
	/////////////////////////////////////severalParagraphInDirectSpeech

	wCount = 46;

	StanfordWord* arr5 = new StanfordWord[wCount];

	arr5[0]= StanfordWord(1,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>());
	arr5[1]= StanfordWord(1,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr5[2]= StanfordWord(1,2, QString("So"),QString("so"),RB, WORD, QVector<corefCoord>());
	arr5[3]= StanfordWord(1,3, QString("do"),QString("do"),VBP, WORD, QVector<corefCoord>());
	arr5[4]= StanfordWord(1,4, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr5[5]= StanfordWord(1,5, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr5[6]= StanfordWord(1,6, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr5[7]= StanfordWord(1,6, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());
	arr5[8]= StanfordWord(1,7, QString("said"),QString("say"),VBD, WORD, QVector<corefCoord>());
	arr5[9]= StanfordWord(1,8, QString("Gandalf"),QString("Gandalf"),NNP, WORD, QVector<corefCoord>());
	arr5[10]= StanfordWord(1,9, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr5[11]= StanfordWord(1,10, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr5[12]= StanfordWord(1,11, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr5[13]= StanfordWord(1,12, QString("and"),QString("and"),CC, WORD, QVector<corefCoord>());
	arr5[14]= StanfordWord(1,13, QString("do"),QString("do"),VB, WORD, QVector<corefCoord>());
	arr5[15]= StanfordWord(1,14, QString("all"),QString("all"),DT, WORD, QVector<corefCoord>());
	arr5[16]= StanfordWord(1,15, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr5[17]= StanfordWord(1,16, QString("</p>"),QString("</p>"),CC, MARK, QVector<corefCoord>());

	arr5[18]= StanfordWord(2,1, QString("<p>"),QString("<p>"),CC, MARK, QVector<corefCoord>());
	arr5[19]= StanfordWord(2,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr5[20]= StanfordWord(2,2, QString("He"),QString("he"),PRP, WORD, QVector<corefCoord>());
	arr5[21]= StanfordWord(2,3, QString("lacks"),QString("lack"),VBZ, WORD, QVector<corefCoord>());
	arr5[22]= StanfordWord(2,4, QString("the"),QString("the"),DT, WORD, QVector<corefCoord>());
	arr5[23]= StanfordWord(2,5, QString("One"),QString("one"),CD, WORD, QVector<corefCoord>());
	arr5[24]= StanfordWord(2,6, QString("Ring"),QString("ring"),NN, WORD, QVector<corefCoord>());
	arr5[25]= StanfordWord(2,7, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr5[26]= StanfordWord(2,7, QString("</p>"),QString("</p>"),CC, MARK, QVector<corefCoord>());

	arr5[27]= StanfordWord(3,1, QString("<p>"),QString("<p>"),CC, MARK, QVector<corefCoord>());
	arr5[28]= StanfordWord(3,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr5[29]= StanfordWord(3,2, QString("The"),QString("the"),DT, WORD, QVector<corefCoord>());
	arr5[30]= StanfordWord(3,3, QString("Three"),QString("three"),CD, WORD, QVector<corefCoord>());
	arr5[31]= StanfordWord(3,4, QString("the"),QString("the"),DT, WORD, QVector<corefCoord>());
	arr5[32]= StanfordWord(3,5, QString("Elf-lords"),QString("Elf-lords"),NNPS, WORD, QVector<corefCoord>());
	arr5[33]= StanfordWord(3,6, QString("hid"),QString("hide"),VBD, WORD, QVector<corefCoord>());
	arr5[34]= StanfordWord(3,7, QString("from"),QString("from"),IN, WORD, QVector<corefCoord>());
	arr5[35]= StanfordWord(3,8, QString("him"),QString("he"),PRP, WORD, QVector<corefCoord>());
	arr5[36]= StanfordWord(3,9, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr5[37]= StanfordWord(3,9, QString("</p>"),QString("</p>"),CC, MARK, QVector<corefCoord>());
	arr5[38]= StanfordWord(4,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr5[39]= StanfordWord(4,2, QString("So"),QString("so"),IN, WORD, QVector<corefCoord>());
	arr5[40]= StanfordWord(4,3, QString("it"),QString("it"),PRP, WORD, QVector<corefCoord>());
	arr5[41]= StanfordWord(4,4, QString("is"),QString("be"),VBZ, WORD, QVector<corefCoord>());
	arr5[42]= StanfordWord(4,5, QString("now"),QString("now"),RB, WORD, QVector<corefCoord>());
	arr5[43]= StanfordWord(4,6, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr5[44]= StanfordWord(4,7, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr5[45]= StanfordWord(4,7, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());

	QList<StanfordWord*> tmp5, res5;

	for(int i = 0; i < wCount; i++)
	{
		tmp5.push_back(arr5+i);
		res5.push_back(arr5+i);
	}

	res5.removeAt(11);
	res5.insert(11 ,new StanfordWord(1,10, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>()));

	QListIterator<StanfordWord*> i5(tmp5);
	textTree* text5 = new textTree(i5);

	QListIterator<StanfordWord*> j5(res5);
	textTree* result5 = new textTree(j5);

	delete res5[11];
	QTest::newRow("severalParagraphInDirectSpeech") << text5 << result5 << arr5;
	
	/////////////////////////////////////���� 6
	/////////////////////////////////////SeveralUncertainAreasBothSides

	wCount = 48;

	StanfordWord* arr6 = new StanfordWord[wCount];

	arr6[0]= StanfordWord(1,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr6[1]= StanfordWord(1,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr6[2]= StanfordWord(1,2, QString("You"),QString("you"),PRP, WORD, QVector<corefCoord>());
	arr6[3]= StanfordWord(1,3, QString("will"),QString("will"),MD, WORD, QVector<corefCoord>());
	arr6[4]= StanfordWord(1,4, QString("live"),QString("live"),VB, WORD, QVector<corefCoord>());
	arr6[5]= StanfordWord(1,5, QString("here"),QString("here"),RB, WORD, QVector<corefCoord>());
	arr6[6]= StanfordWord(1,6, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr6[7]= StanfordWord(1,7, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr6[8]= StanfordWord(1,7, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());
	arr6[9]= StanfordWord(1,8, QString("he"),QString("he"),PRP, WORD, QVector<corefCoord>());
	arr6[10]= StanfordWord(1,9, QString("got"),QString("get"),VBD, WORD, QVector<corefCoord>());
	arr6[11]= StanfordWord(1,10, QString("up"),QString("up"),RP, WORD, QVector<corefCoord>());
	arr6[12]= StanfordWord(1,11, QString("from"),QString("from"),IN, WORD, QVector<corefCoord>());
	arr6[13]= StanfordWord(1,12, QString("the"),QString("the"),DT, WORD, QVector<corefCoord>());
	arr6[14]= StanfordWord(1,13, QString("sofa"),QString("sofa"),NN, WORD, QVector<corefCoord>());
	arr6[15]= StanfordWord(1,14, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr6[16]= StanfordWord(2,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>());
	arr6[17]= StanfordWord(2,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr6[18]= StanfordWord(2,2, QString("Someday"),QString("someday"),RB, WORD, QVector<corefCoord>());
	arr6[19]= StanfordWord(2,3, QString("all"),QString("all"),PDT, WORD, QVector<corefCoord>());
	arr6[20]= StanfordWord(2,4, QString("this"),QString("this"),DT, WORD, QVector<corefCoord>());
	arr6[21]= StanfordWord(2,5, QString("will"),QString("will"),MD, WORD, QVector<corefCoord>());
	arr6[22]= StanfordWord(2,6, QString("get"),QString("get"),VB, WORD, QVector<corefCoord>());
	arr6[23]= StanfordWord(2,7, QString("to"),QString("to"),TO, WORD, QVector<corefCoord>());
	arr6[24]= StanfordWord(2,8, QString("you"),QString("you"),PRP, WORD, QVector<corefCoord>());
	arr6[25]= StanfordWord(2,9, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr6[26]= StanfordWord(2,10, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr6[27]= StanfordWord(2,10, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());
	arr6[28]= StanfordWord(2,11, QString("Garrett"),QString("Garrett"),NNP, WORD, QVector<corefCoord>());
	arr6[29]= StanfordWord(2,12, QString("said"),QString("say"),VBD, WORD, QVector<corefCoord>());
	arr6[30]= StanfordWord(2,13, QString("loudly"),QString("loudly"),RB, WORD, QVector<corefCoord>());
	arr6[31]= StanfordWord(2,14, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr6[32]= StanfordWord(3,1, QString("He"),QString("he"),PRP, WORD, QVector<corefCoord>());
	arr6[33]= StanfordWord(3,2, QString("walked"),QString("walk"),VBD, WORD, QVector<corefCoord>());
	arr6[34]= StanfordWord(3,3, QString("around"),QString("around"),IN, WORD, QVector<corefCoord>());
	arr6[35]= StanfordWord(3,4, QString("the"),QString("the"),DT, WORD, QVector<corefCoord>());
	arr6[36]= StanfordWord(3,5, QString("room"),QString("room"),NN, WORD, QVector<corefCoord>());
	arr6[37]= StanfordWord(3,6, QString("and"),QString("and"),CC, WORD, QVector<corefCoord>());
	arr6[38]= StanfordWord(3,7, QString("added"),QString("add"),VBD, WORD, QVector<corefCoord>());
	arr6[39]= StanfordWord(3,8, QString(":"),QString(":"),Colon, WORD, QVector<corefCoord>());
	arr6[40]= StanfordWord(3,9, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr6[41]= StanfordWord(3,9, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr6[42]= StanfordWord(3,10, QString("It"),QString("it"),PRP, WORD, QVector<corefCoord>());
	arr6[43]= StanfordWord(3,11, QString("'s"),QString("be"),VBZ, WORD, QVector<corefCoord>());
	arr6[44]= StanfordWord(3,12, QString("all"),QString("all"),DT, WORD, QVector<corefCoord>());
	arr6[45]= StanfordWord(3,13, QString("!"),QString("!"),SentFinPunct, WORD, QVector<corefCoord>());
	arr6[46]= StanfordWord(3,14, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr6[47]= StanfordWord(3,14, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());

	QList<StanfordWord*> tmp6, res6;

	for(int i = 0; i < wCount; i++)
	{
		tmp6.push_back(arr6+i);
		res6.push_back(arr6+i);
	}

	res6.removeAt(0);
	res6.insert(0 ,new StanfordWord(1,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>()));

	res6.removeAt(40);
	res6.insert(40 ,new StanfordWord(3,9, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>()));

	QListIterator<StanfordWord*> i6(tmp6);
	textTree* text6 = new textTree(i6);

	QListIterator<StanfordWord*> j6(res6);
	textTree* result6 = new textTree(j6);

	delete res6[0];
	delete res6[40];
	QTest::newRow("SeveralUncertainAreasBothSides") << text6 << result6 << arr6;

	/////////////////////////////////////���� 7
	/////////////////////////////////////SeveralUncertainAreasLeftSide

	wCount = 46;

	StanfordWord* arr7 = new StanfordWord[wCount];

	arr7[0]= StanfordWord(1,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr7[1]= StanfordWord(1,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr7[2]= StanfordWord(1,2, QString("You"),QString("you"),PRP, WORD, QVector<corefCoord>());
	arr7[3]= StanfordWord(1,3, QString("will"),QString("will"),MD, WORD, QVector<corefCoord>());
	arr7[4]= StanfordWord(1,4, QString("live"),QString("live"),VB, WORD, QVector<corefCoord>());
	arr7[5]= StanfordWord(1,5, QString("here"),QString("here"),RB, WORD, QVector<corefCoord>());
	arr7[6]= StanfordWord(1,6, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr7[7]= StanfordWord(1,7, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr7[8]= StanfordWord(1,7, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());
	arr7[9]= StanfordWord(1,8, QString("he"),QString("he"),PRP, WORD, QVector<corefCoord>());
	arr7[10]= StanfordWord(1,9, QString("got"),QString("get"),VBD, WORD, QVector<corefCoord>());
	arr7[11]= StanfordWord(1,10, QString("up"),QString("up"),RP, WORD, QVector<corefCoord>());
	arr7[12]= StanfordWord(1,11, QString("from"),QString("from"),IN, WORD, QVector<corefCoord>());
	arr7[13]= StanfordWord(1,12, QString("the"),QString("the"),DT, WORD, QVector<corefCoord>());
	arr7[14]= StanfordWord(1,13, QString("sofa"),QString("sofa"),NN, WORD, QVector<corefCoord>());
	arr7[15]= StanfordWord(1,14, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	   
	arr7[16]= StanfordWord(2,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr7[17]= StanfordWord(2,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr7[18]= StanfordWord(2,2, QString("Someday"),QString("someday"),RB, WORD, QVector<corefCoord>());
	arr7[19]= StanfordWord(2,3, QString("all"),QString("all"),PDT, WORD, QVector<corefCoord>());
	arr7[20]= StanfordWord(2,4, QString("this"),QString("this"),DT, WORD, QVector<corefCoord>());
	arr7[21]= StanfordWord(2,5, QString("will"),QString("will"),MD, WORD, QVector<corefCoord>());
	arr7[22]= StanfordWord(2,6, QString("get"),QString("get"),VB, WORD, QVector<corefCoord>());
	arr7[23]= StanfordWord(2,7, QString("to"),QString("to"),TO, WORD, QVector<corefCoord>());
	arr7[24]= StanfordWord(2,8, QString("you"),QString("you"),PRP, WORD, QVector<corefCoord>());
	arr7[25]= StanfordWord(2,9, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr7[26]= StanfordWord(2,10, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr7[27]= StanfordWord(2,10, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());
	   
	arr7[28]= StanfordWord(2,11, QString("he"),QString("he"),PRP, WORD, QVector<corefCoord>());
	arr7[29]= StanfordWord(2,12, QString("walked"),QString("walk"),VBD, WORD, QVector<corefCoord>());
	arr7[30]= StanfordWord(2,13, QString("around"),QString("around"),IN, WORD, QVector<corefCoord>());
	arr7[31]= StanfordWord(2,14, QString("the"),QString("the"),DT, WORD, QVector<corefCoord>());
	arr7[32]= StanfordWord(2,15, QString("room"),QString("room"),NN, WORD, QVector<corefCoord>());
	arr7[33]= StanfordWord(2,16, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	   
	arr7[34]= StanfordWord(3,1, QString("Garrett"),QString("Garrett"),NNP, WORD, QVector<corefCoord>());
	arr7[35]= StanfordWord(3,2, QString("said"),QString("say"),VBD, WORD, QVector<corefCoord>());
	arr7[36]= StanfordWord(3,3, QString("loudly"),QString("loudly"),RB, WORD, QVector<corefCoord>());
	arr7[37]= StanfordWord(3,4, QString(":"),QString(":"),Colon, WORD, QVector<corefCoord>());
	arr7[38]= StanfordWord(3,5, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>());
	arr7[39]= StanfordWord(3,5, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr7[40]= StanfordWord(3,6, QString("It"),QString("it"),PRP, WORD, QVector<corefCoord>());
	arr7[41]= StanfordWord(3,7, QString("'s"),QString("be"),VBZ, WORD, QVector<corefCoord>());
	arr7[42]= StanfordWord(3,8, QString("all"),QString("all"),DT, WORD, QVector<corefCoord>());
	arr7[43]= StanfordWord(3,9, QString("!"),QString("!"),SentFinPunct, WORD, QVector<corefCoord>());
	arr7[44]= StanfordWord(3,10, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr7[45]= StanfordWord(3,10, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());

	QList<StanfordWord*> tmp7, res7;

	for(int i = 0; i < wCount; i++)
	{
		tmp7.push_back(arr7+i);
		res7.push_back(arr7+i);
	}

	res7.removeAt(0);
	res7.insert(0, new StanfordWord(1,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>()));

	res7.removeAt(16);
	res7.insert(16, new StanfordWord(2,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>()));

	QListIterator<StanfordWord*> i7(tmp7);
	textTree* text7 = new textTree(i7);

	QListIterator<StanfordWord*> j7(res7);
	textTree* result7 = new textTree(j7);

	delete res7[0];
	delete res7[16];
	QTest::newRow("SeveralUncertainAreasLeftSide") << text7 << result7 << arr7;

	/////////////////////////////////////���� 8
	/////////////////////////////////////SeveralUncertainAreasRightSide

	wCount = 48;

	StanfordWord* arr8 = new StanfordWord[wCount];

	arr8[0]= StanfordWord(1,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>());
	arr8[1]= StanfordWord(1,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr8[2]= StanfordWord(1,2, QString("You"),QString("you"),PRP, WORD, QVector<corefCoord>());
	arr8[3]= StanfordWord(1,3, QString("will"),QString("will"),MD, WORD, QVector<corefCoord>());
	arr8[4]= StanfordWord(1,4, QString("live"),QString("live"),VB, WORD, QVector<corefCoord>());
	arr8[5]= StanfordWord(1,5, QString("here"),QString("here"),RB, WORD, QVector<corefCoord>());
	arr8[6]= StanfordWord(1,6, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr8[7]= StanfordWord(1,7, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr8[8]= StanfordWord(1,7, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());

	arr8[9]= StanfordWord(1,8, QString("Garrett"),QString("Garrett"),NNP, WORD, QVector<corefCoord>());
	arr8[10]= StanfordWord(1,9, QString("said"),QString("say"),VBD, WORD, QVector<corefCoord>());
	arr8[11]= StanfordWord(1,10, QString("loudly"),QString("loudly"),RB, WORD, QVector<corefCoord>());
	arr8[12]= StanfordWord(1,11, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());

	arr8[13]= StanfordWord(2,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr8[14]= StanfordWord(2,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr8[15]= StanfordWord(2,2, QString("Someday"),QString("someday"),RB, WORD, QVector<corefCoord>());
	arr8[16]= StanfordWord(2,3, QString("all"),QString("all"),PDT, WORD, QVector<corefCoord>());
	arr8[17]= StanfordWord(2,4, QString("this"),QString("this"),DT, WORD, QVector<corefCoord>());
	arr8[18]= StanfordWord(2,5, QString("will"),QString("will"),MD, WORD, QVector<corefCoord>());
	arr8[19]= StanfordWord(2,6, QString("get"),QString("get"),VB, WORD, QVector<corefCoord>());
	arr8[20]= StanfordWord(2,7, QString("to"),QString("to"),TO, WORD, QVector<corefCoord>());
	arr8[21]= StanfordWord(2,8, QString("you"),QString("you"),PRP, WORD, QVector<corefCoord>());
	arr8[22]= StanfordWord(2,9, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr8[23]= StanfordWord(2,10, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr8[24]= StanfordWord(2,10, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());

	arr8[25]= StanfordWord(2,11, QString("he"),QString("he"),PRP, WORD, QVector<corefCoord>());
	arr8[26]= StanfordWord(2,12, QString("got"),QString("get"),VBD, WORD, QVector<corefCoord>());
	arr8[27]= StanfordWord(2,13, QString("up"),QString("up"),RP, WORD, QVector<corefCoord>());
	arr8[28]= StanfordWord(2,14, QString("from"),QString("from"),IN, WORD, QVector<corefCoord>());
	arr8[29]= StanfordWord(2,15, QString("the"),QString("the"),DT, WORD, QVector<corefCoord>());
	arr8[30]= StanfordWord(2,16, QString("sofa"),QString("sofa"),NN, WORD, QVector<corefCoord>());
	arr8[31]= StanfordWord(2,17, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());

	arr8[32]= StanfordWord(3,1, QString("He"),QString("he"),PRP, WORD, QVector<corefCoord>());
	arr8[33]= StanfordWord(3,2, QString("walked"),QString("walk"),VBD, WORD, QVector<corefCoord>());
	arr8[34]= StanfordWord(3,3, QString("around"),QString("around"),IN, WORD, QVector<corefCoord>());
	arr8[35]= StanfordWord(3,4, QString("the"),QString("the"),DT, WORD, QVector<corefCoord>());
	arr8[36]= StanfordWord(3,5, QString("room"),QString("room"),NN, WORD, QVector<corefCoord>());
	arr8[37]= StanfordWord(3,6, QString("and"),QString("and"),CC, WORD, QVector<corefCoord>());
	arr8[38]= StanfordWord(3,7, QString("added"),QString("add"),VBD, WORD, QVector<corefCoord>());
	arr8[39]= StanfordWord(3,8, QString(":"),QString(":"),Colon, WORD, QVector<corefCoord>());
	arr8[40]= StanfordWord(3,9, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr8[41]= StanfordWord(3,9, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr8[42]= StanfordWord(3,10, QString("It"),QString("it"),PRP, WORD, QVector<corefCoord>());
	arr8[43]= StanfordWord(3,11, QString("'s"),QString("be"),VBZ, WORD, QVector<corefCoord>());
	arr8[44]= StanfordWord(3,12, QString("all"),QString("all"),DT, WORD, QVector<corefCoord>());
	arr8[45]= StanfordWord(3,13, QString("!"),QString("!"),SentFinPunct, WORD, QVector<corefCoord>());
	arr8[46]= StanfordWord(3,14, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr8[47]= StanfordWord(3,14, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());

	QList<StanfordWord*> tmp8, res8;

	for(int i = 0; i < wCount; i++)
	{
		tmp8.push_back(arr8+i);
		res8.push_back(arr8+i);
	}

	res8.removeAt(13);
	res8.insert(13 ,new StanfordWord(2,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>()));

	res8.removeAt(40);
	res8.insert(40 ,new StanfordWord(3,9, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>()));

	QListIterator<StanfordWord*> i8(tmp8);
	textTree* text8 = new textTree(i8);

	QListIterator<StanfordWord*> j8(res8);
	textTree* result8 = new textTree(j8);

	delete res8[40];
	delete res8[13];
	QTest::newRow("SeveralUncertainAreasRightSide") << text8 << result8 << arr8;
}

void TestClass::testexpansionCharacterZone()
{
	QFETCH(textTree*, input);
	QFETCH(textTree*, output);
	QFETCH(StanfordWord*, dell);

	expansionCharacterZone (input);
	
	QString message = CmpTrees(input, output);

	bool isOk = message.isEmpty() == true;

	QVERIFY2(isOk, message.toUtf8().constData());
	delete input;
	delete output;
	delete [] dell;
}

void TestClass::testinOrderFinder_data()
{
	QTest::addColumn<textTree*>("input");
	QTest::addColumn<textTree*>("output");
	QTest::addColumn<QSet <person> >("allPersons");
	QTest::addColumn<StanfordWord*> ("dell");

	/////////////////////////////////////���� 1
	/////////////////////////////////////UndefinedParagraphTwoCharacters

	int wCount = 46;

	StanfordWord* arr1 = new StanfordWord[wCount];

	arr1[0]= StanfordWord(1,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>());
	arr1[1]= StanfordWord(1,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr1[2]= StanfordWord(1,2, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr1[3]= StanfordWord(1,3, QString("am"),QString("be"),VBP, WORD, QVector<corefCoord>());
	arr1[4]= StanfordWord(1,4, QString("sorry"),QString("sorry"),JJ, WORD, QVector<corefCoord>());
	arr1[5]= StanfordWord(1,5, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr1[6]= StanfordWord(1,6, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr1[7]= StanfordWord(1,6, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());
	arr1[8]= StanfordWord(1,7, QString("said"),QString("say"),VBD, WORD, QVector<corefCoord>());
	arr1[9]= StanfordWord(1,8, QString("Frodo"),QString("Frodo"),NNP, WORD, QVector<corefCoord>());
	arr1[10]= StanfordWord(1,9, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());

	arr1[11]= StanfordWord(2,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>());
	arr1[12]= StanfordWord(2,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr1[13]= StanfordWord(2,2, QString("But"),QString("but"),CC, WORD, QVector<corefCoord>());
	arr1[14]= StanfordWord(2,3, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr1[15]= StanfordWord(2,4, QString("am"),QString("be"),VBP, WORD, QVector<corefCoord>());
	arr1[16]= StanfordWord(2,5, QString("frightened"),QString("frighten"),VBN, WORD, QVector<corefCoord>());
	arr1[17]= StanfordWord(2,6, QString(";"),QString(";"),Colon, WORD, QVector<corefCoord>());
	arr1[18]= StanfordWord(2,7, QString("and"),QString("and"),CC, WORD, QVector<corefCoord>());
	arr1[19]= StanfordWord(2,8, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr1[20]= StanfordWord(2,9, QString("do"),QString("do"),VBP, WORD, QVector<corefCoord>());
	arr1[21]= StanfordWord(2,10, QString("not"),QString("not"),RB, WORD, QVector<corefCoord>());
	arr1[22]= StanfordWord(2,11, QString("feel"),QString("feel"),VB, WORD, QVector<corefCoord>());
	arr1[23]= StanfordWord(2,12, QString("and"),QString("and"),DT, WORD, QVector<corefCoord>());
	arr1[24]= StanfordWord(2,13, QString("pity"),QString("pity"),NN, WORD, QVector<corefCoord>());
	arr1[25]= StanfordWord(2,14, QString("for"),QString("for"),IN, WORD, QVector<corefCoord>());
	arr1[26]= StanfordWord(2,15, QString("Golum"),QString("Golum"),NNP, WORD, QVector<corefCoord>());
	arr1[27]= StanfordWord(2,16, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr1[28]= StanfordWord(2,17, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr1[29]= StanfordWord(2,17, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());
	arr1[30]= StanfordWord(2,17, QString("</p>"),QString("</p>"),CC, MARK, QVector<corefCoord>());
	
	arr1[31]= StanfordWord(3,1, QString("<p>"),QString("<p>"),CC, MARK, QVector<corefCoord>());

	arr1[32]= StanfordWord(3,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr1[33]= StanfordWord(3,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr1[34]= StanfordWord(3,2, QString("You"),QString("you"),PRP, WORD, QVector<corefCoord>());
	arr1[35]= StanfordWord(3,3, QString("have"),QString("have"),VBP, WORD, QVector<corefCoord>());
	arr1[36]= StanfordWord(3,4, QString("not"),QString("not"),RB, WORD, QVector<corefCoord>());
	arr1[37]= StanfordWord(3,5, QString("seen"),QString("see"),VBN, WORD, QVector<corefCoord>());
	arr1[38]= StanfordWord(3,6, QString("him"),QString("he"),PRP, WORD, QVector<corefCoord>());
	arr1[39]= StanfordWord(3,7, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr1[40]= StanfordWord(3,8, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr1[41]= StanfordWord(3,8, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());
	arr1[42]= StanfordWord(3,9, QString("Gandalf"),QString("Gandalf"),NNP, WORD, QVector<corefCoord>());	
	arr1[43]= StanfordWord(3,10, QString("broke"),QString("break"),VBD, WORD, QVector<corefCoord>());
	arr1[44]= StanfordWord(3,11, QString("in"),QString("in"),IN, WORD, QVector<corefCoord>());
	arr1[45]= StanfordWord(3,12, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());

	QList<StanfordWord*> tmp1, res1;

	for(int i = 0; i < wCount; i++)
	{
		tmp1.push_back(arr1+i);
		res1.push_back(arr1+i);
	}

	res1.removeAt(32);
	res1.insert(32, new StanfordWord(3,1, QString("<span style=\"color:\#ff0000\">"),QString("<span style=\"color:\#ff0000\">"),CC, MARK, QVector<corefCoord>()));

	QListIterator<StanfordWord*> i1(tmp1);
	textTree* text1 = new textTree(i1);

	QListIterator<StanfordWord*> j1(res1);
	textTree* result1 = new textTree(j1);

	QSet  <person> allPersons1;

	allPersons1.insert(person(QString("Gandalf"), QStringList(QString("Gandalf")), QStringList(), QString("ff0000")));
	allPersons1.insert(person(QString("Frodo"), QStringList(QString("Frodo")), QStringList(), QString("00ff00")));

	delete res1[32];

	QTest::newRow("UndefinedParagraphTwoCharacters") << text1 << result1 << allPersons1 << arr1;

	/////////////////////////////////////���� 2
	/////////////////////////////////////UndefinedParagraphBetweenTwoDefinedAndTwoCharacters

	wCount = 74;

	StanfordWord* arr2 = new StanfordWord[wCount];

	arr2[0]= StanfordWord(1,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>());
	arr2[1]= StanfordWord(1,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr2[2]= StanfordWord(1,2, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr2[3]= StanfordWord(1,3, QString("am"),QString("be"),VBP, WORD, QVector<corefCoord>());
	arr2[4]= StanfordWord(1,4, QString("sorry"),QString("sorry"),JJ, WORD, QVector<corefCoord>());
	arr2[5]= StanfordWord(1,5, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr2[6]= StanfordWord(1,6, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr2[7]= StanfordWord(1,6, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());
	arr2[8]= StanfordWord(1,7, QString("said"),QString("say"),VBD, WORD, QVector<corefCoord>());
	arr2[9]= StanfordWord(1,8, QString("Frodo"),QString("Frodo"),NNP, WORD, QVector<corefCoord>());
	arr2[10]= StanfordWord(1,9, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr2[11]= StanfordWord(2,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>());
	arr2[12]= StanfordWord(2,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr2[13]= StanfordWord(2,2, QString("But"),QString("but"),CC, WORD, QVector<corefCoord>());
	arr2[14]= StanfordWord(2,3, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr2[15]= StanfordWord(2,4, QString("am"),QString("be"),VBP, WORD, QVector<corefCoord>());
	arr2[16]= StanfordWord(2,5, QString("frightened"),QString("frighten"),VBN, WORD, QVector<corefCoord>());
	arr2[17]= StanfordWord(2,6, QString(";"),QString(";"),Colon, WORD, QVector<corefCoord>());
	arr2[18]= StanfordWord(2,7, QString("and"),QString("and"),CC, WORD, QVector<corefCoord>());
	arr2[19]= StanfordWord(2,8, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr2[20]= StanfordWord(2,9, QString("do"),QString("do"),VBP, WORD, QVector<corefCoord>());
	arr2[21]= StanfordWord(2,10, QString("not"),QString("not"),RB, WORD, QVector<corefCoord>());
	arr2[22]= StanfordWord(2,11, QString("feel"),QString("feel"),VB, WORD, QVector<corefCoord>());
	arr2[23]= StanfordWord(2,12, QString("and"),QString("and"),DT, WORD, QVector<corefCoord>());
	arr2[24]= StanfordWord(2,13, QString("pity"),QString("pity"),NN, WORD, QVector<corefCoord>());
	arr2[25]= StanfordWord(2,14, QString("for"),QString("for"),IN, WORD, QVector<corefCoord>());
	arr2[26]= StanfordWord(2,15, QString("Golum"),QString("Golum"),NNP, WORD, QVector<corefCoord>());
	arr2[27]= StanfordWord(2,16, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr2[28]= StanfordWord(2,17, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr2[29]= StanfordWord(2,17, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());
	arr2[30]= StanfordWord(2,17, QString("</p>"),QString("</p>"),CC, MARK, QVector<corefCoord>());
	arr2[31]= StanfordWord(3,1, QString("<p>"),QString("<p>"),CC, MARK, QVector<corefCoord>());
	arr2[32]= StanfordWord(3,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr2[33]= StanfordWord(3,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr2[34]= StanfordWord(3,2, QString("You"),QString("you"),PRP, WORD, QVector<corefCoord>());
	arr2[35]= StanfordWord(3,3, QString("have"),QString("have"),VBP, WORD, QVector<corefCoord>());
	arr2[36]= StanfordWord(3,4, QString("not"),QString("not"),RB, WORD, QVector<corefCoord>());
	arr2[37]= StanfordWord(3,5, QString("seen"),QString("see"),VBN, WORD, QVector<corefCoord>());
	arr2[38]= StanfordWord(3,6, QString("him"),QString("he"),PRP, WORD, QVector<corefCoord>());
	arr2[39]= StanfordWord(3,7, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr2[40]= StanfordWord(3,8, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr2[41]= StanfordWord(3,8, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());
	arr2[42]= StanfordWord(3,9, QString("Gandalf"),QString("Gandalf"),NNP, WORD, QVector<corefCoord>());	
	arr2[43]= StanfordWord(3,10, QString("broke"),QString("break"),VBD, WORD, QVector<corefCoord>());
	arr2[44]= StanfordWord(3,11, QString("in"),QString("in"),IN, WORD, QVector<corefCoord>());
	arr2[45]= StanfordWord(3,12, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr2[46]= StanfordWord(3,12, QString("</p>"),QString("</p>"),CC, MARK, QVector<corefCoord>());
	arr2[47]= StanfordWord(4,1, QString("<p>"),QString("<p>"),CC, MARK, QVector<corefCoord>());
	arr2[48]= StanfordWord(4,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>());
	arr2[49]= StanfordWord(4,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr2[50]= StanfordWord(4,2, QString("No"),QString("no"),DT, WORD, QVector<corefCoord>());
	arr2[51]= StanfordWord(4,3, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr2[52]= StanfordWord(4,4, QString("and"),QString("and"),CC, WORD, QVector<corefCoord>());
	arr2[53]= StanfordWord(4,5, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr2[54]= StanfordWord(4,6, QString("do"),QString("do"),VBP, WORD, QVector<corefCoord>());
	arr2[55]= StanfordWord(4,7, QString("n't"),QString("not"),RB, WORD, QVector<corefCoord>());
	arr2[56]= StanfordWord(4,8, QString("want"),QString("want"),VB, WORD, QVector<corefCoord>());
	arr2[57]= StanfordWord(4,9, QString("to"),QString("to"),TO, WORD, QVector<corefCoord>());
	arr2[58]= StanfordWord(4,10, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr2[59]= StanfordWord(4,11, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr2[60]= StanfordWord(4,11, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());
	arr2[61]= StanfordWord(4,12, QString("said"),QString("say"),VBD, WORD, QVector<corefCoord>());
	arr2[62]= StanfordWord(4,12, QString("Frodo"),QString("Frodo"),NNP, WORD, QVector<corefCoord>());
	arr2[63]= StanfordWord(4,13, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr2[64]= StanfordWord(5,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>());
	arr2[65]= StanfordWord(5,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr2[66]= StanfordWord(5,2, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr2[67]= StanfordWord(5,3, QString("ca"),QString("ca"),MD, WORD, QVector<corefCoord>());
	arr2[68]= StanfordWord(5,4, QString("n't"),QString("not"),RB, WORD, QVector<corefCoord>());
	arr2[69]= StanfordWord(5,5, QString("understand"),QString("understand"),VB, WORD, QVector<corefCoord>());
	arr2[70]= StanfordWord(5,6, QString("you"),QString("you"),PRP, WORD, QVector<corefCoord>());
	arr2[71]= StanfordWord(5,7, QString("."),QString("."),Comma, WORD, QVector<corefCoord>());
	arr2[72]= StanfordWord(5,8, QString("'"),QString("'"),SentFinPunct, WORD, QVector<corefCoord>());
	arr2[73]= StanfordWord(5,8, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());

	QList<StanfordWord*> tmp2, res2;

	for(int i = 0; i < wCount; i++)
	{
		tmp2.push_back(arr2+i);
		res2.push_back(arr2+i);
	}

	res2.removeAt(32);
	res2.insert(32, new StanfordWord(3,1, QString("<span style=\"color:\#ff0000\">"),QString("<span style=\"color:\#ff0000\">"),CC, MARK, QVector<corefCoord>()));

	QListIterator<StanfordWord*> i2(tmp2);
	textTree* text2 = new textTree(i2);

	QListIterator<StanfordWord*> j2(res2);
	textTree* result2 = new textTree(j2);

	QSet  <person> allPersons2;

	allPersons2.insert(person(QString("Gandalf"), QStringList(QString("Gandalf")), QStringList(), QString("ff0000")));
	allPersons2.insert(person(QString("Frodo"), QStringList(QString("Frodo")), QStringList(), QString("00ff00")));

	delete res2[32];
	QTest::newRow("UndefinedParagraphBetweenTwoDefinedAndTwoCharacters") << text2 << result2 << allPersons2 << arr2;

	/////////////////////////////////////���� 3
	/////////////////////////////////////JustOneParagraphDefinedTwoCharacters

	wCount = 74;

	StanfordWord* arr3 = new StanfordWord[wCount];

	arr3[0]= StanfordWord(1,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>());
	arr3[1]= StanfordWord(1,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr3[2]= StanfordWord(1,2, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr3[3]= StanfordWord(1,3, QString("am"),QString("be"),VBP, WORD, QVector<corefCoord>());
	arr3[4]= StanfordWord(1,4, QString("sorry"),QString("sorry"),JJ, WORD, QVector<corefCoord>());
	arr3[5]= StanfordWord(1,5, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr3[6]= StanfordWord(1,6, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr3[7]= StanfordWord(1,6, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());
	arr3[8]= StanfordWord(1,7, QString("said"),QString("say"),VBD, WORD, QVector<corefCoord>());
	arr3[9]= StanfordWord(1,8, QString("Frodo"),QString("Frodo"),NNP, WORD, QVector<corefCoord>());
	arr3[10]= StanfordWord(1,9, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr3[11]= StanfordWord(2,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>());
	arr3[12]= StanfordWord(2,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr3[13]= StanfordWord(2,2, QString("But"),QString("but"),CC, WORD, QVector<corefCoord>());
	arr3[14]= StanfordWord(2,3, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr3[15]= StanfordWord(2,4, QString("am"),QString("be"),VBP, WORD, QVector<corefCoord>());
	arr3[16]= StanfordWord(2,5, QString("frightened"),QString("frighten"),VBN, WORD, QVector<corefCoord>());
	arr3[17]= StanfordWord(2,6, QString(";"),QString(";"),Colon, WORD, QVector<corefCoord>());
	arr3[18]= StanfordWord(2,7, QString("and"),QString("and"),CC, WORD, QVector<corefCoord>());
	arr3[19]= StanfordWord(2,8, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr3[20]= StanfordWord(2,9, QString("do"),QString("do"),VBP, WORD, QVector<corefCoord>());
	arr3[21]= StanfordWord(2,10, QString("not"),QString("not"),RB, WORD, QVector<corefCoord>());
	arr3[22]= StanfordWord(2,11, QString("feel"),QString("feel"),VB, WORD, QVector<corefCoord>());
	arr3[23]= StanfordWord(2,12, QString("and"),QString("and"),DT, WORD, QVector<corefCoord>());
	arr3[24]= StanfordWord(2,13, QString("pity"),QString("pity"),NN, WORD, QVector<corefCoord>());
	arr3[25]= StanfordWord(2,14, QString("for"),QString("for"),IN, WORD, QVector<corefCoord>());
	arr3[26]= StanfordWord(2,15, QString("Golum"),QString("Golum"),NNP, WORD, QVector<corefCoord>());
	arr3[27]= StanfordWord(2,16, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr3[28]= StanfordWord(2,17, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr3[29]= StanfordWord(2,17, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());
	arr3[30]= StanfordWord(2,17, QString("</p>"),QString("</p>"),CC, MARK, QVector<corefCoord>());
	arr3[31]= StanfordWord(3,1, QString("<p>"),QString("<p>"),CC, MARK, QVector<corefCoord>());
	arr3[32]= StanfordWord(3,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr3[33]= StanfordWord(3,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr3[34]= StanfordWord(3,2, QString("You"),QString("you"),PRP, WORD, QVector<corefCoord>());
	arr3[35]= StanfordWord(3,3, QString("have"),QString("have"),VBP, WORD, QVector<corefCoord>());
	arr3[36]= StanfordWord(3,4, QString("not"),QString("not"),RB, WORD, QVector<corefCoord>());
	arr3[37]= StanfordWord(3,5, QString("seen"),QString("see"),VBN, WORD, QVector<corefCoord>());
	arr3[38]= StanfordWord(3,6, QString("him"),QString("he"),PRP, WORD, QVector<corefCoord>());
	arr3[39]= StanfordWord(3,7, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr3[40]= StanfordWord(3,8, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr3[41]= StanfordWord(3,8, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());
	arr3[42]= StanfordWord(3,9, QString("Gandalf"),QString("Gandalf"),NNP, WORD, QVector<corefCoord>());	
	arr3[43]= StanfordWord(3,10, QString("broke"),QString("break"),VBD, WORD, QVector<corefCoord>());
	arr3[44]= StanfordWord(3,11, QString("in"),QString("in"),IN, WORD, QVector<corefCoord>());
	arr3[45]= StanfordWord(3,12, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr3[46]= StanfordWord(3,12, QString("</p>"),QString("</p>"),CC, MARK, QVector<corefCoord>());
	arr3[47]= StanfordWord(4,1, QString("<p>"),QString("<p>"),CC, MARK, QVector<corefCoord>());
	arr3[48]= StanfordWord(4,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr3[49]= StanfordWord(4,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr3[50]= StanfordWord(4,2, QString("No"),QString("no"),DT, WORD, QVector<corefCoord>());
	arr3[51]= StanfordWord(4,3, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr3[52]= StanfordWord(4,4, QString("and"),QString("and"),CC, WORD, QVector<corefCoord>());
	arr3[53]= StanfordWord(4,5, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr3[54]= StanfordWord(4,6, QString("do"),QString("do"),VBP, WORD, QVector<corefCoord>());
	arr3[55]= StanfordWord(4,7, QString("n't"),QString("not"),RB, WORD, QVector<corefCoord>());
	arr3[56]= StanfordWord(4,8, QString("want"),QString("want"),VB, WORD, QVector<corefCoord>());
	arr3[57]= StanfordWord(4,9, QString("to"),QString("to"),TO, WORD, QVector<corefCoord>());
	arr3[58]= StanfordWord(4,10, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr3[59]= StanfordWord(4,11, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr3[60]= StanfordWord(4,11, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());
	arr3[61]= StanfordWord(4,12, QString("cried"),QString("cry"),VBD, WORD, QVector<corefCoord>());
	arr3[62]= StanfordWord(4,12, QString("Frodo"),QString("Frodo"),NNP, WORD, QVector<corefCoord>());
	arr3[63]= StanfordWord(4,13, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr3[64]= StanfordWord(5,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr3[65]= StanfordWord(5,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr3[66]= StanfordWord(5,2, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr3[67]= StanfordWord(5,3, QString("ca"),QString("ca"),MD, WORD, QVector<corefCoord>());
	arr3[68]= StanfordWord(5,4, QString("n't"),QString("not"),RB, WORD, QVector<corefCoord>());
	arr3[69]= StanfordWord(5,5, QString("understand"),QString("understand"),VB, WORD, QVector<corefCoord>());
	arr3[70]= StanfordWord(5,6, QString("you"),QString("you"),PRP, WORD, QVector<corefCoord>());
	arr3[71]= StanfordWord(5,7, QString("."),QString("."),Comma, WORD, QVector<corefCoord>());
	arr3[72]= StanfordWord(5,8, QString("'"),QString("'"),SentFinPunct, WORD, QVector<corefCoord>());
	arr3[73]= StanfordWord(5,8, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());

	QList<StanfordWord*> tmp3, res3;

	for(int i = 0; i < wCount; i++)
	{
		tmp3.push_back(arr3+i);
		res3.push_back(arr3+i);
	}

	res3.removeAt(32);
	res3.insert(32, new StanfordWord(3,1, QString("<span style=\"color:\#ff0000\">"),QString("<span style=\"color:\#ff0000\">"),CC, MARK, QVector<corefCoord>()));

	res3.removeAt(48);
	res3.insert(48, new StanfordWord(4,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>()));

	res3.removeAt(64);
	res3.insert(64, new StanfordWord(5,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>()));

	QListIterator<StanfordWord*> i3(tmp3);
	textTree* text3 = new textTree(i3);

	QListIterator<StanfordWord*> j3(res3);
	textTree* result3 = new textTree(j3);

	QSet  <person> allPersons3;

	allPersons3.insert(person(QString("Gandalf"), QStringList(QString("Gandalf")), QStringList(), QString("ff0000")));
	allPersons3.insert(person(QString("Frodo"), QStringList(QString("Frodo")), QStringList(), QString("00ff00")));

	delete res3[32];
	delete res3[48];
	delete res3[64];
	QTest::newRow("JustOneParagraphDefinedTwoCharacters") << text3 << result3 << allPersons3 << arr3;

	/////////////////////////////////////���� 4
	/////////////////////////////////////JustOneParagraphInCenterDefinedTwoCharacters
	
	wCount = 73;

	StanfordWord* arr4 = new StanfordWord[wCount];

	arr4[0]= StanfordWord(1,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr4[1]= StanfordWord(1,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr4[2]= StanfordWord(1,2, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr4[3]= StanfordWord(1,3, QString("am"),QString("be"),VBP, WORD, QVector<corefCoord>());
	arr4[4]= StanfordWord(1,4, QString("sorry"),QString("sorry"),JJ, WORD, QVector<corefCoord>());
	arr4[5]= StanfordWord(1,5, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr4[6]= StanfordWord(1,6, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr4[7]= StanfordWord(1,6, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());
	arr4[8]= StanfordWord(1,7, QString("cried"),QString("cry"),VBD, WORD, QVector<corefCoord>());
	arr4[9]= StanfordWord(1,8, QString("Frodo"),QString("Frodo"),NNP, WORD, QVector<corefCoord>());
	arr4[10]= StanfordWord(1,9, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr4[11]= StanfordWord(2,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr4[12]= StanfordWord(2,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr4[13]= StanfordWord(2,2, QString("But"),QString("but"),CC, WORD, QVector<corefCoord>());
	arr4[14]= StanfordWord(2,3, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr4[15]= StanfordWord(2,4, QString("am"),QString("be"),VBP, WORD, QVector<corefCoord>());
	arr4[16]= StanfordWord(2,5, QString("frightened"),QString("frighten"),VBN, WORD, QVector<corefCoord>());
	arr4[17]= StanfordWord(2,6, QString(";"),QString(";"),Colon, WORD, QVector<corefCoord>());
	arr4[18]= StanfordWord(2,7, QString("and"),QString("and"),CC, WORD, QVector<corefCoord>());
	arr4[19]= StanfordWord(2,8, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr4[20]= StanfordWord(2,9, QString("do"),QString("do"),VBP, WORD, QVector<corefCoord>());
	arr4[21]= StanfordWord(2,10, QString("not"),QString("not"),RB, WORD, QVector<corefCoord>());
	arr4[22]= StanfordWord(2,11, QString("feel"),QString("feel"),VB, WORD, QVector<corefCoord>());
	arr4[23]= StanfordWord(2,12, QString("and"),QString("and"),DT, WORD, QVector<corefCoord>());
	arr4[24]= StanfordWord(2,13, QString("pity"),QString("pity"),NN, WORD, QVector<corefCoord>());
	arr4[25]= StanfordWord(2,14, QString("for"),QString("for"),IN, WORD, QVector<corefCoord>());
	arr4[26]= StanfordWord(2,15, QString("Golum"),QString("Golum"),NNP, WORD, QVector<corefCoord>());
	arr4[27]= StanfordWord(2,16, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr4[28]= StanfordWord(2,17, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr4[29]= StanfordWord(2,17, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());
	arr4[30]= StanfordWord(2,17, QString("</p>"),QString("</p>"),CC, MARK, QVector<corefCoord>());
	arr4[31]= StanfordWord(3,1, QString("<p>"),QString("<p>"),CC, MARK, QVector<corefCoord>());
	arr4[32]= StanfordWord(3,1, QString("<span style=\"color:\#ff0000\">"),QString("<span style=\"color:\#ff0000\">"),CC, MARK, QVector<corefCoord>());
	arr4[33]= StanfordWord(3,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr4[34]= StanfordWord(3,2, QString("You"),QString("you"),PRP, WORD, QVector<corefCoord>());
	arr4[35]= StanfordWord(3,3, QString("have"),QString("have"),VBP, WORD, QVector<corefCoord>());
	arr4[36]= StanfordWord(3,4, QString("not"),QString("not"),RB, WORD, QVector<corefCoord>());
	arr4[37]= StanfordWord(3,5, QString("seen"),QString("see"),VBN, WORD, QVector<corefCoord>());
	arr4[38]= StanfordWord(3,6, QString("him"),QString("he"),PRP, WORD, QVector<corefCoord>());
	arr4[39]= StanfordWord(3,7, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr4[40]= StanfordWord(3,8, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr4[41]= StanfordWord(3,8, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());
	
	arr4[42]= StanfordWord(3,9, QString("said"),QString("say"),VBD, WORD, QVector<corefCoord>());
	arr4[43]= StanfordWord(3,10, QString("Gandalf"),QString("Gandalf"),NNP, WORD, QVector<corefCoord>());	
	
	arr4[44]= StanfordWord(3,11, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr4[45]= StanfordWord(3,11, QString("</p>"),QString("</p>"),CC, MARK, QVector<corefCoord>());
	arr4[46]= StanfordWord(4,1, QString("<p>"),QString("<p>"),CC, MARK, QVector<corefCoord>());
	arr4[47]= StanfordWord(4,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr4[48]= StanfordWord(4,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr4[49]= StanfordWord(4,2, QString("No"),QString("no"),DT, WORD, QVector<corefCoord>());
	arr4[50]= StanfordWord(4,3, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr4[51]= StanfordWord(4,4, QString("and"),QString("and"),CC, WORD, QVector<corefCoord>());
	arr4[52]= StanfordWord(4,5, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr4[53]= StanfordWord(4,6, QString("do"),QString("do"),VBP, WORD, QVector<corefCoord>());
	arr4[54]= StanfordWord(4,7, QString("n't"),QString("not"),RB, WORD, QVector<corefCoord>());
	arr4[55]= StanfordWord(4,8, QString("want"),QString("want"),VB, WORD, QVector<corefCoord>());
	arr4[56]= StanfordWord(4,9, QString("to"),QString("to"),TO, WORD, QVector<corefCoord>());
	arr4[57]= StanfordWord(4,10, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr4[58]= StanfordWord(4,11, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr4[59]= StanfordWord(4,11, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());
	arr4[60]= StanfordWord(4,12, QString("cried"),QString("cry"),VBD, WORD, QVector<corefCoord>());
	arr4[61]= StanfordWord(4,12, QString("Frodo"),QString("Frodo"),NNP, WORD, QVector<corefCoord>());
	arr4[62]= StanfordWord(4,13, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr4[63]= StanfordWord(5,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr4[64]= StanfordWord(5,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr4[65]= StanfordWord(5,2, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr4[66]= StanfordWord(5,3, QString("ca"),QString("ca"),MD, WORD, QVector<corefCoord>());
	arr4[67]= StanfordWord(5,4, QString("n't"),QString("not"),RB, WORD, QVector<corefCoord>());
	arr4[68]= StanfordWord(5,5, QString("understand"),QString("understand"),VB, WORD, QVector<corefCoord>());
	arr4[69]= StanfordWord(5,6, QString("you"),QString("you"),PRP, WORD, QVector<corefCoord>());
	arr4[70]= StanfordWord(5,7, QString("."),QString("."),Comma, WORD, QVector<corefCoord>());
	arr4[71]= StanfordWord(5,8, QString("'"),QString("'"),SentFinPunct, WORD, QVector<corefCoord>());
	arr4[72]= StanfordWord(5,8, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());
		 
	QList<StanfordWord*> tmp4, res4;

	for(int i = 0; i < wCount; i++)
	{
		tmp4.push_back(arr4+i);
		res4.push_back(arr4+i);
	}

	res4.removeAt(0);
	res4.insert(0, new StanfordWord(1,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>()));

	res4.removeAt(11);
	res4.insert(11, new StanfordWord(2,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>()));

	res4.removeAt(47);
	res4.insert(47, new StanfordWord(4,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>()));

	res4.removeAt(63);
	res4.insert(63, new StanfordWord(5,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>()));

	QListIterator<StanfordWord*> i4(tmp4);
	textTree* text4 = new textTree(i4);

	QListIterator<StanfordWord*> j4(res4);
	textTree* result4 = new textTree(j4);

	QSet  <person> allPersons4;

	allPersons4.insert(person(QString("Gandalf"), QStringList(QString("Gandalf")), QStringList(), QString("ff0000")));
	allPersons4.insert(person(QString("Frodo"), QStringList(QString("Frodo")), QStringList(), QString("00ff00")));

	delete res4[0];
	delete res4[11];
	delete res4[47];
	delete res4[63];
	QTest::newRow("JustOneParagraphInCenterDefinedTwoCharacters") << text4 << result4 << allPersons4 << arr4;

	/////////////////////////////////////���� 5
	/////////////////////////////////////JustOneParagraphInEndDefinedTwoCharacters
	
	wCount = 74;

	StanfordWord* arr5 = new StanfordWord[wCount];

	arr5[0]= StanfordWord(1,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr5[1]= StanfordWord(1,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr5[2]= StanfordWord(1,2, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr5[3]= StanfordWord(1,3, QString("am"),QString("be"),VBP, WORD, QVector<corefCoord>());
	arr5[4]= StanfordWord(1,4, QString("sorry"),QString("sorry"),JJ, WORD, QVector<corefCoord>());
	arr5[5]= StanfordWord(1,5, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr5[6]= StanfordWord(1,6, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr5[7]= StanfordWord(1,6, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());
	arr5[8]= StanfordWord(1,7, QString("cried"),QString("cry"),VBD, WORD, QVector<corefCoord>());
	arr5[9]= StanfordWord(1,8, QString("Frodo"),QString("Frodo"),NNP, WORD, QVector<corefCoord>());
	arr5[10]= StanfordWord(1,9, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr5[11]= StanfordWord(2,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr5[12]= StanfordWord(2,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr5[13]= StanfordWord(2,2, QString("But"),QString("but"),CC, WORD, QVector<corefCoord>());
	arr5[14]= StanfordWord(2,3, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr5[15]= StanfordWord(2,4, QString("am"),QString("be"),VBP, WORD, QVector<corefCoord>());
	arr5[16]= StanfordWord(2,5, QString("frightened"),QString("frighten"),VBN, WORD, QVector<corefCoord>());
	arr5[17]= StanfordWord(2,6, QString(";"),QString(";"),Colon, WORD, QVector<corefCoord>());
	arr5[18]= StanfordWord(2,7, QString("and"),QString("and"),CC, WORD, QVector<corefCoord>());
	arr5[19]= StanfordWord(2,8, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr5[20]= StanfordWord(2,9, QString("do"),QString("do"),VBP, WORD, QVector<corefCoord>());
	arr5[21]= StanfordWord(2,10, QString("not"),QString("not"),RB, WORD, QVector<corefCoord>());
	arr5[22]= StanfordWord(2,11, QString("feel"),QString("feel"),VB, WORD, QVector<corefCoord>());
	arr5[23]= StanfordWord(2,12, QString("and"),QString("and"),DT, WORD, QVector<corefCoord>());
	arr5[24]= StanfordWord(2,13, QString("pity"),QString("pity"),NN, WORD, QVector<corefCoord>());
	arr5[25]= StanfordWord(2,14, QString("for"),QString("for"),IN, WORD, QVector<corefCoord>());
	arr5[26]= StanfordWord(2,15, QString("Golum"),QString("Golum"),NNP, WORD, QVector<corefCoord>());
	arr5[27]= StanfordWord(2,16, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr5[28]= StanfordWord(2,17, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr5[29]= StanfordWord(2,17, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());
	arr5[30]= StanfordWord(2,17, QString("</p>"),QString("</p>"),CC, MARK, QVector<corefCoord>());
	arr5[31]= StanfordWord(3,1, QString("<p>"),QString("<p>"),CC, MARK, QVector<corefCoord>());
	arr5[32]= StanfordWord(3,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr5[33]= StanfordWord(3,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr5[34]= StanfordWord(3,2, QString("You"),QString("you"),PRP, WORD, QVector<corefCoord>());
	arr5[35]= StanfordWord(3,3, QString("have"),QString("have"),VBP, WORD, QVector<corefCoord>());
	arr5[36]= StanfordWord(3,4, QString("not"),QString("not"),RB, WORD, QVector<corefCoord>());
	arr5[37]= StanfordWord(3,5, QString("seen"),QString("see"),VBN, WORD, QVector<corefCoord>());
	arr5[38]= StanfordWord(3,6, QString("him"),QString("he"),PRP, WORD, QVector<corefCoord>());
	arr5[39]= StanfordWord(3,7, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr5[40]= StanfordWord(3,8, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr5[41]= StanfordWord(3,8, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());
	arr5[42]= StanfordWord(3,9, QString("Gandalf"),QString("Gandalf"),NNP, WORD, QVector<corefCoord>());	
	arr5[43]= StanfordWord(3,10, QString("broke"),QString("break"),VBD, WORD, QVector<corefCoord>());
	arr5[44]= StanfordWord(3,11, QString("in"),QString("in"),IN, WORD, QVector<corefCoord>());
	arr5[45]= StanfordWord(3,12, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr5[46]= StanfordWord(3,12, QString("</p>"),QString("</p>"),CC, MARK, QVector<corefCoord>());
	arr5[47]= StanfordWord(4,1, QString("<p>"),QString("<p>"),CC, MARK, QVector<corefCoord>());
	arr5[48]= StanfordWord(4,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>());
	arr5[49]= StanfordWord(4,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr5[50]= StanfordWord(4,2, QString("No"),QString("no"),DT, WORD, QVector<corefCoord>());
	arr5[51]= StanfordWord(4,3, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr5[52]= StanfordWord(4,4, QString("and"),QString("and"),CC, WORD, QVector<corefCoord>());
	arr5[53]= StanfordWord(4,5, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr5[54]= StanfordWord(4,6, QString("do"),QString("do"),VBP, WORD, QVector<corefCoord>());
	arr5[55]= StanfordWord(4,7, QString("n't"),QString("not"),RB, WORD, QVector<corefCoord>());
	arr5[56]= StanfordWord(4,8, QString("want"),QString("want"),VB, WORD, QVector<corefCoord>());
	arr5[57]= StanfordWord(4,9, QString("to"),QString("to"),TO, WORD, QVector<corefCoord>());
	arr5[58]= StanfordWord(4,10, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr5[59]= StanfordWord(4,11, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr5[60]= StanfordWord(4,11, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());
	arr5[61]= StanfordWord(4,12, QString("said"),QString("say"),VBD, WORD, QVector<corefCoord>());
	arr5[62]= StanfordWord(4,12, QString("Frodo"),QString("Frodo"),NNP, WORD, QVector<corefCoord>());
	arr5[63]= StanfordWord(4,13, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr5[64]= StanfordWord(5,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>());
	arr5[65]= StanfordWord(5,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr5[66]= StanfordWord(5,2, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr5[67]= StanfordWord(5,3, QString("ca"),QString("ca"),MD, WORD, QVector<corefCoord>());
	arr5[68]= StanfordWord(5,4, QString("n't"),QString("not"),RB, WORD, QVector<corefCoord>());
	arr5[69]= StanfordWord(5,5, QString("understand"),QString("understand"),VB, WORD, QVector<corefCoord>());
	arr5[70]= StanfordWord(5,6, QString("you"),QString("you"),PRP, WORD, QVector<corefCoord>());
	arr5[71]= StanfordWord(5,7, QString("."),QString("."),Comma, WORD, QVector<corefCoord>());
	arr5[72]= StanfordWord(5,8, QString("'"),QString("'"),SentFinPunct, WORD, QVector<corefCoord>());
	arr5[73]= StanfordWord(5,8, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());

	QList<StanfordWord*> tmp5, res5;

	for(int i = 0; i < wCount; i++)
	{
		tmp5.push_back(arr5+i);
		res5.push_back(arr5+i);
	}

	res5.removeAt(0);
	res5.insert(0, new StanfordWord(1,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>()));

	res5.removeAt(11);
	res5.insert(11, new StanfordWord(2,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>()));

	res5.removeAt(32);
	res5.insert(32, new StanfordWord(3,1, QString("<span style=\"color:\#ff0000\">"),QString("<span style=\"color:\#ff0000\">"),CC, MARK, QVector<corefCoord>()));

	QListIterator<StanfordWord*> i5(tmp5);
	textTree* text5 = new textTree(i5);

	QListIterator<StanfordWord*> j5(res5);
	textTree* result5 = new textTree(j5);

	QSet  <person> allPersons5;

	allPersons5.insert(person(QString("Gandalf"), QStringList(QString("Gandalf")), QStringList(), QString("ff0000")));
	allPersons5.insert(person(QString("Frodo"), QStringList(QString("Frodo")), QStringList(), QString("00ff00")));

	delete res5[0];
	delete res5[11];
	delete res5[32];
	QTest::newRow("JustOneParagraphInEndDefinedTwoCharacters") << text5 << result5 << allPersons5 << arr5;
}

void TestClass::testinOrderFinder()
{
	QFETCH(textTree*, input);
	QFETCH(textTree*, output);
	QFETCH(QSet <person>, allPersons);
	person::InitPer(allPersons);
	QFETCH(StanfordWord*, dell);

	inOrderFinder(input);
	
	QString message = CmpTrees(input, output);

	bool isOk = message.isEmpty() == true;

	QVERIFY2(isOk, message.toUtf8().constData());
	delete input;
	delete output;
	delete [] dell;
}

void TestClass::testtextMarkup_data()
{
	QTest::addColumn<QSet <person> >("allPersons");
	QTest::addColumn<QMap <QString, StanfordWord*> >("Text");
	QTest::addColumn<textTree*>("input");
	QTest::addColumn<textTree*>("output");
	QTest::addColumn<QSet <person> >("speakersList");
	QTest::addColumn<StanfordWord*> ("dell");

	/////////////////////////////////////���� 1
	/////////////////////////////////////TwoPersonsExactCharacterDefinition

	int wCount = 84;
	StanfordWord* arr1 = new StanfordWord[wCount];

	arr1[0]= StanfordWord(1,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr1[1]= StanfordWord(1,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr1[2]= StanfordWord(1,2, QString("How"),QString("how"),WRB, WORD, QVector<corefCoord>());
	arr1[3]= StanfordWord(1,3, QString("bright"),QString("bright"),JJ, WORD, QVector<corefCoord>());
	arr1[4]= StanfordWord(1,4, QString("your"),QString("you"),PRPdollar, WORD, QVector<corefCoord>());
	arr1[5]= StanfordWord(1,5, QString("garden"),QString("garden"),NN, WORD, QVector<corefCoord>());
	arr1[6]= StanfordWord(1,6, QString("looks"),QString("look"),VBZ, WORD, QVector<corefCoord>());
	arr1[7]= StanfordWord(1,7, QString("!"),QString("!"),SentFinPunct, WORD, QVector<corefCoord>());
	arr1[8]= StanfordWord(1,8, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr1[9]= StanfordWord(1,8, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());
	arr1[10]= StanfordWord(2,1, QString("said"),QString("say"),VBD, WORD, QVector<corefCoord>());
	arr1[11]= StanfordWord(2,2, QString("Gandalf"),QString("Gandalf"),NNP, WORD, QVector<corefCoord>());
	arr1[12]= StanfordWord(2,3, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr1[13]= StanfordWord(2,3, QString("</p>"),QString("</p>"),CC, MARK, QVector<corefCoord>());
	
	arr1[14]= StanfordWord(3,1, QString("<p>"),QString("<p>"),CC, MARK, QVector<corefCoord>());
	arr1[15]= StanfordWord(3,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr1[16]= StanfordWord(3,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr1[17]= StanfordWord(3,2, QString("Yes"),QString("yes"),UH, WORD, QVector<corefCoord>());
	arr1[18]= StanfordWord(3,3, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr1[19]= StanfordWord(3,4, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr1[20]= StanfordWord(3,4, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());
	arr1[21]= StanfordWord(3,5, QString("said"),QString("say"),VBD, WORD, QVector<corefCoord>());
	arr1[22]= StanfordWord(3,6, QString("Bilbo"),QString("Bilbo"),NNP, WORD, QVector<corefCoord>());
	arr1[23]= StanfordWord(3,7, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());

	arr1[24]= StanfordWord(4,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr1[25]= StanfordWord(4,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr1[26]= StanfordWord(4,2, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr1[27]= StanfordWord(4,3, QString("am"),QString("be"),VBP, WORD, QVector<corefCoord>());
	arr1[28]= StanfordWord(4,4, QString("very"),QString("very"),RB, WORD, QVector<corefCoord>());
	arr1[29]= StanfordWord(4,5, QString("fond"),QString("fond"),JJ, WORD, QVector<corefCoord>());
	arr1[30]= StanfordWord(4,6, QString("indeed"),QString("indeed"),RB, WORD, QVector<corefCoord>());
	arr1[31]= StanfordWord(4,7, QString("of"),QString("of"),IN, WORD, QVector<corefCoord>());
	arr1[32]= StanfordWord(4,8, QString("it"),QString("it"),PRP, WORD, QVector<corefCoord>());
	arr1[33]= StanfordWord(4,9, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr1[34]= StanfordWord(4,10, QString("and"),QString("and"),CC, WORD, QVector<corefCoord>());
	arr1[35]= StanfordWord(4,11, QString("of"),QString("of"),IN, WORD, QVector<corefCoord>());
	arr1[36]= StanfordWord(4,12, QString("all"),QString("all"),PDT, WORD, QVector<corefCoord>());
	arr1[37]= StanfordWord(4,13, QString("the"),QString("the"),DT, WORD, QVector<corefCoord>());
	arr1[38]= StanfordWord(4,14, QString("dear"),QString("dear"),RB, WORD, QVector<corefCoord>());
	arr1[39]= StanfordWord(4,15, QString("old"),QString("old"),JJ, WORD, QVector<corefCoord>());
	arr1[40]= StanfordWord(4,16, QString("Shire"),QString("Shire"),NNP, WORD, QVector<corefCoord>());
	arr1[41]= StanfordWord(4,17, QString(";"),QString(";"),Colon, WORD, QVector<corefCoord>());
	arr1[42]= StanfordWord(4,18, QString("but"),QString("but"),CC, WORD, QVector<corefCoord>());
	arr1[43]= StanfordWord(4,19, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr1[44]= StanfordWord(4,20, QString("think"),QString("think"),VBP, WORD, QVector<corefCoord>());
	arr1[45]= StanfordWord(4,21, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr1[46]= StanfordWord(4,22, QString("need"),QString("need"),VB, WORD, QVector<corefCoord>());
	arr1[47]= StanfordWord(4,23, QString("a"),QString("a"),DT, WORD, QVector<corefCoord>());
	arr1[48]= StanfordWord(4,24, QString("holiday "),QString("holiday "),NN, WORD, QVector<corefCoord>());
	arr1[49]= StanfordWord(4,25, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr1[50]= StanfordWord(4,26, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr1[51]= StanfordWord(4,26, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());
	arr1[52]= StanfordWord(4,26, QString("</p>"),QString("</p>"),CC, MARK, QVector<corefCoord>());

	arr1[53]= StanfordWord(5,1, QString("<p>"),QString("<p>"),CC, MARK, QVector<corefCoord>());
	arr1[54]= StanfordWord(5,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr1[55]= StanfordWord(5,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr1[56]= StanfordWord(5,2, QString("Who"),QString("who"),WP, WORD, QVector<corefCoord>());
	arr1[57]= StanfordWord(5,3, QString("will"),QString("will"),MD, WORD, QVector<corefCoord>());
	arr1[58]= StanfordWord(5,4, QString("laugh"),QString("laugh"),NN, WORD, QVector<corefCoord>());
	arr1[59]= StanfordWord(5,5, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr1[60]= StanfordWord(5,6, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr1[61]= StanfordWord(5,7, QString("wonder"),QString("wonder"),VBP, WORD, QVector<corefCoord>());
	arr1[62]= StanfordWord(5,8, QString("?"),QString("?"),SentFinPunct, WORD, QVector<corefCoord>());
	arr1[63]= StanfordWord(5,9, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr1[64]= StanfordWord(5,9, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());

	arr1[65]= StanfordWord(6,1, QString("said"),QString("say"),VBD, WORD, QVector<corefCoord>());
	arr1[66]= StanfordWord(6,2, QString("Gandalf"),QString("Gandalf"),NNP, WORD, QVector<corefCoord>());
	arr1[67]= StanfordWord(6,3, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr1[68]= StanfordWord(6,4, QString("shaking"),QString("shake"),VBG, WORD, QVector<corefCoord>());
	arr1[69]= StanfordWord(6,5, QString("his"),QString("he"),PRPdollar, WORD, QVector<corefCoord>());
	arr1[70]= StanfordWord(6,6, QString("head"),QString("head"),NN, WORD, QVector<corefCoord>());
	arr1[71]= StanfordWord(6,7, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr1[72]= StanfordWord(6,7, QString("</p>"),QString("</p>"),CC, MARK, QVector<corefCoord>());

	arr1[73]= StanfordWord(7,1, QString("<p>"),QString("<p>"),CC, MARK, QVector<corefCoord>());
	arr1[74]= StanfordWord(7,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr1[75]= StanfordWord(7,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr1[76]= StanfordWord(7,2, QString("We"),QString("we"),PRP, WORD, QVector<corefCoord>());
	arr1[77]= StanfordWord(7,3, QString("shall"),QString("shall"),MD, WORD, QVector<corefCoord>());
	arr1[78]= StanfordWord(7,4, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr1[79]= StanfordWord(7,5, QString("old"),QString("old"),JJ, WORD, QVector<corefCoord>());
	arr1[80]= StanfordWord(7,6, QString("friend"),QString("friend"),NN, WORD, QVector<corefCoord>());
	arr1[81]= StanfordWord(7,7, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr1[82]= StanfordWord(7,8, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr1[83]= StanfordWord(7,8, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());

	QList<StanfordWord*> start1, res1;
	QMap<QString, StanfordWord*> Text1;
	for(int i = 0 ; i < wCount ; i++)
	{
		start1.push_back(arr1+i);
		res1.push_back(arr1+i);
		if(arr1[i].type==WORD)
		{
			Text1.insert(arr1[i].doKey(), arr1+i);
		}
	}

	res1.removeAt(0);
	res1.insert(0 , new StanfordWord(1,1, QString("<span style=\"color:\#ff0000\">"),QString("<span style=\"color:\#ff0000\">"),CC, MARK, QVector<corefCoord>()));

	res1.removeAt(15);
	res1.insert(15 , new StanfordWord(3,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>()));

	res1.removeAt(24);
	res1.insert(24 , new StanfordWord(4,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>()));

	res1.removeAt(54);
	res1.insert(54 , new StanfordWord(5,1, QString("<span style=\"color:\#ff0000\">"),QString("<span style=\"color:\#ff0000\">"),CC, MARK, QVector<corefCoord>()));

	res1.removeAt(74);
	res1.insert(74 , new StanfordWord(7,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>()));

	QSet  <person> allPersons1, speakersList1;

	QStringList objQualifFirstPer1;
	objQualifFirstPer1.push_back(QString("Bilbo"));
	objQualifFirstPer1.push_back(QString("hobbit"));

	QStringList subjQualifFirstPer1;
	subjQualifFirstPer1.push_back("old friend");

	QStringList objQualifSecondPer1;
	objQualifSecondPer1.push_back(QString("Gandalf"));
	objQualifSecondPer1.push_back(QString("wizard"));

	QStringList subjQualifSecondPer1;

	allPersons1.insert(person(QString("Bilbo"), objQualifFirstPer1, subjQualifFirstPer1, QString("00ff00")));
	allPersons1.insert(person(QString("Gandalf"), objQualifSecondPer1, subjQualifSecondPer1, QString("ff0000")));
	QListIterator<StanfordWord*> i1(start1);
	textTree* input1 = new textTree(i1);

	QListIterator<StanfordWord*> j1(res1);
	textTree* output1 = new textTree(j1);

	delete res1[0];
	delete res1[15];
	delete res1[24];
	delete res1[54];
	delete res1[74];
	QTest::newRow("TwoPersonsExactCharacterDefinition") << allPersons1 << Text1 << input1 << output1 << speakersList1 << arr1;
	
	/////////////////////////////////////���� 2
	/////////////////////////////////////TwoPersonsUndefinitPhrases

	wCount = 128;
	StanfordWord* arr2 = new StanfordWord[wCount];

	arr2[0]= StanfordWord(1,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr2[1]= StanfordWord(1,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr2[2]= StanfordWord(1,2, QString("How"),QString("how"),WRB, WORD, QVector<corefCoord>());
	arr2[3]= StanfordWord(1,3, QString("bright"),QString("bright"),JJ, WORD, QVector<corefCoord>());
	arr2[4]= StanfordWord(1,4, QString("your"),QString("you"),PRPdollar, WORD, QVector<corefCoord>());
	arr2[5]= StanfordWord(1,5, QString("garden"),QString("garden"),NN, WORD, QVector<corefCoord>());
	arr2[6]= StanfordWord(1,6, QString("looks"),QString("look"),VBZ, WORD, QVector<corefCoord>());
	arr2[7]= StanfordWord(1,7, QString("!"),QString("!"),SentFinPunct, WORD, QVector<corefCoord>());
	arr2[8]= StanfordWord(1,8, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr2[9]= StanfordWord(1,8, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());
	arr2[10]= StanfordWord(2,9, QString("said"),QString("say"),VBD, WORD, QVector<corefCoord>());
	arr2[11]= StanfordWord(2,10, QString("Gandalf"),QString("Gandalf"),NNP, WORD, QVector<corefCoord>());
	arr2[12]= StanfordWord(2,11, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr2[13]= StanfordWord(2,11, QString("</p>"),QString("</p>"),CC, MARK, QVector<corefCoord>());
	
	arr2[14]= StanfordWord(3,1, QString("<p>"),QString("<p>"),CC, MARK, QVector<corefCoord>());
	arr2[15]= StanfordWord(3,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr2[16]= StanfordWord(3,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr2[17]= StanfordWord(3,2, QString("Yes"),QString("yes"),UH, WORD, QVector<corefCoord>());
	arr2[18]= StanfordWord(3,3, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr2[19]= StanfordWord(3,4, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr2[20]= StanfordWord(3,4, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());
	arr2[21]= StanfordWord(3,5, QString("said"),QString("say"),VBD, WORD, QVector<corefCoord>());
	arr2[22]= StanfordWord(3,6, QString("Bilbo"),QString("Bilbo"),NNP, WORD, QVector<corefCoord>());
	arr2[23]= StanfordWord(3,7, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());

	arr2[24]= StanfordWord(4,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr2[25]= StanfordWord(4,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr2[26]= StanfordWord(4,2, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr2[27]= StanfordWord(4,3, QString("am"),QString("be"),VBP, WORD, QVector<corefCoord>());
	arr2[28]= StanfordWord(4,4, QString("very"),QString("very"),RB, WORD, QVector<corefCoord>());
	arr2[29]= StanfordWord(4,5, QString("fond"),QString("fond"),JJ, WORD, QVector<corefCoord>());
	arr2[30]= StanfordWord(4,6, QString("indeed"),QString("indeed"),RB, WORD, QVector<corefCoord>());
	arr2[31]= StanfordWord(4,7, QString("of"),QString("of"),IN, WORD, QVector<corefCoord>());
	arr2[32]= StanfordWord(4,8, QString("it"),QString("it"),PRP, WORD, QVector<corefCoord>());
	arr2[33]= StanfordWord(4,9, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr2[34]= StanfordWord(4,10, QString("and"),QString("and"),CC, WORD, QVector<corefCoord>());
	arr2[35]= StanfordWord(4,11, QString("of"),QString("of"),IN, WORD, QVector<corefCoord>());
	arr2[36]= StanfordWord(4,12, QString("all"),QString("all"),PDT, WORD, QVector<corefCoord>());
	arr2[37]= StanfordWord(4,13, QString("the"),QString("the"),DT, WORD, QVector<corefCoord>());
	arr2[38]= StanfordWord(4,14, QString("dear"),QString("dear"),RB, WORD, QVector<corefCoord>());
	arr2[39]= StanfordWord(4,15, QString("old"),QString("old"),JJ, WORD, QVector<corefCoord>());
	arr2[40]= StanfordWord(4,16, QString("Shire"),QString("Shire"),NNP, WORD, QVector<corefCoord>());
	arr2[41]= StanfordWord(4,17, QString(";"),QString(";"),Colon, WORD, QVector<corefCoord>());
	arr2[42]= StanfordWord(4,18, QString("but"),QString("but"),CC, WORD, QVector<corefCoord>());
	arr2[43]= StanfordWord(4,19, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr2[44]= StanfordWord(4,20, QString("think"),QString("think"),VBP, WORD, QVector<corefCoord>());
	arr2[45]= StanfordWord(4,21, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr2[46]= StanfordWord(4,22, QString("need"),QString("need"),VB, WORD, QVector<corefCoord>());
	arr2[47]= StanfordWord(4,23, QString("a"),QString("a"),DT, WORD, QVector<corefCoord>());
	arr2[48]= StanfordWord(4,24, QString("holiday "),QString("holiday "),NN, WORD, QVector<corefCoord>());
	arr2[49]= StanfordWord(4,25, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr2[50]= StanfordWord(4,26, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr2[51]= StanfordWord(4,26, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());
	arr2[52]= StanfordWord(4,26, QString("</p>"),QString("</p>"),CC, MARK, QVector<corefCoord>());

	arr2[53]= StanfordWord(5,1, QString("<p>"),QString("<p>"),CC, MARK, QVector<corefCoord>());
	arr2[54]= StanfordWord(5,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr2[55]= StanfordWord(5,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr2[56]= StanfordWord(5,2, QString("You"),QString("you"),PRP, WORD, QVector<corefCoord>());
	arr2[57]= StanfordWord(5,3, QString("mean"),QString("mean"),VBP, WORD, QVector<corefCoord>());
	arr2[58]= StanfordWord(5,4, QString("to"),QString("to"),TO, WORD, QVector<corefCoord>());
	arr2[59]= StanfordWord(5,5, QString("go"),QString("go"),VB, WORD, QVector<corefCoord>());
	arr2[60]= StanfordWord(5,6, QString("on"),QString("on"),IN, WORD, QVector<corefCoord>());
	arr2[61]= StanfordWord(5,7, QString("with"),QString("with"),IN, WORD, QVector<corefCoord>());
	arr2[62]= StanfordWord(5,8, QString("your"),QString("you"),PRPdollar, WORD, QVector<corefCoord>());
	arr2[63]= StanfordWord(5,9, QString("plan"),QString("plan"),NN, WORD, QVector<corefCoord>());
	arr2[64]= StanfordWord(5,10, QString("then"),QString("then"),RB, WORD, QVector<corefCoord>());
	arr2[65]= StanfordWord(5,11, QString("?"),QString("?"),SentFinPunct, WORD, QVector<corefCoord>());
	arr2[66]= StanfordWord(5,12, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr2[67]= StanfordWord(5,12, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());
	arr2[68]= StanfordWord(5,12, QString("</p>"),QString("</p>"),CC, MARK, QVector<corefCoord>());
	
	arr2[69]= StanfordWord(6,1, QString("<p>"),QString("<p>"),CC, MARK, QVector<corefCoord>());
	arr2[70]= StanfordWord(6,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr2[71]= StanfordWord(6,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr2[72]= StanfordWord(6,2, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr2[73]= StanfordWord(6,3, QString("do"),QString("do"),VBP, WORD, QVector<corefCoord>());
	arr2[74]= StanfordWord(6,4, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr2[75]= StanfordWord(6,5, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr2[76]= StanfordWord(6,5, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());
	arr2[77]= StanfordWord(6,5, QString("</p>"),QString("</p>"),CC, MARK, QVector<corefCoord>());
	
	arr2[78]= StanfordWord(7,1, QString("<p>"),QString("<p>"),CC, MARK, QVector<corefCoord>());
	arr2[79]= StanfordWord(7,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr2[80]= StanfordWord(7,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr2[81]= StanfordWord(7,2, QString("Very"),QString("very"),RB, WORD, QVector<corefCoord>());
	arr2[82]= StanfordWord(7,3, QString("well"),QString("well"),RB, WORD, QVector<corefCoord>());
	arr2[83]= StanfordWord(7,4, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr2[84]= StanfordWord(7,5, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr2[85]= StanfordWord(7,5, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());
	arr2[86]= StanfordWord(7,5, QString("</p>"),QString("</p>"),CC, MARK, QVector<corefCoord>());
	
	arr2[87]= StanfordWord(8,1, QString("<p>"),QString("<p>"),CC, MARK, QVector<corefCoord>());
	arr2[88]= StanfordWord(8,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr2[89]= StanfordWord(8,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr2[90]= StanfordWord(8,2, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr2[91]= StanfordWord(8,3, QString("hope"),QString("hope"),VBP, WORD, QVector<corefCoord>());
	arr2[92]= StanfordWord(8,4, QString("so"),QString("so"),RB, WORD, QVector<corefCoord>());
	arr2[93]= StanfordWord(8,5, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr2[94]= StanfordWord(8,6, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr2[95]= StanfordWord(8,6, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());
	arr2[96]= StanfordWord(8,6, QString("</p>"),QString("</p>"),CC, MARK, QVector<corefCoord>());

	arr2[97]= StanfordWord(9,1, QString("<p>"),QString("<p>"),CC, MARK, QVector<corefCoord>());
	arr2[98]= StanfordWord(9,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr2[99]= StanfordWord(9,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr2[100]= StanfordWord(9,2, QString("Who"),QString("who"),WP, WORD, QVector<corefCoord>());
	arr2[101]= StanfordWord(9,3, QString("will"),QString("will"),MD, WORD, QVector<corefCoord>());
	arr2[102]= StanfordWord(9,4, QString("laugh"),QString("laugh"),NN, WORD, QVector<corefCoord>());
	arr2[103]= StanfordWord(9,5, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr2[104]= StanfordWord(9,6, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr2[105]= StanfordWord(9,7, QString("wonder"),QString("wonder"),VBP, WORD, QVector<corefCoord>());
	arr2[106]= StanfordWord(9,8, QString("?"),QString("?"),SentFinPunct, WORD, QVector<corefCoord>());
	arr2[107]= StanfordWord(9,9, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr2[108]= StanfordWord(9,9, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());

	arr2[109]= StanfordWord(10,1, QString("said"),QString("say"),VBD, WORD, QVector<corefCoord>());
	arr2[110]= StanfordWord(10,2, QString("Gandalf"),QString("Gandalf"),NNP, WORD, QVector<corefCoord>());
	arr2[111]= StanfordWord(10,3, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr2[112]= StanfordWord(10,4, QString("shaking"),QString("shake"),VBG, WORD, QVector<corefCoord>());
	arr2[113]= StanfordWord(10,5, QString("his"),QString("he"),PRPdollar, WORD, QVector<corefCoord>());
	arr2[114]= StanfordWord(10,6, QString("head"),QString("head"),NN, WORD, QVector<corefCoord>());
	arr2[115]= StanfordWord(10,7, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr2[116]= StanfordWord(10,7, QString("</p>"),QString("</p>"),CC, MARK, QVector<corefCoord>());

	arr2[117]= StanfordWord(11,1, QString("<p>"),QString("<p>"),CC, MARK, QVector<corefCoord>());
	arr2[118]= StanfordWord(11,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr2[119]= StanfordWord(11,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr2[120]= StanfordWord(11,2, QString("We"),QString("we"),PRP, WORD, QVector<corefCoord>());
	arr2[121]= StanfordWord(11,3, QString("shall"),QString("shall"),MD, WORD, QVector<corefCoord>());
	arr2[122]= StanfordWord(11,4, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr2[123]= StanfordWord(11,5, QString("old"),QString("old"),JJ, WORD, QVector<corefCoord>());
	arr2[124]= StanfordWord(11,6, QString("friend"),QString("friend"),NN, WORD, QVector<corefCoord>());
	arr2[125]= StanfordWord(11,7, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr2[126]= StanfordWord(11,8, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr2[127]= StanfordWord(11,8, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());

	QList<StanfordWord*> start2, res2;
	QMap<QString, StanfordWord*> Text2;
	for(int i = 0 ; i < wCount ; i++)
	{
		start2.push_back(arr2+i);
		res2.push_back(arr2+i);
		if(arr2[i].type==WORD)
		{
			Text2.insert(arr2[i].doKey(), arr2+i);
		}
	}

	res2.removeAt(0);
	res2.insert(0 , new StanfordWord(1,1, QString("<span style=\"color:\#ff0000\">"),QString("<span style=\"color:\#ff0000\">"),CC, MARK, QVector<corefCoord>()));

	res2.removeAt(15);
	res2.insert(15 , new StanfordWord(3,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>()));

	res2.removeAt(24);
	res2.insert(24 , new StanfordWord(4,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>()));

	res2.removeAt(98);
	res2.insert(98 , new StanfordWord(9,1, QString("<span style=\"color:\#ff0000\">"),QString("<span style=\"color:\#ff0000\">"),CC, MARK, QVector<corefCoord>()));

	res2.removeAt(118);
	res2.insert(118 , new StanfordWord(11,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>()));

	QSet  <person> allPersons2, speakersList2;

	QStringList objQualifFirstPer2;
	objQualifFirstPer2.push_back(QString("Bilbo"));
	objQualifFirstPer2.push_back(QString("hobbit"));

	QStringList subjQualifFirstPer2;
	subjQualifFirstPer2.push_back("old friend");

	QStringList objQualifSecondPer2;
	objQualifSecondPer2.push_back(QString("Gandalf"));
	objQualifSecondPer2.push_back(QString("wizard"));

	QStringList subjQualifSecondPer2;

	allPersons2.insert(person(QString("Bilbo"), objQualifFirstPer2, subjQualifFirstPer2, QString("00ff00")));
	allPersons2.insert(person(QString("Gandalf"), objQualifSecondPer2, subjQualifSecondPer2, QString("ff0000")));
	QListIterator<StanfordWord*> i2(start2);
	textTree* input2 = new textTree(i2);

	QListIterator<StanfordWord*> j2(res2);
	textTree* output2 = new textTree(j2);

	delete res2[0];
	delete res2[15];
	delete res2[24];
	delete res2[98];
	delete res2[118];
	QTest::newRow("TwoPersonsUndefinitPhrases") << allPersons2 << Text2 << input2 << output2 << speakersList2 << arr2;

	/////////////////////////////////////���� 3
	/////////////////////////////////////TwoPersonsUnexactCharacterDefinition

	wCount = 84;
	StanfordWord* arr3 = new StanfordWord[wCount];

	arr3[0]= StanfordWord(1,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr3[1]= StanfordWord(1,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr3[2]= StanfordWord(1,2, QString("How"),QString("how"),WRB, WORD, QVector<corefCoord>());
	arr3[3]= StanfordWord(1,3, QString("bright"),QString("bright"),JJ, WORD, QVector<corefCoord>());
	arr3[4]= StanfordWord(1,4, QString("your"),QString("you"),PRPdollar, WORD, QVector<corefCoord>());
	arr3[5]= StanfordWord(1,5, QString("garden"),QString("garden"),NN, WORD, QVector<corefCoord>());
	arr3[6]= StanfordWord(1,6, QString("looks"),QString("look"),VBZ, WORD, QVector<corefCoord>());
	arr3[7]= StanfordWord(1,7, QString("!"),QString("!"),SentFinPunct, WORD, QVector<corefCoord>());
	arr3[8]= StanfordWord(1,8, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr3[9]= StanfordWord(1,8, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());
	arr3[10]= StanfordWord(2,1, QString("said"),QString("say"),VBD, WORD, QVector<corefCoord>());
	arr3[11]= StanfordWord(2,2, QString("Gandalf"),QString("Gandalf"),NNP, WORD, QVector<corefCoord>());
	arr3[12]= StanfordWord(2,3, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr3[13]= StanfordWord(2,3, QString("</p>"),QString("</p>"),CC, MARK, QVector<corefCoord>());
	
	arr3[14]= StanfordWord(3,1, QString("<p>"),QString("<p>"),CC, MARK, QVector<corefCoord>());
	arr3[15]= StanfordWord(3,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr3[16]= StanfordWord(3,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr3[17]= StanfordWord(3,2, QString("Yes"),QString("yes"),UH, WORD, QVector<corefCoord>());
	arr3[18]= StanfordWord(3,3, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr3[19]= StanfordWord(3,4, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr3[20]= StanfordWord(3,4, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());
	arr3[21]= StanfordWord(3,5, QString("said"),QString("say"),VBD, WORD, QVector<corefCoord>());
	arr3[22]= StanfordWord(3,6, QString("Bilbo"),QString("Bilbo"),NNP, WORD, QVector<corefCoord>());
	arr3[23]= StanfordWord(3,7, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());

	arr3[24]= StanfordWord(4,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr3[25]= StanfordWord(4,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr3[26]= StanfordWord(4,2, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr3[27]= StanfordWord(4,3, QString("am"),QString("be"),VBP, WORD, QVector<corefCoord>());
	arr3[28]= StanfordWord(4,4, QString("very"),QString("very"),RB, WORD, QVector<corefCoord>());
	arr3[29]= StanfordWord(4,5, QString("fond"),QString("fond"),JJ, WORD, QVector<corefCoord>());
	arr3[30]= StanfordWord(4,6, QString("indeed"),QString("indeed"),RB, WORD, QVector<corefCoord>());
	arr3[31]= StanfordWord(4,7, QString("of"),QString("of"),IN, WORD, QVector<corefCoord>());
	arr3[32]= StanfordWord(4,8, QString("it"),QString("it"),PRP, WORD, QVector<corefCoord>());
	arr3[33]= StanfordWord(4,9, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr3[34]= StanfordWord(4,10, QString("and"),QString("and"),CC, WORD, QVector<corefCoord>());
	arr3[35]= StanfordWord(4,11, QString("of"),QString("of"),IN, WORD, QVector<corefCoord>());
	arr3[36]= StanfordWord(4,12, QString("all"),QString("all"),PDT, WORD, QVector<corefCoord>());
	arr3[37]= StanfordWord(4,13, QString("the"),QString("the"),DT, WORD, QVector<corefCoord>());
	arr3[38]= StanfordWord(4,14, QString("dear"),QString("dear"),RB, WORD, QVector<corefCoord>());
	arr3[39]= StanfordWord(4,15, QString("old"),QString("old"),JJ, WORD, QVector<corefCoord>());
	arr3[40]= StanfordWord(4,16, QString("Shire"),QString("Shire"),NNP, WORD, QVector<corefCoord>());
	arr3[41]= StanfordWord(4,17, QString(";"),QString(";"),Colon, WORD, QVector<corefCoord>());
	arr3[42]= StanfordWord(4,18, QString("but"),QString("but"),CC, WORD, QVector<corefCoord>());
	arr3[43]= StanfordWord(4,19, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr3[44]= StanfordWord(4,20, QString("think"),QString("think"),VBP, WORD, QVector<corefCoord>());
	arr3[45]= StanfordWord(4,21, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr3[46]= StanfordWord(4,22, QString("need"),QString("need"),VB, WORD, QVector<corefCoord>());
	arr3[47]= StanfordWord(4,23, QString("a"),QString("a"),DT, WORD, QVector<corefCoord>());
	arr3[48]= StanfordWord(4,24, QString("holiday "),QString("holiday "),NN, WORD, QVector<corefCoord>());
	arr3[49]= StanfordWord(4,25, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr3[50]= StanfordWord(4,26, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr3[51]= StanfordWord(4,26, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());
	arr3[52]= StanfordWord(4,26, QString("</p>"),QString("</p>"),CC, MARK, QVector<corefCoord>());

	arr3[53]= StanfordWord(5,1, QString("<p>"),QString("<p>"),CC, MARK, QVector<corefCoord>());
	arr3[54]= StanfordWord(5,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr3[55]= StanfordWord(5,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr3[56]= StanfordWord(5,2, QString("Who"),QString("who"),WP, WORD, QVector<corefCoord>());
	arr3[57]= StanfordWord(5,3, QString("will"),QString("will"),MD, WORD, QVector<corefCoord>());
	arr3[58]= StanfordWord(5,4, QString("laugh"),QString("laugh"),NN, WORD, QVector<corefCoord>());
	arr3[59]= StanfordWord(5,5, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr3[60]= StanfordWord(5,6, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr3[61]= StanfordWord(5,7, QString("wonder"),QString("wonder"),VBP, WORD, QVector<corefCoord>());
	arr3[62]= StanfordWord(5,8, QString("?"),QString("?"),SentFinPunct, WORD, QVector<corefCoord>());
	arr3[63]= StanfordWord(5,9, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr3[64]= StanfordWord(5,9, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());

	arr3[65]= StanfordWord(6,1, QString("said"),QString("say"),VBD, WORD, QVector<corefCoord>());
	arr3[66]= StanfordWord(6,2, QString("Gandalf"),QString("Gandalf"),NNP, WORD, QVector<corefCoord>());
	arr3[67]= StanfordWord(6,3, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr3[68]= StanfordWord(6,4, QString("shaking"),QString("shake"),VBG, WORD, QVector<corefCoord>());
	arr3[69]= StanfordWord(6,5, QString("his"),QString("he"),PRPdollar, WORD, QVector<corefCoord>());
	arr3[70]= StanfordWord(6,6, QString("head"),QString("head"),NN, WORD, QVector<corefCoord>());
	arr3[71]= StanfordWord(6,7, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr3[72]= StanfordWord(6,7, QString("</p>"),QString("</p>"),CC, MARK, QVector<corefCoord>());

	arr3[73]= StanfordWord(7,1, QString("<p>"),QString("<p>"),CC, MARK, QVector<corefCoord>());
	arr3[74]= StanfordWord(7,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr3[75]= StanfordWord(7,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr3[76]= StanfordWord(7,2, QString("We"),QString("we"),PRP, WORD, QVector<corefCoord>());
	arr3[77]= StanfordWord(7,3, QString("shall"),QString("shall"),MD, WORD, QVector<corefCoord>());
	arr3[78]= StanfordWord(7,4, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr3[79]= StanfordWord(7,5, QString("old"),QString("old"),JJ, WORD, QVector<corefCoord>());
	arr3[80]= StanfordWord(7,6, QString("friend"),QString("friend"),NN, WORD, QVector<corefCoord>());
	arr3[81]= StanfordWord(7,7, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr3[82]= StanfordWord(7,8, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr3[83]= StanfordWord(7,8, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());

	QList<StanfordWord*> start3, res3;
	QMap<QString, StanfordWord*> Text3;
	for(int i = 0 ; i < wCount ; i++)
	{
		start3.push_back(arr3+i);
		res3.push_back(arr3+i);
		if(arr3[i].type==WORD)
		{
			Text3.insert(arr3[i].doKey(), arr3+i);
		}
	}

	res3.removeAt(0);
	res3.insert(0 , new StanfordWord(1,1, QString("<span style=\"color:\#ff0000\">"),QString("<span style=\"color:\#ff0000\">"),CC, MARK, QVector<corefCoord>()));

	res3.removeAt(15);
	res3.insert(15 , new StanfordWord(3,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>()));

	res3.removeAt(24);
	res3.insert(24 , new StanfordWord(4,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>()));

	res3.removeAt(54);
	res3.insert(54 , new StanfordWord(5,1, QString("<span style=\"color:\#ff0000\">"),QString("<span style=\"color:\#ff0000\">"),CC, MARK, QVector<corefCoord>()));

	res3.removeAt(74);
	res3.insert(74 , new StanfordWord(7,1, QString("<u><span style=\"color:\#0000ff\">"),QString("<u><span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>()));

	res3.removeAt(83);
	res3.insert(83 , new StanfordWord(7,8, QString("</span></u>(Bilbo, Frodo)"),QString("</span></u>(Bilbo, Frodo)"),CC, MARK, QVector<corefCoord>()));

	QSet  <person> allPersons3, speakersList3;

	QStringList objQualifFirstPer3;
	objQualifFirstPer3.push_back(QString("Bilbo"));
	objQualifFirstPer3.push_back(QString("hobbit"));

	QStringList subjQualifFirstPer3;
	subjQualifFirstPer3.push_back("old friend");

	QStringList objQualifSecondPer3;
	objQualifSecondPer3.push_back(QString("Gandalf"));
	objQualifSecondPer3.push_back(QString("wizard"));

	QStringList subjQualifSecondPer3;

	QStringList objQualifTherdPer3;
	objQualifTherdPer3.push_back(QString("Frodo"));
	objQualifTherdPer3.push_back(QString("hobbit"));

	QStringList subjQualifTherdPer3;
	subjQualifTherdPer3.push_back("old friend");

	allPersons3.insert(person(QString("Bilbo"), objQualifFirstPer3, subjQualifFirstPer3, QString("00ff00")));
	allPersons3.insert(person(QString("Gandalf"), objQualifSecondPer3, subjQualifSecondPer3, QString("ff0000")));
	allPersons3.insert(person(QString("Frodo"), objQualifTherdPer3, subjQualifTherdPer3, QString("f0000f")));

	QListIterator<StanfordWord*> i3(start3);
	textTree* input3 = new textTree(i3);

	QListIterator<StanfordWord*> j3(res3);
	textTree* output3 = new textTree(j3);

	delete res3[0];
	delete res3[15];
	delete res3[24];
	delete res3[54];
	delete res3[74];
	delete res3[83];
	QTest::newRow("TwoPersonsUnexactCharacterDefinition") << allPersons3 << Text3 << input3 << output3 << speakersList3 << arr3;

	/////////////////////////////////////���� 4
	/////////////////////////////////////ThreeCharactersTestReturnedValueTwoCharacters

	wCount = 10;
	StanfordWord* arr4 = new StanfordWord[wCount];

	arr4[0]= StanfordWord(1,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr4[1]= StanfordWord(1,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr4[2]= StanfordWord(1,2, QString("We"),QString("we"),PRP, WORD, QVector<corefCoord>());
	arr4[3]= StanfordWord(1,3, QString("shall"),QString("shall"),MD, WORD, QVector<corefCoord>());
	arr4[4]= StanfordWord(1,4, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr4[5]= StanfordWord(1,5, QString("old"),QString("old"),JJ, WORD, QVector<corefCoord>());
	arr4[6]= StanfordWord(1,6, QString("friend"),QString("friend"),NN, WORD, QVector<corefCoord>());
	arr4[7]= StanfordWord(1,7, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr4[8]= StanfordWord(1,8, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr4[9]= StanfordWord(1,8, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());

	QList<StanfordWord*> start4, res4;
	QMap<QString, StanfordWord*> Text4;
	for(int i = 0 ; i < wCount ; i++)
	{
		start4.push_back(arr4+i);
		res4.push_back(arr4+i);
		if(arr4[i].type==WORD)
		{
			Text4.insert(arr4[i].doKey(), arr4+i);
		}
	}

	QSet  <person> allPersons4, speakersList4;

	QStringList objQualifFirstPer4;
	objQualifFirstPer4.push_back(QString("Bilbo"));
	objQualifFirstPer4.push_back(QString("hobbit"));

	QStringList subjQualifFirstPer4;
	subjQualifFirstPer4.push_back("old friend");

	QStringList objQualifSecondPer4;
	objQualifSecondPer4.push_back(QString("Gandalf"));
	objQualifSecondPer4.push_back(QString("wizard"));

	QStringList subjQualifSecondPer4;

	QStringList objQualifTherdPer4;
	objQualifTherdPer4.push_back(QString("Frodo"));
	objQualifTherdPer4.push_back(QString("hobbit"));

	QStringList subjQualifTherdPer4;
	subjQualifTherdPer4.push_back("old friend");

	allPersons4.insert(person(QString("Bilbo"), objQualifFirstPer4, subjQualifFirstPer4, QString("00ff00")));
	allPersons4.insert(person(QString("Gandalf"), objQualifSecondPer4, subjQualifSecondPer4, QString("ff0000")));
	allPersons4.insert(person(QString("Frodo"), objQualifTherdPer4, subjQualifTherdPer4, QString("f0000f")));

	speakersList4.insert(person(QString("Bilbo"), objQualifFirstPer4, subjQualifFirstPer4, QString("00ff00")));
	speakersList4.insert(person(QString("Frodo"), objQualifTherdPer4, subjQualifTherdPer4, QString("f0000f")));

	QListIterator<StanfordWord*> i4(start4);
	textTree* input4 = new textTree(i4);

	QListIterator<StanfordWord*> j4(res4);
	textTree* output4 = new textTree(j4);

	QTest::newRow("ThreeCharactersTestReturnedValueTwoCharacters") << allPersons4 << Text4 << input4->mainText[0].childList[0] << output4->mainText[0].childList[0]<< speakersList4 << arr4;

	/////////////////////////////////////���� 5
	/////////////////////////////////////TwoCharactersTestReturnedValueTwoCharacters

	wCount = 10;
	StanfordWord* arr5 = new StanfordWord[wCount];

	arr5[0]= StanfordWord(1,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr5[1]= StanfordWord(1,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr5[2]= StanfordWord(1,2, QString("We"),QString("we"),PRP, WORD, QVector<corefCoord>());
	arr5[3]= StanfordWord(1,3, QString("shall"),QString("shall"),MD, WORD, QVector<corefCoord>());
	arr5[4]= StanfordWord(1,4, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr5[5]= StanfordWord(1,5, QString("old"),QString("old"),JJ, WORD, QVector<corefCoord>());
	arr5[6]= StanfordWord(1,6, QString("friend"),QString("friend"),NN, WORD, QVector<corefCoord>());
	arr5[7]= StanfordWord(1,7, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr5[8]= StanfordWord(1,8, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr5[9]= StanfordWord(1,8, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());

	QList<StanfordWord*> start5, res5;
	QMap<QString, StanfordWord*> Text5;
	for(int i = 0 ; i < wCount ; i++)
	{
		start5.push_back(arr5+i);
		res5.push_back(arr5+i);
		if(arr5[i].type==WORD)
		{
			Text5.insert(arr5[i].doKey(), arr5+i);
		}
	}

	QSet  <person> allPersons5, speakersList5;

	QStringList objQualifFirstPer5;
	objQualifFirstPer5.push_back(QString("Bilbo"));
	objQualifFirstPer5.push_back(QString("hobbit"));

	QStringList subjQualifFirstPer5;
	subjQualifFirstPer5.push_back("old friend");

	QStringList objQualifTherdPer5;
	objQualifTherdPer5.push_back(QString("Frodo"));
	objQualifTherdPer5.push_back(QString("hobbit"));

	QStringList subjQualifTherdPer5;
	subjQualifTherdPer5.push_back("old friend");

	allPersons5.insert(person(QString("Bilbo"), objQualifFirstPer5, subjQualifFirstPer5, QString("00ff00")));
	allPersons5.insert(person(QString("Frodo"), objQualifTherdPer5, subjQualifTherdPer5, QString("f0000f")));

	speakersList5.insert(person(QString("Bilbo"), objQualifFirstPer5, subjQualifFirstPer5, QString("00ff00")));
	speakersList5.insert(person(QString("Frodo"), objQualifTherdPer5, subjQualifTherdPer5, QString("f0000f")));

	QListIterator<StanfordWord*> i5(start5);
	textTree* input5 = new textTree(i5);

	QListIterator<StanfordWord*> j5(res5);
	textTree* output5 = new textTree(j5);

	QTest::newRow("TwoCharactersTestReturnedValueTwoCharacters") << allPersons5 << Text5 << input5->mainText[0].childList[0] << output5->mainText[0].childList[0]<< speakersList5 << arr5;

	/////////////////////////////////////���� 6
	/////////////////////////////////////TwoCharactersTestReturnedValueOneCharacter

	wCount = 10;
	StanfordWord* arr6 = new StanfordWord[wCount];

	arr6[0]= StanfordWord(1,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr6[1]= StanfordWord(1,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr6[2]= StanfordWord(1,2, QString("We"),QString("we"),PRP, WORD, QVector<corefCoord>());
	arr6[3]= StanfordWord(1,3, QString("shall"),QString("shall"),MD, WORD, QVector<corefCoord>());
	arr6[4]= StanfordWord(1,4, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr6[5]= StanfordWord(1,5, QString("old"),QString("old"),JJ, WORD, QVector<corefCoord>());
	arr6[6]= StanfordWord(1,6, QString("friend"),QString("friend"),NN, WORD, QVector<corefCoord>());
	arr6[7]= StanfordWord(1,7, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr6[8]= StanfordWord(1,8, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr6[9]= StanfordWord(1,8, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());

	QList<StanfordWord*> start6, res6;
	QMap<QString, StanfordWord*> Text6;
	for(int i = 0 ; i < wCount ; i++)
	{
		start6.push_back(arr6+i);
		res6.push_back(arr6+i);
		if(arr6[i].type==WORD)
		{
			Text6.insert(arr6[i].doKey(), arr6+i);
		}
	}

	QSet  <person> allPersons6, speakersList6;

	QStringList objQualifFirstPer6;
	objQualifFirstPer6.push_back(QString("Bilbo"));
	objQualifFirstPer6.push_back(QString("hobbit"));

	QStringList subjQualifFirstPer6;
	subjQualifFirstPer6.push_back("old friend");

	QStringList objQualifSecondPer6;
	objQualifSecondPer6.push_back(QString("Gandalf"));
	objQualifSecondPer6.push_back(QString("wizard"));

	QStringList subjQualifSecondPer6;

	allPersons6.insert(person(QString("Bilbo"), objQualifFirstPer6, subjQualifFirstPer6, QString("00ff00")));
	allPersons6.insert(person(QString("Gandalf"), objQualifSecondPer6, subjQualifSecondPer6, QString("ff0000")));

	speakersList6.insert(person(QString("Bilbo"), objQualifFirstPer6, subjQualifFirstPer6, QString("00ff00")));


	QListIterator<StanfordWord*> i6(start6);
	textTree* input6 = new textTree(i6);

	QListIterator<StanfordWord*> j6(res6);
	textTree* output6 = new textTree(j6);

	QTest::newRow("TwoCharactersTestReturnedValueOneCharacter") << allPersons6 << Text6 << input6->mainText[0].childList[0] << output6->mainText[0].childList[0]<< speakersList6 << arr6;

	/////////////////////////////////////���� 7
	/////////////////////////////////////HightLevelNarrative

	wCount = 10;
	StanfordWord* arr7 = new StanfordWord[wCount];

	arr7[0]= StanfordWord(1,1, QString("Bilbo"),QString("Bilbo"),NNP, WORD, QVector<corefCoord>());
	arr7[1]= StanfordWord(1,1, QString("and"),QString("and"),CC, WORD, QVector<corefCoord>());
	arr7[2]= StanfordWord(1,2, QString("Gandalf"),QString("Gandalf"),PRP, WORD, QVector<corefCoord>());
	arr7[3]= StanfordWord(1,3, QString("are"),QString("be"),VBP, WORD, QVector<corefCoord>());
	arr7[4]= StanfordWord(1,4, QString("sitting"),QString("sit"),VBG, WORD, QVector<corefCoord>());
	arr7[5]= StanfordWord(1,5, QString("by"),QString("by"),DT, WORD, QVector<corefCoord>());
	arr7[6]= StanfordWord(1,6, QString("the"),QString("the"),DT, WORD, QVector<corefCoord>());
	arr7[7]= StanfordWord(1,7, QString("open"),QString("open"),JJ, WORD, QVector<corefCoord>());
	arr7[8]= StanfordWord(1,8, QString("window"),QString("window"),NN, WORD, QVector<corefCoord>());
	arr7[9]= StanfordWord(1,7, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());

	QList<StanfordWord*> start7, res7;
	QMap<QString, StanfordWord*> Text7;
	for(int i = 0 ; i < wCount ; i++)
	{
		start7.push_back(arr7+i);
		res7.push_back(arr7+i);
		if(arr7[i].type==WORD)
		{
			Text7.insert(arr7[i].doKey(), arr7+i);
		}
	}

	QSet  <person> allPersons7, speakersList7;

	QStringList objQualifFirstPer7;
	objQualifFirstPer7.push_back(QString("Bilbo"));
	objQualifFirstPer7.push_back(QString("hobbit"));

	QStringList subjQualifFirstPer7;
	subjQualifFirstPer7.push_back("old friend");

	QStringList objQualifSecondPer7;
	objQualifSecondPer7.push_back(QString("Gandalf"));
	objQualifSecondPer7.push_back(QString("wizard"));

	QStringList subjQualifSecondPer7;

	allPersons7.insert(person(QString("Bilbo"), objQualifFirstPer7, subjQualifFirstPer7, QString("00ff00")));
	allPersons7.insert(person(QString("Gandalf"), objQualifSecondPer7, subjQualifSecondPer7, QString("ff0000")));

	QListIterator<StanfordWord*> i7(start7);
	textTree* input7 = new textTree(i7);

	QListIterator<StanfordWord*> j7(res7);
	textTree* output7 = new textTree(j7);

	QTest::newRow("TwoCharactersTestReturnedValueOneCharacter") << allPersons7 << Text7 << input7 << output7 << speakersList7 << arr7;


	/////////////////////////////////////���� 8
	/////////////////////////////////////TwoAppealsOneParagraphOneSentence

	wCount = 12;
	StanfordWord* arr8 = new StanfordWord[wCount];

	arr8[0]= StanfordWord(1,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr8[1]= StanfordWord(1,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr8[2]= StanfordWord(1,2, QString("We"),QString("we"),PRP, WORD, QVector<corefCoord>());
	arr8[3]= StanfordWord(1,3, QString("shall"),QString("shall"),MD, WORD, QVector<corefCoord>());
	arr8[4]= StanfordWord(1,4, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr8[5]= StanfordWord(1,5, QString("Gandalf"),QString("Gandalf"),NNP, WORD, QVector<corefCoord>());
	arr8[6]= StanfordWord(1,6, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr8[7]= StanfordWord(1,7, QString("old"),QString("old"),JJ, WORD, QVector<corefCoord>());
	arr8[8]= StanfordWord(1,8, QString("friend"),QString("friend"),NN, WORD, QVector<corefCoord>());
	arr8[9]= StanfordWord(1,9, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr8[10]= StanfordWord(1,10, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr8[11]= StanfordWord(1,10, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());

	QList<StanfordWord*> start8, res8;
	QMap<QString, StanfordWord*> Text8;
	for(int i = 0 ; i < wCount ; i++)
	{
		start8.push_back(arr8+i);
		res8.push_back(arr8+i);
		if(arr8[i].type==WORD)
		{
			Text8.insert(arr8[i].doKey(), arr8+i);
		}
	}

	res8.removeAt(0);
	res8.insert(0, new StanfordWord(1,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>()));

	QSet  <person> allPersons8, speakersList8;

	QStringList objQualifFirstPer8;
	objQualifFirstPer8.push_back(QString("Bilbo"));
	objQualifFirstPer8.push_back(QString("hobbit"));

	QStringList subjQualifFirstPer8;
	subjQualifFirstPer8.push_back("old friend");

	QStringList objQualifSecondPer8;
	objQualifSecondPer8.push_back(QString("Gandalf"));
	objQualifSecondPer8.push_back(QString("wizard"));

	QStringList subjQualifSecondPer8;
	subjQualifSecondPer8.push_back("old friend");

	QStringList objQualifTherdPer8;
	objQualifTherdPer8.push_back(QString("Frodo"));
	objQualifTherdPer8.push_back(QString("hobbit"));

	QStringList subjQualifTherdPer8;

	allPersons8.insert(person(QString("Bilbo"), objQualifFirstPer8, subjQualifFirstPer8, QString("00ff00")));
	allPersons8.insert(person(QString("Gandalf"), objQualifSecondPer8, subjQualifSecondPer8, QString("ff0000")));
	allPersons8.insert(person(QString("Frodo"), objQualifTherdPer8, subjQualifTherdPer8, QString("f0000f")));

	QListIterator<StanfordWord*> i8(start8);
	textTree* input8 = new textTree(i8);

	QListIterator<StanfordWord*> j8(res8);
	textTree* output8 = new textTree(j8);

	delete res8[0];
	QTest::newRow("TwoAppealsOneParagraphOneSentence") << allPersons8 << Text8 << input8<< output8<< speakersList8 << arr8;

	/////////////////////////////////////���� 9
	/////////////////////////////////////TwoAppealsOneParagraphDifferentSentence

	wCount = 32;
	StanfordWord* arr9 = new StanfordWord[wCount];

	arr9[0]= StanfordWord(1,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr9[1]= StanfordWord(1,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr9[2]= StanfordWord(1,2, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr9[3]= StanfordWord(1,3, QString("am"),QString("be"),VBP, WORD, QVector<corefCoord>());
	arr9[4]= StanfordWord(1,4, QString("very"),QString("very"),RB, WORD, QVector<corefCoord>());
	arr9[5]= StanfordWord(1,5, QString("fond"),QString("fond"),JJ, WORD, QVector<corefCoord>());
	arr9[6]= StanfordWord(1,6, QString("indeed"),QString("indeed"),RB, WORD, QVector<corefCoord>());
	arr9[7]= StanfordWord(1,7, QString("of"),QString("of"),IN, WORD, QVector<corefCoord>());
	arr9[8]= StanfordWord(1,8, QString("it"),QString("it"),PRP, WORD, QVector<corefCoord>());
	arr9[9]= StanfordWord(1,9, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr9[10]= StanfordWord(1,10, QString("and"),QString("and"),CC, WORD, QVector<corefCoord>());
	arr9[11]= StanfordWord(1,11, QString("of"),QString("of"),IN, WORD, QVector<corefCoord>());
	arr9[12]= StanfordWord(1,12, QString("all"),QString("all"),PDT, WORD, QVector<corefCoord>());
	arr9[13]= StanfordWord(1,13, QString("the"),QString("the"),DT, WORD, QVector<corefCoord>());
	arr9[14]= StanfordWord(1,14, QString("dear"),QString("dear"),RB, WORD, QVector<corefCoord>());
	arr9[15]= StanfordWord(1,15, QString("old"),QString("old"),JJ, WORD, QVector<corefCoord>());
	arr9[16]= StanfordWord(1,16, QString("Shire"),QString("Shire"),NNP, WORD, QVector<corefCoord>());
	arr9[17]= StanfordWord(1,17, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr9[18]= StanfordWord(1,18, QString("Gandalf"),QString("Gandalf"),NNP, WORD, QVector<corefCoord>());
	arr9[19]= StanfordWord(1,19, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());

	arr9[20]= StanfordWord(2,1, QString("Old"),QString("old"),JJ, WORD, QVector<corefCoord>());
	arr9[21]= StanfordWord(2,2, QString("friend"),QString("friend"),NN, WORD, QVector<corefCoord>());
	arr9[22]= StanfordWord(2,3, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr9[23]= StanfordWord(2,4, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr9[24]= StanfordWord(2,5, QString("think"),QString("think"),VBP, WORD, QVector<corefCoord>());
	arr9[25]= StanfordWord(2,6, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr9[26]= StanfordWord(2,7, QString("need"),QString("need"),VB, WORD, QVector<corefCoord>());
	arr9[27]= StanfordWord(2,8, QString("a"),QString("a"),DT, WORD, QVector<corefCoord>());
	arr9[28]= StanfordWord(2,9, QString("holiday "),QString("holiday "),NN, WORD, QVector<corefCoord>());
	arr9[29]= StanfordWord(2,10, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr9[30]= StanfordWord(2,11, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr9[31]= StanfordWord(2,11, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());

	QList<StanfordWord*> start9, res9;
	QMap<QString, StanfordWord*> Text9;
	for(int i = 0 ; i < wCount ; i++)
	{
		start9.push_back(arr9+i);
		res9.push_back(arr9+i);
		if(arr9[i].type==WORD)
		{
			Text9.insert(arr9[i].doKey(), arr9+i);
		}
	}

	res9.removeAt(0);
	res9.insert(0, new StanfordWord(1,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>()));

	QSet  <person> allPersons9, speakersList9;

	QStringList objQualifFirstPer9;
	objQualifFirstPer9.push_back(QString("Bilbo"));
	objQualifFirstPer9.push_back(QString("hobbit"));

	QStringList subjQualifFirstPer9;
	subjQualifFirstPer9.push_back("old friend");

	QStringList objQualifSecondPer9;
	objQualifSecondPer9.push_back(QString("Gandalf"));
	objQualifSecondPer9.push_back(QString("wizard"));

	QStringList subjQualifSecondPer9;
	subjQualifSecondPer9.push_back("old friend");

	QStringList objQualifTherdPer9;
	objQualifTherdPer9.push_back(QString("Frodo"));
	objQualifTherdPer9.push_back(QString("hobbit"));

	QStringList subjQualifTherdPer9;

	allPersons9.insert(person(QString("Bilbo"), objQualifFirstPer9, subjQualifFirstPer9, QString("00ff00")));
	allPersons9.insert(person(QString("Gandalf"), objQualifSecondPer9, subjQualifSecondPer9, QString("ff0000")));
	allPersons9.insert(person(QString("Frodo"), objQualifTherdPer9, subjQualifTherdPer9, QString("f0000f")));

	QListIterator<StanfordWord*> i9(start9);
	textTree* input9 = new textTree(i9);

	QListIterator<StanfordWord*> j9(res9);
	textTree* output9 = new textTree(j9);

	delete res9[0];
	QTest::newRow("TwoAppealsOneParagraphDifferentSentence") << allPersons9 << Text9 << input9 << output9 << speakersList9 << arr9;
	
	/////////////////////////////////////���� 10
	/////////////////////////////////////ObjectiveSpecifierAfterDirectSpeech
	
	wCount = 39;
	StanfordWord* arr10 = new StanfordWord[wCount];

	arr10[0]= StanfordWord(1,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr10[1]= StanfordWord(1,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr10[2]= StanfordWord(1,2, QString("Yes"),QString("yes"),UH, WORD, QVector<corefCoord>());
	arr10[3]= StanfordWord(1,3, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr10[4]= StanfordWord(1,4, QString("Frodo"),QString("Frodo"),NNP, WORD, QVector<corefCoord>());
	arr10[5]= StanfordWord(1,5, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr10[6]= StanfordWord(1,6, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr10[7]= StanfordWord(1,6, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());
	arr10[8]= StanfordWord(1,7, QString("said"),QString("say"),VBD, WORD, QVector<corefCoord>());
	arr10[9]= StanfordWord(1,8, QString("hobbit"),QString("hobbit"),NN, WORD, QVector<corefCoord>());
	arr10[10]= StanfordWord(1,9, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());

	arr10[11]= StanfordWord(2,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr10[12]= StanfordWord(2,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr10[13]= StanfordWord(2,2, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr10[14]= StanfordWord(2,3, QString("am"),QString("be"),VBP, WORD, QVector<corefCoord>());
	arr10[15]= StanfordWord(2,4, QString("very"),QString("very"),RB, WORD, QVector<corefCoord>());
	arr10[16]= StanfordWord(2,5, QString("fond"),QString("fond"),JJ, WORD, QVector<corefCoord>());
	arr10[17]= StanfordWord(2,6, QString("indeed"),QString("indeed"),RB, WORD, QVector<corefCoord>());
	arr10[18]= StanfordWord(2,7, QString("of"),QString("of"),IN, WORD, QVector<corefCoord>());
	arr10[19]= StanfordWord(2,8, QString("it"),QString("it"),PRP, WORD, QVector<corefCoord>());
	arr10[20]= StanfordWord(2,9, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr10[21]= StanfordWord(2,10, QString("and"),QString("and"),CC, WORD, QVector<corefCoord>());
	arr10[22]= StanfordWord(2,11, QString("of"),QString("of"),IN, WORD, QVector<corefCoord>());
	arr10[23]= StanfordWord(2,12, QString("all"),QString("all"),PDT, WORD, QVector<corefCoord>());
	arr10[24]= StanfordWord(2,13, QString("the"),QString("the"),DT, WORD, QVector<corefCoord>());
	arr10[25]= StanfordWord(2,14, QString("dear"),QString("dear"),RB, WORD, QVector<corefCoord>());
	arr10[26]= StanfordWord(2,15, QString("old"),QString("old"),JJ, WORD, QVector<corefCoord>());
	arr10[27]= StanfordWord(2,16, QString("Shire"),QString("Shire"),NNP, WORD, QVector<corefCoord>());
	arr10[28]= StanfordWord(2,17, QString(";"),QString(";"),Colon, WORD, QVector<corefCoord>());
	arr10[29]= StanfordWord(2,18, QString("but"),QString("but"),CC, WORD, QVector<corefCoord>());
	arr10[30]= StanfordWord(2,19, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr10[31]= StanfordWord(2,20, QString("think"),QString("think"),VBP, WORD, QVector<corefCoord>());
	arr10[32]= StanfordWord(2,21, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr10[33]= StanfordWord(2,22, QString("need"),QString("need"),VB, WORD, QVector<corefCoord>());
	arr10[34]= StanfordWord(2,23, QString("a"),QString("a"),DT, WORD, QVector<corefCoord>());
	arr10[35]= StanfordWord(2,24, QString("holiday "),QString("holiday "),NN, WORD, QVector<corefCoord>());
	arr10[36]= StanfordWord(2,25, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr10[37]= StanfordWord(2,26, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr10[38]= StanfordWord(2,26, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());

	QList<StanfordWord*> start10, res10;
	QMap<QString, StanfordWord*> Text10;
	for(int i = 0 ; i < wCount ; i++)
	{
		start10.push_back(arr10+i);
		res10.push_back(arr10+i);
		if(arr10[i].type==WORD)
		{
			Text10.insert(arr10[i].doKey(), arr10+i);
		}
	}

	res10.removeAt(0);
	res10.insert(0, new StanfordWord(1,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>()));

	res10.removeAt(11);
	res10.insert(11, new StanfordWord(2,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>()));

	QSet  <person> allPersons10, speakersList10;

	QStringList objQualifFirstPer10;
	objQualifFirstPer10.push_back(QString("Bilbo"));
	objQualifFirstPer10.push_back(QString("hobbit"));

	QStringList subjQualifFirstPer10;
	subjQualifFirstPer10.push_back("old friend");

	QStringList objQualifSecondPer10;
	objQualifSecondPer10.push_back(QString("Gandalf"));
	objQualifSecondPer10.push_back(QString("wizard"));

	QStringList subjQualifSecondPer10;
	subjQualifSecondPer10.push_back("old friend");

	QStringList objQualifTherdPer10;
	objQualifTherdPer10.push_back(QString("Frodo"));
	objQualifTherdPer10.push_back(QString("hobbit"));

	QStringList subjQualifTherdPer10;

	allPersons10.insert(person(QString("Bilbo"), objQualifFirstPer10, subjQualifFirstPer10, QString("00ff00")));
	allPersons10.insert(person(QString("Gandalf"), objQualifSecondPer10, subjQualifSecondPer10, QString("ff0000")));
	allPersons10.insert(person(QString("Frodo"), objQualifTherdPer10, subjQualifTherdPer10, QString("f0000f")));

	QListIterator<StanfordWord*> i10(start10);
	textTree* input10 = new textTree(i10);

	QListIterator<StanfordWord*> j10(res10);
	textTree* output10 = new textTree(j10);

	delete res10[0];
	delete res10[11];

	QTest::newRow("ObjectiveSpecifierAfterDirectSpeech") << allPersons10 << Text10 << input10 << output10 << speakersList10 << arr10;

	/////////////////////////////////////���� 11
	/////////////////////////////////////ObjectiveSpecifierBeforeDirectSpeech
	
	wCount = 35;
	StanfordWord* arr11 = new StanfordWord[wCount];
	
	arr11[0]= StanfordWord(1,1, QString("Hobbit"),QString("hobbit"),NN, WORD, QVector<corefCoord>());
	arr11[1]= StanfordWord(1,2, QString("said"),QString("say"),VBD, WORD, QVector<corefCoord>());
	arr11[2]= StanfordWord(1,3, QString(":"),QString(":"),Colon, WORD, QVector<corefCoord>());
	arr11[3]= StanfordWord(1,4, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr11[4]= StanfordWord(1,4, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr11[5]= StanfordWord(1,5, QString("Yes"),QString("yes"),UH, WORD, QVector<corefCoord>());
	arr11[6]= StanfordWord(1,6, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr11[7]= StanfordWord(1,7, QString("Frodo"),QString("Frodo"),NNP, WORD, QVector<corefCoord>());
	arr11[8]= StanfordWord(1,8, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());

	arr11[9]= StanfordWord(2,1, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr11[10]= StanfordWord(2,2, QString("am"),QString("be"),VBP, WORD, QVector<corefCoord>());
	arr11[11]= StanfordWord(2,3, QString("very"),QString("very"),RB, WORD, QVector<corefCoord>());
	arr11[12]= StanfordWord(2,4, QString("fond"),QString("fond"),JJ, WORD, QVector<corefCoord>());
	arr11[13]= StanfordWord(2,5, QString("indeed"),QString("indeed"),RB, WORD, QVector<corefCoord>());
	arr11[14]= StanfordWord(2,6, QString("of"),QString("of"),IN, WORD, QVector<corefCoord>());
	arr11[15]= StanfordWord(2,7, QString("it"),QString("it"),PRP, WORD, QVector<corefCoord>());
	arr11[16]= StanfordWord(2,8, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr11[17]= StanfordWord(2,9, QString("and"),QString("and"),CC, WORD, QVector<corefCoord>());
	arr11[18]= StanfordWord(2,10, QString("of"),QString("of"),IN, WORD, QVector<corefCoord>());
	arr11[19]= StanfordWord(2,11, QString("all"),QString("all"),PDT, WORD, QVector<corefCoord>());
	arr11[20]= StanfordWord(2,12, QString("the"),QString("the"),DT, WORD, QVector<corefCoord>());
	arr11[21]= StanfordWord(2,13, QString("dear"),QString("dear"),RB, WORD, QVector<corefCoord>());
	arr11[22]= StanfordWord(2,14, QString("old"),QString("old"),JJ, WORD, QVector<corefCoord>());
	arr11[23]= StanfordWord(2,15, QString("Shire"),QString("Shire"),NNP, WORD, QVector<corefCoord>());
	arr11[24]= StanfordWord(2,16, QString(";"),QString(";"),Colon, WORD, QVector<corefCoord>());
	arr11[25]= StanfordWord(2,17, QString("but"),QString("but"),CC, WORD, QVector<corefCoord>());
	arr11[26]= StanfordWord(2,18, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr11[27]= StanfordWord(2,19, QString("think"),QString("think"),VBP, WORD, QVector<corefCoord>());
	arr11[28]= StanfordWord(2,20, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr11[29]= StanfordWord(2,21, QString("need"),QString("need"),VB, WORD, QVector<corefCoord>());
	arr11[30]= StanfordWord(2,22, QString("a"),QString("a"),DT, WORD, QVector<corefCoord>());
	arr11[31]= StanfordWord(2,23, QString("holiday "),QString("holiday "),NN, WORD, QVector<corefCoord>());
	arr11[32]= StanfordWord(2,24, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr11[33]= StanfordWord(2,25, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr11[34]= StanfordWord(2,25, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());

	QList<StanfordWord*> start11, res11;
	QMap<QString, StanfordWord*> Text11;
	for(int i = 0 ; i < wCount ; i++)
	{
		start11.push_back(arr11+i);
		res11.push_back(arr11+i);
		if(arr11[i].type==WORD)
		{
			Text11.insert(arr11[i].doKey(), arr11+i);
		}
	}

	res11.removeAt(3);
	res11.insert(3, new StanfordWord(1,4, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>()));

	QSet  <person> allPersons11, speakersList11;

	QStringList objQualifFirstPer11;
	objQualifFirstPer11.push_back(QString("Bilbo"));
	objQualifFirstPer11.push_back(QString("hobbit"));

	QStringList subjQualifFirstPer11;
	subjQualifFirstPer11.push_back("old friend");

	QStringList objQualifSecondPer11;
	objQualifSecondPer11.push_back(QString("Gandalf"));
	objQualifSecondPer11.push_back(QString("wizard"));

	QStringList subjQualifSecondPer11;
	subjQualifSecondPer11.push_back("old friend");

	QStringList objQualifTherdPer11;
	objQualifTherdPer11.push_back(QString("Frodo"));
	objQualifTherdPer11.push_back(QString("hobbit"));

	QStringList subjQualifTherdPer11;

	allPersons11.insert(person(QString("Bilbo"), objQualifFirstPer11, subjQualifFirstPer11, QString("00ff00")));
	allPersons11.insert(person(QString("Gandalf"), objQualifSecondPer11, subjQualifSecondPer11, QString("ff0000")));
	allPersons11.insert(person(QString("Frodo"), objQualifTherdPer11, subjQualifTherdPer11, QString("f0000f")));

	QListIterator<StanfordWord*> i11(start11);
	textTree* input11 = new textTree(i11);

	QListIterator<StanfordWord*> j11(res11);
	textTree* output11 = new textTree(j11);

	delete res11[3];
	QTest::newRow("ObjectiveSpecifierBeforeDirectSpeech") << allPersons11 << Text11 << input11 << output11 << speakersList11 << arr11;

	/////////////////////////////////////���� 12
	/////////////////////////////////////PartialTruncationObjectiveSpecifierFirst

	wCount = 35;
	StanfordWord* arr12 = new StanfordWord[wCount];
	
	arr12[0]= StanfordWord(1,1, QString("Hobbit"),QString("hobbit"),NN, WORD, QVector<corefCoord>());
	arr12[1]= StanfordWord(1,2, QString("said"),QString("say"),VBD, WORD, QVector<corefCoord>());
	arr12[2]= StanfordWord(1,3, QString(":"),QString(":"),Colon, WORD, QVector<corefCoord>());
	arr12[3]= StanfordWord(1,4, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr12[4]= StanfordWord(1,4, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr12[5]= StanfordWord(1,5, QString("Yes"),QString("yes"),UH, WORD, QVector<corefCoord>());
	arr12[6]= StanfordWord(1,6, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr12[7]= StanfordWord(1,7, QString("Frodo"),QString("Frodo"),NNP, WORD, QVector<corefCoord>());
	arr12[8]= StanfordWord(1,8, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());

	arr12[9]= StanfordWord(2,1, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr12[10]= StanfordWord(2,2, QString("am"),QString("be"),VBP, WORD, QVector<corefCoord>());
	arr12[11]= StanfordWord(2,3, QString("very"),QString("very"),RB, WORD, QVector<corefCoord>());
	arr12[12]= StanfordWord(2,4, QString("fond"),QString("fond"),JJ, WORD, QVector<corefCoord>());
	arr12[13]= StanfordWord(2,5, QString("indeed"),QString("indeed"),RB, WORD, QVector<corefCoord>());
	arr12[14]= StanfordWord(2,6, QString("of"),QString("of"),IN, WORD, QVector<corefCoord>());
	arr12[15]= StanfordWord(2,7, QString("it"),QString("it"),PRP, WORD, QVector<corefCoord>());
	arr12[16]= StanfordWord(2,8, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr12[17]= StanfordWord(2,9, QString("and"),QString("and"),CC, WORD, QVector<corefCoord>());
	arr12[18]= StanfordWord(2,10, QString("of"),QString("of"),IN, WORD, QVector<corefCoord>());
	arr12[19]= StanfordWord(2,11, QString("all"),QString("all"),PDT, WORD, QVector<corefCoord>());
	arr12[20]= StanfordWord(2,12, QString("the"),QString("the"),DT, WORD, QVector<corefCoord>());
	arr12[21]= StanfordWord(2,13, QString("dear"),QString("dear"),RB, WORD, QVector<corefCoord>());
	arr12[22]= StanfordWord(2,14, QString("old"),QString("old"),JJ, WORD, QVector<corefCoord>());
	arr12[23]= StanfordWord(2,15, QString("Shire"),QString("Shire"),NNP, WORD, QVector<corefCoord>());
	arr12[24]= StanfordWord(2,16, QString(";"),QString(";"),Colon, WORD, QVector<corefCoord>());
	arr12[25]= StanfordWord(2,17, QString("but"),QString("but"),CC, WORD, QVector<corefCoord>());
	arr12[26]= StanfordWord(2,18, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr12[27]= StanfordWord(2,19, QString("think"),QString("think"),VBP, WORD, QVector<corefCoord>());
	arr12[28]= StanfordWord(2,20, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr12[29]= StanfordWord(2,21, QString("need"),QString("need"),VB, WORD, QVector<corefCoord>());
	arr12[30]= StanfordWord(2,22, QString("a"),QString("a"),DT, WORD, QVector<corefCoord>());
	arr12[31]= StanfordWord(2,23, QString("holiday "),QString("holiday "),NN, WORD, QVector<corefCoord>());
	arr12[32]= StanfordWord(2,24, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr12[33]= StanfordWord(2,25, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr12[34]= StanfordWord(2,25, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());

	QList<StanfordWord*> start12, res12;
	QMap<QString, StanfordWord*> Text12;
	for(int i = 0 ; i < wCount ; i++)
	{
		start12.push_back(arr12+i);
		res12.push_back(arr12+i);
		if(arr12[i].type==WORD)
		{
			Text12.insert(arr12[i].doKey(), arr12+i);
		}
	}

	res12.removeAt(3);
	res12.insert(3, new StanfordWord(1,4, QString("<u><span style=\"color:\#0000ff\">"),QString("<u><span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>()));

	res12.removeAt(34);
	res12.insert(34, new StanfordWord(2,25, QString("</span></u>(Bilbo, Mery)"),QString("</span></u>(Bilbo, Mery)"),CC, MARK, QVector<corefCoord>()));

	QSet  <person> allPersons12, speakersList12;

	QStringList objQualifFirstPer12;
	objQualifFirstPer12.push_back(QString("Bilbo"));
	objQualifFirstPer12.push_back(QString("hobbit"));

	QStringList subjQualifFirstPer12;
	subjQualifFirstPer12.push_back("old friend");

	QStringList objQualifSecondPer12;
	objQualifSecondPer12.push_back(QString("Gandalf"));
	objQualifSecondPer12.push_back(QString("wizard"));

	QStringList subjQualifSecondPer12;
	subjQualifSecondPer12.push_back("old friend");

	QStringList objQualifTherdPer12;
	objQualifTherdPer12.push_back(QString("Frodo"));
	objQualifTherdPer12.push_back(QString("hobbit"));

	QStringList subjQualifTherdPer12;

	QStringList objQualifFourPer12;
	objQualifFourPer12.push_back(QString("Mery"));
	objQualifFourPer12.push_back(QString("hobbit"));

	QStringList subjQualifFourPer12;

	allPersons12.insert(person(QString("Bilbo"), objQualifFirstPer12, subjQualifFirstPer12, QString("00ff00")));
	allPersons12.insert(person(QString("Gandalf"), objQualifSecondPer12, subjQualifSecondPer12, QString("ff0000")));
	allPersons12.insert(person(QString("Frodo"), objQualifTherdPer12, subjQualifTherdPer12, QString("f0000f")));
	allPersons12.insert(person(QString("Mery"), objQualifFourPer12, subjQualifFourPer12, QString("000ff0")));

	QListIterator<StanfordWord*> i12(start12);
	textTree* input12 = new textTree(i12);

	QListIterator<StanfordWord*> j12(res12);
	textTree* output12 = new textTree(j12);

	delete res12[3];
	delete res12[34];
	QTest::newRow("PartialTruncationObjectiveSpecifierFirst") << allPersons12 << Text12 << input12 << output12 << speakersList12 << arr12;

	/////////////////////////////////////���� 13
	/////////////////////////////////////PartialTruncationObjectiveSpecifierAfterSpeech
	
	wCount = 39;
	StanfordWord* arr13 = new StanfordWord[wCount];

	arr13[0]= StanfordWord(1,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr13[1]= StanfordWord(1,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr13[2]= StanfordWord(1,2, QString("Yes"),QString("yes"),UH, WORD, QVector<corefCoord>());
	arr13[3]= StanfordWord(1,3, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr13[4]= StanfordWord(1,4, QString("Frodo"),QString("Frodo"),NNP, WORD, QVector<corefCoord>());
	arr13[5]= StanfordWord(1,5, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr13[6]= StanfordWord(1,6, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr13[7]= StanfordWord(1,6, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());
	arr13[8]= StanfordWord(1,7, QString("said"),QString("say"),VBD, WORD, QVector<corefCoord>());
	arr13[9]= StanfordWord(1,8, QString("hobbit"),QString("hobbit"),NN, WORD, QVector<corefCoord>());
	arr13[10]= StanfordWord(1,9, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());

	arr13[11]= StanfordWord(2,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr13[12]= StanfordWord(2,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr13[13]= StanfordWord(2,2, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr13[14]= StanfordWord(2,3, QString("am"),QString("be"),VBP, WORD, QVector<corefCoord>());
	arr13[15]= StanfordWord(2,4, QString("very"),QString("very"),RB, WORD, QVector<corefCoord>());
	arr13[16]= StanfordWord(2,5, QString("fond"),QString("fond"),JJ, WORD, QVector<corefCoord>());
	arr13[17]= StanfordWord(2,6, QString("indeed"),QString("indeed"),RB, WORD, QVector<corefCoord>());
	arr13[18]= StanfordWord(2,7, QString("of"),QString("of"),IN, WORD, QVector<corefCoord>());
	arr13[19]= StanfordWord(2,8, QString("it"),QString("it"),PRP, WORD, QVector<corefCoord>());
	arr13[20]= StanfordWord(2,9, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr13[21]= StanfordWord(2,10, QString("and"),QString("and"),CC, WORD, QVector<corefCoord>());
	arr13[22]= StanfordWord(2,11, QString("of"),QString("of"),IN, WORD, QVector<corefCoord>());
	arr13[23]= StanfordWord(2,12, QString("all"),QString("all"),PDT, WORD, QVector<corefCoord>());
	arr13[24]= StanfordWord(2,13, QString("the"),QString("the"),DT, WORD, QVector<corefCoord>());
	arr13[25]= StanfordWord(2,14, QString("dear"),QString("dear"),RB, WORD, QVector<corefCoord>());
	arr13[26]= StanfordWord(2,15, QString("old"),QString("old"),JJ, WORD, QVector<corefCoord>());
	arr13[27]= StanfordWord(2,16, QString("Shire"),QString("Shire"),NNP, WORD, QVector<corefCoord>());
	arr13[28]= StanfordWord(2,17, QString(";"),QString(";"),Colon, WORD, QVector<corefCoord>());
	arr13[29]= StanfordWord(2,18, QString("but"),QString("but"),CC, WORD, QVector<corefCoord>());
	arr13[30]= StanfordWord(2,19, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr13[31]= StanfordWord(2,20, QString("think"),QString("think"),VBP, WORD, QVector<corefCoord>());
	arr13[32]= StanfordWord(2,21, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr13[33]= StanfordWord(2,22, QString("need"),QString("need"),VB, WORD, QVector<corefCoord>());
	arr13[34]= StanfordWord(2,23, QString("a"),QString("a"),DT, WORD, QVector<corefCoord>());
	arr13[35]= StanfordWord(2,24, QString("holiday "),QString("holiday "),NN, WORD, QVector<corefCoord>());
	arr13[36]= StanfordWord(2,25, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr13[37]= StanfordWord(2,26, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr13[38]= StanfordWord(2,26, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());

	QList<StanfordWord*> start13, res13;
	QMap<QString, StanfordWord*> Text13;
	for(int i = 0 ; i < wCount ; i++)
	{
		start13.push_back(arr13+i);
		res13.push_back(arr13+i);
		if(arr13[i].type==WORD)
		{
			Text13.insert(arr13[i].doKey(), arr13+i);
		}
	}

	res13.removeAt(0);
	res13.insert(0, new StanfordWord(1,1, QString("<u><span style=\"color:\#0000ff\">"),QString("<u><span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>()));

	res13.removeAt(7);
	res13.insert(7, new StanfordWord(1,6, QString("</span></u>(Bilbo, Mery)"),QString("</span></u>(Bilbo, Mery)"),CC, MARK, QVector<corefCoord>()));

	res13.removeAt(11);
	res13.insert(11, new StanfordWord(2,1, QString("<u><span style=\"color:\#0000ff\">"),QString("<u><span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>()));

	res13.removeAt(38);
	res13.insert(38, new StanfordWord(2,26, QString("</span></u>(Bilbo, Mery)"),QString("</span></u>(Bilbo, Mery)"),CC, MARK, QVector<corefCoord>()));

	QSet  <person> allPersons13, speakersList13;

	QStringList objQualifFirstPer13;
	objQualifFirstPer13.push_back(QString("Bilbo"));
	objQualifFirstPer13.push_back(QString("hobbit"));

	QStringList subjQualifFirstPer13;
	subjQualifFirstPer13.push_back("old friend");

	QStringList objQualifSecondPer13;
	objQualifSecondPer13.push_back(QString("Gandalf"));
	objQualifSecondPer13.push_back(QString("wizard"));

	QStringList subjQualifSecondPer13;
	subjQualifSecondPer13.push_back("old friend");

	QStringList objQualifTherdPer13;
	objQualifTherdPer13.push_back(QString("Frodo"));
	objQualifTherdPer13.push_back(QString("hobbit"));

	QStringList subjQualifTherdPer13;

	QStringList objQualifFourPer13;
	objQualifFourPer13.push_back(QString("Mery"));
	objQualifFourPer13.push_back(QString("hobbit"));

	QStringList subjQualifFourPer13;

	allPersons13.insert(person(QString("Bilbo"), objQualifFirstPer13, subjQualifFirstPer13, QString("00ff00")));
	allPersons13.insert(person(QString("Gandalf"), objQualifSecondPer13, subjQualifSecondPer13, QString("ff0000")));
	allPersons13.insert(person(QString("Frodo"), objQualifTherdPer13, subjQualifTherdPer13, QString("f0000f")));
	allPersons13.insert(person(QString("Mery"), objQualifFourPer13, subjQualifFourPer13, QString("000ff0")));

	QListIterator<StanfordWord*> i13(start13);
	textTree* input13 = new textTree(i13);

	QListIterator<StanfordWord*> j13(res13);
	textTree* output13 = new textTree(j13);

	delete res13[0];
	delete res13[7];
	delete res13[11];
	delete res13[38];
	QTest::newRow("PartialTruncationObjectiveSpecifierAfterSpeech") << allPersons13 << Text13 << input13 << output13 << speakersList13 << arr13;

	/////////////////////////////////////���� 14
	/////////////////////////////////////AddressByNameFewSentences
	
	wCount = 31;
	StanfordWord* arr14 = new StanfordWord[wCount];

	arr14[0]= StanfordWord(1,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr14[1]= StanfordWord(1,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr14[2]= StanfordWord(1,2, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr14[3]= StanfordWord(1,3, QString("am"),QString("be"),VBP, WORD, QVector<corefCoord>());
	arr14[4]= StanfordWord(1,4, QString("very"),QString("very"),RB, WORD, QVector<corefCoord>());
	arr14[5]= StanfordWord(1,5, QString("fond"),QString("fond"),JJ, WORD, QVector<corefCoord>());
	arr14[6]= StanfordWord(1,6, QString("indeed"),QString("indeed"),RB, WORD, QVector<corefCoord>());
	arr14[7]= StanfordWord(1,7, QString("of"),QString("of"),IN, WORD, QVector<corefCoord>());
	arr14[8]= StanfordWord(1,8, QString("it"),QString("it"),PRP, WORD, QVector<corefCoord>());
	arr14[9]= StanfordWord(1,9, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr14[10]= StanfordWord(1,10, QString("and"),QString("and"),CC, WORD, QVector<corefCoord>());
	arr14[11]= StanfordWord(1,11, QString("of"),QString("of"),IN, WORD, QVector<corefCoord>());
	arr14[12]= StanfordWord(1,12, QString("all"),QString("all"),PDT, WORD, QVector<corefCoord>());
	arr14[13]= StanfordWord(1,13, QString("the"),QString("the"),DT, WORD, QVector<corefCoord>());
	arr14[14]= StanfordWord(1,14, QString("dear"),QString("dear"),RB, WORD, QVector<corefCoord>());
	arr14[15]= StanfordWord(1,15, QString("old"),QString("old"),JJ, WORD, QVector<corefCoord>());
	arr14[16]= StanfordWord(1,16, QString("Shire"),QString("Shire"),NNP, WORD, QVector<corefCoord>());
	arr14[17]= StanfordWord(1,17, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr14[18]= StanfordWord(1,18, QString("Gandalf"),QString("Gandalf"),NNP, WORD, QVector<corefCoord>());
	arr14[19]= StanfordWord(1,19, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr14[20]= StanfordWord(2,1, QString("Frodo"),QString("Frodo"),NNP, WORD, QVector<corefCoord>());
	arr14[21]= StanfordWord(2,2, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr14[22]= StanfordWord(2,3, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr14[23]= StanfordWord(2,4, QString("think"),QString("think"),VBP, WORD, QVector<corefCoord>());
	arr14[24]= StanfordWord(2,5, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr14[25]= StanfordWord(2,6, QString("need"),QString("need"),VB, WORD, QVector<corefCoord>());
	arr14[26]= StanfordWord(2,7, QString("a"),QString("a"),DT, WORD, QVector<corefCoord>());
	arr14[27]= StanfordWord(2,8, QString("holiday "),QString("holiday "),NN, WORD, QVector<corefCoord>());
	arr14[28]= StanfordWord(2,9, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr14[29]= StanfordWord(2,10, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr14[30]= StanfordWord(2,10, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());

	QList<StanfordWord*> start14, res14;
	QMap<QString, StanfordWord*> Text14;
	for(int i = 0 ; i < wCount ; i++)
	{
		start14.push_back(arr14+i);
		res14.push_back(arr14+i);
		if(arr14[i].type==WORD)
		{
			Text14.insert(arr14[i].doKey(), arr14+i);
		}
	}

	res14.removeAt(0);
	res14.insert(0, new StanfordWord(1,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>()));

	QSet  <person> allPersons14, speakersList14;

	QStringList objQualifFirstPer14;
	objQualifFirstPer14.push_back(QString("Bilbo"));
	objQualifFirstPer14.push_back(QString("hobbit"));

	QStringList subjQualifFirstPer14;
	subjQualifFirstPer14.push_back("old friend");

	QStringList objQualifSecondPer14;
	objQualifSecondPer14.push_back(QString("Gandalf"));
	objQualifSecondPer14.push_back(QString("wizard"));

	QStringList subjQualifSecondPer14;
	subjQualifSecondPer14.push_back("old friend");

	QStringList objQualifTherdPer14;
	objQualifTherdPer14.push_back(QString("Frodo"));
	objQualifTherdPer14.push_back(QString("hobbit"));

	QStringList subjQualifTherdPer14;

	allPersons14.insert(person(QString("Bilbo"), objQualifFirstPer14, subjQualifFirstPer14, QString("00ff00")));
	allPersons14.insert(person(QString("Gandalf"), objQualifSecondPer14, subjQualifSecondPer14, QString("ff0000")));
	allPersons14.insert(person(QString("Frodo"), objQualifTherdPer14, subjQualifTherdPer14, QString("f0000f")));

	QListIterator<StanfordWord*> i14(start14);
	textTree* input14 = new textTree(i14);

	QListIterator<StanfordWord*> j14(res14);
	textTree* output14 = new textTree(j14);

	delete res14[0];
	QTest::newRow("TwoAppealsByNameInOneFragmentInDifferentSentences") << allPersons14 << Text14 << input14 << output14 << speakersList14 << arr14;

	/////////////////////////////////////���� 15
	/////////////////////////////////////TwoAppealsByNameInOneFragmentInOneSentence
	
	wCount = 31;
	StanfordWord* arr15 = new StanfordWord[wCount];

	arr15[0]= StanfordWord(1,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr15[1]= StanfordWord(1,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr15[2]= StanfordWord(1,2, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr15[3]= StanfordWord(1,3, QString("am"),QString("be"),VBP, WORD, QVector<corefCoord>());
	arr15[4]= StanfordWord(1,4, QString("very"),QString("very"),RB, WORD, QVector<corefCoord>());
	arr15[5]= StanfordWord(1,5, QString("fond"),QString("fond"),JJ, WORD, QVector<corefCoord>());
	arr15[6]= StanfordWord(1,6, QString("indeed"),QString("indeed"),RB, WORD, QVector<corefCoord>());
	arr15[7]= StanfordWord(1,7, QString("of"),QString("of"),IN, WORD, QVector<corefCoord>());
	arr15[8]= StanfordWord(1,8, QString("it"),QString("it"),PRP, WORD, QVector<corefCoord>());
	arr15[9]= StanfordWord(1,9, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr15[10]= StanfordWord(1,10, QString("and"),QString("and"),CC, WORD, QVector<corefCoord>());
	arr15[11]= StanfordWord(1,11, QString("of"),QString("of"),IN, WORD, QVector<corefCoord>());
	arr15[12]= StanfordWord(1,12, QString("all"),QString("all"),PDT, WORD, QVector<corefCoord>());
	arr15[13]= StanfordWord(1,13, QString("the"),QString("the"),DT, WORD, QVector<corefCoord>());
	arr15[14]= StanfordWord(1,14, QString("dear"),QString("dear"),RB, WORD, QVector<corefCoord>());
	arr15[15]= StanfordWord(1,15, QString("old"),QString("old"),JJ, WORD, QVector<corefCoord>());
	arr15[16]= StanfordWord(1,16, QString("Shire"),QString("Shire"),NNP, WORD, QVector<corefCoord>());
	arr15[17]= StanfordWord(1,17, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr15[18]= StanfordWord(2,1, QString("Frodo"),QString("Frodo"),NNP, WORD, QVector<corefCoord>());
	arr15[19]= StanfordWord(2,2, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr15[20]= StanfordWord(2,3, QString("Gandalf"),QString("Gandalf"),NNP, WORD, QVector<corefCoord>());
	arr15[21]= StanfordWord(2,4, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr15[22]= StanfordWord(2,5, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr15[23]= StanfordWord(2,6, QString("think"),QString("think"),VBP, WORD, QVector<corefCoord>());
	arr15[24]= StanfordWord(2,7, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr15[25]= StanfordWord(2,8, QString("need"),QString("need"),VB, WORD, QVector<corefCoord>());
	arr15[26]= StanfordWord(2,9, QString("a"),QString("a"),DT, WORD, QVector<corefCoord>());
	arr15[27]= StanfordWord(2,10, QString("holiday "),QString("holiday "),NN, WORD, QVector<corefCoord>());
	arr15[28]= StanfordWord(2,11, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr15[29]= StanfordWord(2,12, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr15[30]= StanfordWord(2,12, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());

	QList<StanfordWord*> start15, res15;
	QMap<QString, StanfordWord*> Text15;
	for(int i = 0 ; i < wCount ; i++)
	{
		start15.push_back(arr15+i);
		res15.push_back(arr15+i);
		if(arr15[i].type==WORD)
		{
			Text15.insert(arr15[i].doKey(), arr15+i);
		}
	}

	res15.removeAt(0);
	res15.insert(0, new StanfordWord(1,1, QString("<span style=\"color:\#00ff00\">"),QString("<span style=\"color:\#00ff00\">"),CC, MARK, QVector<corefCoord>()));

	QSet  <person> allPersons15, speakersList15;

	QStringList objQualifFirstPer15;
	objQualifFirstPer15.push_back(QString("Bilbo"));
	objQualifFirstPer15.push_back(QString("hobbit"));

	QStringList subjQualifFirstPer15;
	subjQualifFirstPer15.push_back("old friend");

	QStringList objQualifSecondPer15;
	objQualifSecondPer15.push_back(QString("Gandalf"));
	objQualifSecondPer15.push_back(QString("wizard"));

	QStringList subjQualifSecondPer15;
	subjQualifSecondPer15.push_back("old friend");

	QStringList objQualifTherdPer15;
	objQualifTherdPer15.push_back(QString("Frodo"));
	objQualifTherdPer15.push_back(QString("hobbit"));

	QStringList subjQualifTherdPer15;

	allPersons15.insert(person(QString("Bilbo"), objQualifFirstPer15, subjQualifFirstPer15, QString("00ff00")));
	allPersons15.insert(person(QString("Gandalf"), objQualifSecondPer15, subjQualifSecondPer15, QString("ff0000")));
	allPersons15.insert(person(QString("Frodo"), objQualifTherdPer15, subjQualifTherdPer15, QString("f0000f")));

	QListIterator<StanfordWord*> i15(start15);
	textTree* input15 = new textTree(i15);

	QListIterator<StanfordWord*> j15(res15);
	textTree* output15 = new textTree(j15);

	delete res15[0];
	QTest::newRow("TwoAppealsByNameInOneFragmentInOneSentence") << allPersons15 << Text15 << input15 << output15 << speakersList15 << arr15;

	/////////////////////////////////////���� 16
	/////////////////////////////////////Conflict

	wCount = 34;
	StanfordWord* arr16 = new StanfordWord[wCount];

	arr16[0]= StanfordWord(1,1, QString("<span style=\"color:\#0000ff\">"),QString("<span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>());
	arr16[1]= StanfordWord(1,1, QString("`"),QString("`"),quotation, WORD, QVector<corefCoord>());
	arr16[2]= StanfordWord(1,2, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr16[3]= StanfordWord(1,3, QString("am"),QString("be"),VBP, WORD, QVector<corefCoord>());
	arr16[4]= StanfordWord(1,4, QString("very"),QString("very"),RB, WORD, QVector<corefCoord>());
	arr16[5]= StanfordWord(1,5, QString("fond"),QString("fond"),JJ, WORD, QVector<corefCoord>());
	arr16[6]= StanfordWord(1,6, QString("indeed"),QString("indeed"),RB, WORD, QVector<corefCoord>());
	arr16[7]= StanfordWord(1,7, QString("of"),QString("of"),IN, WORD, QVector<corefCoord>());
	arr16[8]= StanfordWord(1,8, QString("it"),QString("it"),PRP, WORD, QVector<corefCoord>());
	arr16[9]= StanfordWord(1,9, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr16[10]= StanfordWord(1,10, QString("and"),QString("and"),CC, WORD, QVector<corefCoord>());
	arr16[11]= StanfordWord(1,11, QString("of"),QString("of"),IN, WORD, QVector<corefCoord>());
	arr16[12]= StanfordWord(1,12, QString("all"),QString("all"),PDT, WORD, QVector<corefCoord>());
	arr16[13]= StanfordWord(1,13, QString("the"),QString("the"),DT, WORD, QVector<corefCoord>());
	arr16[14]= StanfordWord(1,14, QString("dear"),QString("dear"),RB, WORD, QVector<corefCoord>());
	arr16[15]= StanfordWord(1,15, QString("old"),QString("old"),JJ, WORD, QVector<corefCoord>());
	arr16[16]= StanfordWord(1,16, QString("Shire"),QString("Shire"),NNP, WORD, QVector<corefCoord>());
	arr16[17]= StanfordWord(1,17, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr16[18]= StanfordWord(2,1, QString("Frodo"),QString("Frodo"),NNP, WORD, QVector<corefCoord>());
	arr16[19]= StanfordWord(2,2, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr16[20]= StanfordWord(2,3, QString("Gandalf"),QString("Gandalf"),NNP, WORD, QVector<corefCoord>());
	arr16[21]= StanfordWord(2,4, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr16[22]= StanfordWord(2,5, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr16[23]= StanfordWord(2,6, QString("think"),QString("think"),VBP, WORD, QVector<corefCoord>());
	arr16[24]= StanfordWord(2,7, QString("I"),QString("I"),PRP, WORD, QVector<corefCoord>());
	arr16[25]= StanfordWord(2,8, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr16[26]= StanfordWord(2,9, QString("Bilbo"),QString("Bilbo"),NNP, WORD, QVector<corefCoord>());
	arr16[27]= StanfordWord(2,10, QString(","),QString(","),Comma, WORD, QVector<corefCoord>());
	arr16[28]= StanfordWord(2,11, QString("need"),QString("need"),VB, WORD, QVector<corefCoord>());
	arr16[29]= StanfordWord(2,12, QString("a"),QString("a"),DT, WORD, QVector<corefCoord>());
	arr16[30]= StanfordWord(2,13, QString("holiday "),QString("holiday "),NN, WORD, QVector<corefCoord>());
	arr16[31]= StanfordWord(2,14, QString("."),QString("."),SentFinPunct, WORD, QVector<corefCoord>());
	arr16[32]= StanfordWord(2,15, QString("'"),QString("'"),quotation, WORD, QVector<corefCoord>());
	arr16[33]= StanfordWord(2,15, QString("</span>"),QString("</span>"),CC, MARK, QVector<corefCoord>());

	QList<StanfordWord*> start16, res16;
	QMap<QString, StanfordWord*> Text16;
	for(int i = 0 ; i < wCount ; i++)
	{
		start16.push_back(arr16+i);
		res16.push_back(arr16+i);
		if(arr16[i].type==WORD)
		{
			Text16.insert(arr16[i].doKey(), arr16+i);
		}
	}

	res16.removeAt(0);
	res16.insert(0, new StanfordWord(1,1, QString("<u><span style=\"color:\#0000ff\">"),QString("<u><span style=\"color:\#0000ff\">"),CC, MARK, QVector<corefCoord>()));

	res16.removeAt(33);
	res16.insert(33, new StanfordWord(2,15, QString("</span><u>(CONFLICT)"),QString("</span><u>(CONFLICT)"),CC, MARK, QVector<corefCoord>()));

	QSet  <person> allPersons16, speakersList16;

	QStringList objQualifFirstPer16;
	objQualifFirstPer16.push_back(QString("Bilbo"));
	objQualifFirstPer16.push_back(QString("hobbit"));

	QStringList subjQualifFirstPer16;
	subjQualifFirstPer16.push_back("old friend");

	QStringList objQualifSecondPer16;
	objQualifSecondPer16.push_back(QString("Gandalf"));
	objQualifSecondPer16.push_back(QString("wizard"));

	QStringList subjQualifSecondPer16;
	subjQualifSecondPer16.push_back("old friend");

	QStringList objQualifTherdPer16;
	objQualifTherdPer16.push_back(QString("Frodo"));
	objQualifTherdPer16.push_back(QString("hobbit"));

	QStringList subjQualifTherdPer16;

	allPersons16.insert(person(QString("Bilbo"), objQualifFirstPer16, subjQualifFirstPer16, QString("00ff00")));
	allPersons16.insert(person(QString("Gandalf"), objQualifSecondPer16, subjQualifSecondPer16, QString("ff0000")));
	allPersons16.insert(person(QString("Frodo"), objQualifTherdPer16, subjQualifTherdPer16, QString("f0000f")));

	QListIterator<StanfordWord*> i16(start16);
	textTree* input16 = new textTree(i16);

	QListIterator<StanfordWord*> j16(res16);
	textTree* output16 = new textTree(j16);

	delete res16[0];
	delete res16[33];
	QTest::newRow("Conflict") << allPersons16 << Text16 << input16 << output16 << speakersList16 << arr16;
}

void TestClass::testtextMarkup() 
{
	QFETCH(QSet <person>, allPersons);
	QFETCH(newQMapName,Text);
	QFETCH(textTree*,input);
	QFETCH(textTree*, output);
	QFETCH(QSet <person>,speakersList);
	QFETCH(StanfordWord*, dell);

	person::InitPer(allPersons);

	QStringList a;
	a.push_back(QString("say"));
	a.push_back(QString("tell"));
	a.push_back(QString("ask"));

	sentence::InitKeys(a);

	QSet  <person> tmp;
	textMarkup(Text, input,  tmp);

	QString message = CmpTrees(input, output);
	QVERIFY2(message.isEmpty(), message.toUtf8().constData());

	QStringList Forgotten, superfluous;

	QString message1 = QString("\nResult checking failed\n\nForgotten: \n");

	bool isEqu = isEqual(tmp, speakersList, Forgotten, superfluous);

	if(!Forgotten.isEmpty() || !superfluous.isEmpty())
	{
		QMutableStringListIterator j(Forgotten);
		while(j.hasNext())
			message1+= j.next() + QString("\n");

		message1+=QString("\nSuperfluous: \n");

		QMutableStringListIterator k(superfluous);
		while(k.hasNext())
			message1+= k.next() + QString("\n");
	}

	QVERIFY2(isEqu, message1.toUtf8().constData());
	delete input;
	delete output;
	delete [] dell;
}

bool TestClass::isEqual(QSet <person> first, QSet <person> second, QStringList & Forgotten, QStringList & superfluous)
{
	//����� ����� ������� ��������� �� �������� �� ������
	QMutableSetIterator<person> j(first);
	while(j.hasNext())
	{
		person t = j.next();
		if(!second.contains(t))
		{
			superfluous.push_back(QString(t.name.data()));
		}
	}
	
	//����� ����� ������� ��������� �� �������� � ������
	QMutableSetIterator<person> k(second);
	while(k.hasNext())
	{
		person t = k.next();
		if(!first.contains(t))
		{
			Forgotten.push_back(QString(t.name.data()));
		}
	}

	//������� ������� ����, ��� ��������� ������������
	return superfluous.isEmpty() && Forgotten.isEmpty();
}

bool TestClass::isEqualVectors(QVector<corefCoord> main, QVector<corefCoord> second, QVector<corefCoord> & Forgotten, QVector<corefCoord> & superfluous)
{
	//����� ����� ������� ������� �� �������� �� ������
	for(int i = 0; i < main.size(); i++)
	{
		bool hasValue = false;
		for(int j = 0; j < second.size() && !hasValue; j++)
		{
			hasValue = compareCoords(main[i], second[j]);
		}

		if(!hasValue)
		{
			Forgotten.push_back(main[i]);
		}
	}

	//����� ����� ������� ������� �� �������� �� ������
	for(int i = 0; i < second.size(); i++)
	{
		bool hasValue = false;
		for(int j = 0; j < main.size() && !hasValue; j++)
		{
			hasValue = compareCoords(main[j], second[i]);
		}

		if(!hasValue)
		{
			superfluous.push_back(second[i]);
		}
	}

	//������� ������� ��������������� ��������
	return Forgotten.isEmpty() && superfluous.isEmpty();
}

bool TestClass::compareCoords(corefCoord a, corefCoord b)
{
	//�������� ������ ���� ������ a � ��������������� ����� b
	if(a.endID != b.endID)
	{
		return false;
	}
	if(a.rootID != b.rootID)
	{
		return false;
	}
	if(a.sentenceID != b.sentenceID)
	{
		return false;
	}
	if(a.startID != b.startID)
	{
		return false;
	}
	return true;
}

QString TestClass::CmpTrees(textTree* a, textTree* b)
{
	//�������� ��������� ���
	if(a->startingTag != b->startingTag)
	{
		return(QString("StartingTag") + a->startingTag + QString("Real:") + b->startingTag);
	}
	//�������� �������� ���
	if(a->endingTag != b->endingTag)
	{
		QStringList aTag = a->endingTag.split(QString("</span></u>("));
		QStringList bTag = b->endingTag.split(QString("</span></u>("));

		aTag = aTag[1].split(QString(")"));
		bTag = bTag[1].split(QString(")"));

		aTag = aTag[0].split(QString(", "));
		bTag = bTag[0].split(QString(", "));

		for(int i = 0; i<aTag.size(); i++)
		{
			if(!bTag.contains(aTag[i]))
			{
				//������� ������ ��� ������������
				return(QString("EndingTag mustBe:") + a->endingTag + QString("Real:") + b->endingTag);
			}
		}

		for(int i = 0; i<bTag.size(); i++)
		{
			if(!aTag.contains(bTag[i]))
			{
				//������� ������ ��� ������������
				return(QString("EndingTag mustBe:") + a->endingTag + QString("Real:") + b->endingTag);
			}
		}
	}

	//�������� ���� ������ 
	if(a->highLevel != b->highLevel)
	{
		return(QString("Level"));
	}
	//�������� ���������� �����������
	if(a->mainText.size()!=b->mainText.size())
	{
		return(QString("TextSize"));
	}

	//���������� ����������� �� ������ ������
	QListIterator<sentence> i(a->mainText);
	QListIterator<sentence> j(b->mainText);
	int t = 0;
	while(i.hasNext())
	{
		sentence tmp1 = i.next();
		sentence tmp2 = j.next();
		//�������� ���������� ����
		if(tmp1.words.size() != tmp2.words.size())
		{
			return QString(QString("sentence ") + QString::number(t) + QString(" size"));
		}

		QListIterator<StanfordWord*> iter1(tmp1.words);
		QListIterator<StanfordWord*> iter2(tmp2.words);
		int t1 = 0;

		//���������� ����� � �����������
		while(iter1.hasNext())
		{
			StanfordWord* c1 = iter1.next();
			StanfordWord* c2 = iter2.next();

			if(*c1 != *c2)
			{
				return QString(QString("sentence ") + QString::number(t) + QString(" word ") + QString::number(t1));
			}
		}

		//�������� ���������� ��������
		if(tmp1.childList.size() != tmp2.childList.size())
		{
			return QString(QString("sentence ") + QString::number(t) + QString(" childs size"));
		}

		QListIterator<textTree*> child1(tmp1.childList);
		QListIterator<textTree*> child2(tmp2.childList);
		int t2 = 0;
		//�������� ��������
		while(child1.hasNext())
		{
			QString mess = CmpTrees(child1.next(), child2.next());
			if(!mess.isEmpty())
			{
				return QString("child")+QString::number(t2)+QString("->")+mess;
			}
			t2++;
		}

		t++;
	}

	//������� ������ ������, ���� ��������� ������ �������
	return QString();
}